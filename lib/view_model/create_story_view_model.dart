import 'dart:io';

import 'package:tfg_app/model/story_page.dart';
import 'package:tfg_app/model/services/database.dart';
import 'package:tfg_app/model/story.dart';
import 'package:tfg_app/model/user.dart';

class CreateStoryViewModel{

  DatabaseService _databaseService = DatabaseService();

  Future createStory(Story story) async {
    /*var story = new Story(
        title: 'Cuento de fantasía 3', genre: 'Fantasía', author: 'Pepe', numPages: '0');*/

    var storyID = await _databaseService.createStory(story);

    if (storyID != null) {
      _databaseService.addStoryToCreator(storyID, story);
    }

    return storyID;
  }

  Future updateStory(Story story, String storyID) async {

    return await _databaseService.updateStory(story, storyID);

  }

  Future<List<String>> getPictogramsByKeywords(String keywords) async{
    return await _databaseService.getPictogramsByKeywords(keywords);
  }

  Future addPageToStory(List<String> page, String pageID, String storyID) async{
      return await _databaseService.addPageToStory(page, pageID, storyID);
  }

  Future updateStoryNumPages(String storyID, int numPages) async{
    return await _databaseService.updateNumPages(storyID, numPages);
  }

  Future<void> saveImages(File _image, String storyID) async {
    return await _databaseService.saveImages(_image, storyID);
  }

  Future<String> uploadFile(File _image) async {
    return await _databaseService.uploadFile(_image);
  }

  Future addPictoToWord(String storyID, String pageID, String wordID, String picto) async{
    return await _databaseService.addPictoToWord(storyID, pageID, wordID, picto);
  }

  Future deletePictoFromWord(String storyID, String pageID, String wordID) async{
    return await _databaseService.deletePictoFromWord(storyID, pageID, wordID);
  }

  Future<List<dynamic>> getUsers() async {
    return await _databaseService.getUsers();
  }

  Future assignStoryToUser(String storyID, String userID) async{

    print(storyID);
    Story story = new Story();

    story = await _databaseService.getStoryData(storyID);

    if (story != null){
      await _databaseService.saveStoryInUser(story, userID, storyID);
    }

    User user = new User();

    user = await _databaseService.getUserData(userID);

    if (user != null) {
      await _databaseService.saveUserInStory(user, userID, storyID);
    }

    return 0;
  }

  Future<List<dynamic>> getUsersAssigned(String storyID) async {
    return await _databaseService.getUsersAssigned(storyID);
  }

  Future addImageToPage(String storyID, String pageID, String image) async {
    return await _databaseService.addImageToPage(storyID, pageID, image);
  }

  Future addImageTextToPage(String storyID, String pageID, String imageText) async {
    return await _databaseService.addImageTextToPage(storyID, pageID, imageText);
  }

  Future<StoryPage> getPageImageInfo(String storyID, String pageID) async {
    return await _databaseService.getPageImage(pageID, storyID);
  }

  Future updateWord(String wordID, String storyID, String pageID, String word) async {
    return await _databaseService.updateWord(wordID, storyID, pageID, word);
  }
}