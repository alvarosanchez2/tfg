import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:tfg_app/model/tutor.dart';
import 'package:tfg_app/model/services/auth_service.dart';
import 'package:tfg_app/model/services/database.dart';
import 'package:tfg_app/model/educator.dart';
import 'package:tfg_app/model/user.dart';

class SignUpViewModel {
  AuthService _authService = AuthService();
  DatabaseService _databaseService = DatabaseService();

  Future registerUser(User user) async {
    // Cargamos el usuario en BD
    var result = await _databaseService.setUserDataOnRegister(user);

    print(result);
    // Guardamos en la lista del tutor
    await _databaseService.saveUserInTutorList(user, result);

    // Guardamos el tutor en el usuario
    FirebaseUser userLogged = await _authService.getCurrentUserLogged();

    Tutor tutor = await _databaseService.getTutorData(userLogged.uid);

    await _databaseService.saveTutorInUserList(tutor, result, userLogged.uid);

    if (result != null) {
      return result;
    } else {
      return null;
    }
  }

  Future registerTutor(Tutor tutor) async {
    var result = await _authService.register(tutor.email, tutor.password);

    if (result != null) {
      // Cargamos el usuario en BD
      await _databaseService.setTutorDataOnRegister(result, tutor);
      return result;
    } else {
      return null;
    }
  }

  Future registerEducator(Educator educator) async {
    var result = await _authService.register(educator.email, educator.password);

    if (result != null) {
      await _databaseService.setEducatorDataOnRegister(result, educator);
      return result;
    } else {
      return null;
    }
  }

  Future signInUser(String email, String password) async {
    var result = await _authService.signInWithEmailAndPassword(email, password);

    if (result != null) {
      return result;
    } else {
      return null;
    }
  }

  Future<QuerySnapshot> getUsersData(String tutorType, String fullname) async {
    return await _databaseService.getUsersData(tutorType, fullname);
  }

  Future<String> getTutorType() {
    return _databaseService.getUserLoggedType();
  }

  Future<List<dynamic>> getEducators() async {
    return await _databaseService.getEducators();
  }

  Future<List<dynamic>> getEducatorsAssigned(String userID) async {
    return await _databaseService.getEducatorsAsigned(userID);
  }

  Future assignTutors(String tutorID, String tutorType, String userID) async {
    Educator educator = new Educator();
    Tutor tutor = new Tutor();

    if (tutorType == 'Educador') {
      educator = await _databaseService.getEducatorData(tutorID);
    } else {
      tutor = await _databaseService.getTutorData(tutorID);
    }

    if (tutorType == 'Educador') {
      if (educator != null){
        _databaseService.saveEducatorInUserList(educator, userID, tutorID);
      } else {
        return null;
      }
    } else {
      if (tutor != null) {
        _databaseService.saveTutorInUserList(tutor, userID, tutorID);
      } else {
        return null;
      }
    }

    User user = await _databaseService.getUserData(userID);

    if (user != null) {
      if (tutorType == 'Educador') {
        _databaseService.saveUserInEducatorList(user, userID, tutorID);
      } else {
        _databaseService.saveUserInSpecificTutorList(user, userID, tutorID);
      }
    } else {
      return null;
    }

    return 0;
  }

  Future logOut() async {
    return await _authService.signOut();

  }

  Future changePassword(String password) async {

    var uid = await _authService.changePassword(password);

    print(uid.toString());

    if (uid != null) {
      _databaseService.updatePassword(uid.toString(), password);
    }

  }


  Future<User> getUserInfo(String userID) async {
    return await _databaseService.getUserData(userID);
  }

  Future updateUser(User user) async {
    return await _databaseService.updateUserInfo(user);
  }
}
