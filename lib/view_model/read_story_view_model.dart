import 'package:tfg_app/model/services/database.dart';
import 'package:tfg_app/model/story.dart';

class ReadStoryViewModel {
  DatabaseService _databaseService = DatabaseService();

  Stream<List<Story>> getStories(String userID) {
    return _databaseService.getStories(userID);
  }

  Future<List<dynamic>> getPageInfo(String pageID, String storyID) async {
    return await _databaseService.getPagesInfo(pageID, storyID);
  }

  Future<List<dynamic>> getStoriesCreated() async {
    return await _databaseService.getStoriesCreated();
  }

  Future deleteStory(String storyID) async {
    await _databaseService.delete(storyID);
    return await _databaseService.deleteStory(storyID);
  }

  Future<Story> getStory(String storyID) async {
    var result = await _databaseService.getStoryData(storyID);
    return result;
  }

  Stream<List<Story>> getStoriesByGenre(String genre, String userID) {
    return _databaseService.getStoriesByGenre(genre, userID);
  }
}
