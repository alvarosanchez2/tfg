import 'package:flutter/material.dart';
import 'package:tfg_app/view/screens/add_image_to_page_screen.dart';
import 'package:tfg_app/view/screens/change_pass_screen.dart';
import 'package:tfg_app/view/screens/create_story_screen.dart';
import 'package:tfg_app/view/screens/edit_story_child_view_screen.dart';
import 'package:tfg_app/view/screens/front_page_story_screen.dart';
import 'package:tfg_app/view/screens/manage_stories_screen.dart';
import 'package:tfg_app/view/screens/read_story_screen.dart';
import 'package:tfg_app/view/screens/select_genre_screen.dart';
import 'package:tfg_app/view/screens/login_screen.dart';
import 'package:tfg_app/view/screens/register_screen.dart';
import 'package:tfg_app/view/screens/register_user_screen.dart';
import 'package:tfg_app/view/screens/select_image_screen.dart';
import 'package:tfg_app/view/screens/select_user_page.dart';
import 'package:flutter/services.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();   SystemChrome.setPreferredOrientations([DeviceOrientation.portraitDown,DeviceOrientation.portraitUp]).then((_){
    runApp(MyApp());
  });
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: "/",
      routes: {
        "/": (BuildContext context) => LogIn(),
        "/register": (BuildContext context) => RegisterPage(),
        "/register-user": (BuildContext context) => RegisterUserPage(),
        "/home": (BuildContext context) => SelectGenrePage(),
        "/select-user": (BuildContext context) => SelectUserPage(),
        "/manage-book": (BuildContext context) => ManageBooksPage(),
        "/story-front-page": (BuildContext context) => StoryFrontPage(),
        "/read-story": (BuildContext context) => ReadStoryPage(),
        "/create-story": (BuildContext context) => CreateStoryPage(),
        "/select-image": (BuildContext context) => SelectImagePage(),
        "/change-password": (BuildContext context) => ChangePasswordPage(),
        "/add-image-to-page": (BuildContext context) => AddImageToPage(),
        "/edit-story-child-view": (BuildContext context) => EditChildViewPage()
      },
    );
  }
}
