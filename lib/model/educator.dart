class Educator {
  final String educatorID;
  final String name;
  final String fullname;
  final String email;
  final String password;
  final int phoneNumber;
  final String tutorType;

  Educator({this.educatorID, this.name, this.fullname, this.email, this.password, this.phoneNumber, this.tutorType});
}