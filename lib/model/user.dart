class User {
  final String userID;
  final String name;
  final String fullname;
  final int age;
  final String teaType;
  final String email;
  final String password;
  final String birthDate;
  final String favGenre;
  final String interest;


  User({this.name, this.fullname, this.age, this.teaType, this.email, this.password, this.userID, this.birthDate, this.favGenre, this.interest});


}