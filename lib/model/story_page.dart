import 'package:tfg_app/model/word.dart';

class StoryPage{
  final List<Word> words;
  final String image;
  final String imageText;

  StoryPage({this.words, this.image, this.imageText});
}