class Word {

  final int wordID;
  final String picto;
  final String word;

  Word({this.wordID, this.picto, this.word});
}