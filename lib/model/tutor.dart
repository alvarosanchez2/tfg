
class Tutor {
  final String tutorID;
  final String name;
  final String fullname;
  final String email;
  final String password;
  final int phoneNumber;
  final String tutorType;

  Tutor({this.tutorID, this.name, this.fullname, this.email, this.password, this.phoneNumber, this.tutorType});
}