class Story{
  final String storyID;
  final String title;
  final String genre;
  final String author;
  final String numPages;
  final String front;
  final String genreImage;

  Story({this.storyID,this.title, this.genre, this.author, this.numPages, this.front, this.genreImage});
}

