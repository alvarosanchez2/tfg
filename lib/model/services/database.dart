import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:tfg_app/model/services/auth_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:tfg_app/model/educator.dart';
import 'package:tfg_app/model/story.dart';
import 'package:tfg_app/model/word.dart';
import 'package:uuid/uuid.dart';
import 'package:http/http.dart' as http;
import 'package:cloud_functions/cloud_functions.dart';

import '../story_page.dart';
import '../tutor.dart';
import '../user.dart';

class DatabaseService {
  AuthService _authService = AuthService();

  // Variable que genera el id de los usuarios
  var uuid = Uuid();

  // Referencias a las diferentes colecciones de la BD
  final CollectionReference userCollection =
      Firestore.instance.collection('users');
  final CollectionReference tutorsCollection =
      Firestore.instance.collection('tutors');
  final CollectionReference educatorCollection =
      Firestore.instance.collection('educators');
  final CollectionReference storiesCollection =
      Firestore.instance.collection('stories');

  // Genera un hash y cifra la contraseña con md5
  String generateMd5(String input) {
    return md5.convert(utf8.encode(input)).toString();
  }

  // Guarda un usuario en BD
  Future setUserDataOnRegister(User user) async {
    try {
      var id = uuid.v4();
      await userCollection.document(id).setData({
        'name': user.name,
        'fullname': user.fullname,
        'age': user.age,
        'teaType': user.teaType,
        'email': user.email,
        'password': generateMd5(user.password),
        'userID': id,
        'birthDate': user.birthDate,
        'favGenre': user.favGenre,
        'interest': user.interest
      });

      return id;
    } catch (e) {
      print('ERROR: $e');
    }
  }

  // Guarda un tutor en BD
  Future setTutorDataOnRegister(String id, Tutor tutor) async {
    try {
      return await tutorsCollection.document(id).setData({
        'tutorID': id,
        'name': tutor.name,
        'fullname': tutor.fullname,
        'email': tutor.email,
        'password': generateMd5(tutor.password),
        'phoneNumber': tutor.phoneNumber,
        'tutorType': tutor.tutorType
      });
    } catch (e) {
      print('ERROR: $e');
    }
  }

  // Guarda un educador en BD
  Future setEducatorDataOnRegister(String id, Educator educator) async {
    try {
      return await educatorCollection.document(id).setData({
        'educatorID': id,
        'name': educator.name,
        'fullname': educator.fullname,
        'email': educator.email,
        'password': generateMd5(educator.password),
        'phoneNumber': educator.phoneNumber,
        'tutorType': educator.tutorType
      });
    } catch (e) {
      print('ERROR: $e');
    }
  }

  // Guardamos un usuario para un tutor específico
  Future saveUserInTutorList(User user, String userID) async {
    try {
      FirebaseUser userLogged = await _authService.getCurrentUserLogged();

      await tutorsCollection
          .document(userLogged.uid.toString())
          .collection("users")
          .document(userID)
          .setData({
        'name': user.name,
        'fullname': user.fullname,
        'age': user.age,
        'teaType': user.teaType,
        'email': user.email,
        'userID': userID,
        'birthDate': user.birthDate,
        'favGenre': user.favGenre,
        'interest': user.interest
      });
    } catch (e) {
      print('ERROR: $e');
    }
  }

  // Actualizar contraseña de un usuario
  Future updatePassword(String id, String password) async {
    try {
      var tutorType = await getUserLoggedType();

      if (tutorType == 'Tutor') {
        tutorsCollection.document(id).updateData({'password': generateMd5(password)});

      } else {
        educatorCollection.document(id).updateData({'password': generateMd5(password)});
      }

      print('Contraseña modificada');

    } catch (e) {
      print('ERROR: $e');
    }
  }

  // Obtiene los usuarios asigandos al usuario loggeado
  Future<QuerySnapshot> getUsersData(String tutorType, String fullname) async {
    try {
      FirebaseUser userLogged = await _authService.getCurrentUserLogged();
      var kids = Stream.empty();

      if (fullname != null && fullname != "") {
        if (tutorType == 'Tutor') {
          kids = tutorsCollection
              .document(userLogged.uid.toString())
              .collection("users")
              .where('fullname', isEqualTo: fullname)
              .snapshots();
          return (await kids.first);
        } else {
          kids = educatorCollection
              .document(userLogged.uid.toString())
              .collection("users")
              .where('fullname', isEqualTo: fullname)
              .snapshots();
          return (await kids.first);
        }
      } else {
        if (tutorType == 'Tutor') {
          kids = tutorsCollection
              .document(userLogged.uid.toString())
              .collection("users")
              .snapshots();
          return (await kids.first);
        } else {
          kids = educatorCollection
              .document(userLogged.uid.toString())
              .collection("users")
              .snapshots();
          return (await kids.first);
        }
      }
    } catch (e) {
      print('ERROR: $e');
    }
  }

  // Obtiene el tipo del usuario que ha iniciado sesión, si es tutor o si es un educador
  Future<String> getUserLoggedType() async {
    try {
      FirebaseUser userLogged = await _authService.getCurrentUserLogged();
      var userInfo =
          await tutorsCollection.document(userLogged.uid.toString()).get();
      var userType = null;

      if (userInfo.data != null) {
        userType = userInfo.data['tutorType'];
      } else {
        userInfo =
            await educatorCollection.document(userLogged.uid.toString()).get();

        if (userInfo != null) {
          userType = userInfo.data['tutorType'];
        }
      }
      return userType;
    } catch (e) {
      print('ERROR: $e');
    }
  }

  // Obtiene las lista de educadores que se encuentra en Base de Datos
  Future<List<dynamic>> getEducators() async {
    var educators = [];

    FirebaseUser userLogged = await _authService.getCurrentUserLogged();

    try {
      await educatorCollection
          .getDocuments()
          .then((QuerySnapshot querySnapshot) => {
                querySnapshot.documents.forEach((element) {
                  educators.add({
                    'id': element.documentID,
                    'name': element['name'],
                    'fullname': element['fullname'],
                    'tutorType': element['tutorType']
                  });
                })
              });

      // Obtenemos tambien los tutores porque podemos asignarlos a los niños
      await tutorsCollection
          .getDocuments()
          .then((QuerySnapshot querySnapshot) => {
                querySnapshot.documents.forEach((element) {
                  if (userLogged.uid != element.documentID) {
                    educators.add({
                      'id': element.documentID,
                      'name': element['name'],
                      'fullname': element['fullname'],
                      'tutorType': element['tutorType']
                    });
                  }
                })
              });
    } catch (e) {
      print('ERROR: $e');
    }
    return educators;
  }

  // Obtiene la lista de educadores asignados al niño seleccionado
  Future<List<dynamic>> getEducatorsAsigned(String userID) async {
    var educators = [];

    try {
      await userCollection
          .document(userID)
          .collection("educators")
          .getDocuments()
          .then((QuerySnapshot querySnapshot) => {
                querySnapshot.documents.forEach((element) {
                  educators.add({
                    'id': element.documentID,
                    'name': element['name'],
                    'fullname': element['fullname']
                  });
                })
              });
    } catch (e) {
      print('ERROR: $e');
    }
    return educators;
  }

  // Parsea los datos y los convierte en una entidad Educator
  Educator _educatorDataFromSnapshot(DocumentSnapshot snapshot) {
    return Educator(
        educatorID: snapshot.data['educatorID'],
        name: snapshot.data['name'],
        fullname: snapshot.data['fullname'],
        email: snapshot.data['email'],
        password: snapshot.data['password'],
        phoneNumber: snapshot.data['phoneNumber'],
        tutorType: snapshot.data['tutorType']);
  }

  // Parsea los datos y los convierte en una entidad Tutor
  Tutor _tutorDataFromSnapshot(DocumentSnapshot snapshot) {
    return Tutor(
        tutorID: snapshot.data['tutorID'],
        name: snapshot.data['name'],
        fullname: snapshot.data['fullname'],
        email: snapshot.data['email'],
        password: snapshot.data['password'],
        phoneNumber: snapshot.data['phoneNumber'],
        tutorType: snapshot.data['tutorType']);
  }

  // Parsea los datos y los convierte en una entidad Usuario
  User _userDataFromSnapshot(DocumentSnapshot snapshot) {
    return User(
        name: snapshot.data['name'],
        fullname: snapshot.data['fullname'],
        email: snapshot.data['email'],
        password: snapshot.data['password'],
        age: snapshot.data['age'],
        teaType: snapshot.data['teaType'],
        userID: snapshot.data['userID'],
        birthDate: snapshot.data['birthDate'],
        favGenre: snapshot.data['favGenre'],
        interest: snapshot.data['interest']
    );
  }

  // Parsea los datos y los convierte a una Lista de Usuarios
  List<User> _userListDataFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc) {
      return User(
          name: doc.data['name'],
          fullname: doc.data['fullname'],
          email: doc.data['email'],
          password: doc.data['password'],
          age: doc.data['age'],
          teaType: doc.data['teaType'],
          userID: doc.data['userID'],
          birthDate: doc.data['birthDate'],
          favGenre: doc.data['favGenre'],
          interest: doc.data['interest']
      );
    }).toList();
  }

  // Obtiene todos los datos de un educador específico
  Future<Educator> getEducatorData(String educatorID) async {
    var educator = null;

    try {
      await educatorCollection.document(educatorID).get().then(
          (DocumentSnapshot documentSnapshot) =>
              {educator = _educatorDataFromSnapshot(documentSnapshot)});
    } catch (e) {
      print('ERROR: $e');
    }
    return educator;
  }

  // Obtiene todos los datos de un usuario específico
  Future<User> getUserData(String userID) async {
    var user = null;

    try {
      await userCollection.document(userID).get().then(
          (DocumentSnapshot documentSnapshot) =>
              {user = _userDataFromSnapshot(documentSnapshot)});
    } catch (e) {
      print('ERROR: $e');
    }

    return user;
  }

  // Obtiene todos los datos de un educador específico
  Future<Tutor> getTutorData(String tutorID) async {
    var tutor = null;

    try {
      await tutorsCollection.document(tutorID).get().then(
          (DocumentSnapshot documentSnapshot) =>
              {tutor = _tutorDataFromSnapshot(documentSnapshot)});
    } catch (e) {
      print('ERROR: $e');
    }
    return tutor;
  }

  // Asigna un educador a un usuario específico
  Future saveEducatorInUserList(
      Educator educator, String userID, String educatorID) async {
    try {
      await userCollection
          .document(userID)
          .collection("educators")
          .document(educatorID)
          .setData({
        'educatorID': educatorID,
        'name': educator.name,
        'fullname': educator.fullname,
        'email': educator.email,
        'password': generateMd5(educator.password),
        'phoneNumber': educator.phoneNumber,
        'tutorType': educator.tutorType
      });
    } catch (e) {
      print('ERROR: $e');
    }
  }

  // Asigna un tutor o un educador a un usuario específico
  Future saveTutorInUserList(Tutor tutor, String userID, String tutorID) async {
    try {
      await userCollection
          .document(userID)
          .collection("educators")
          .document(tutorID)
          .setData({
        'tutorID': tutorID,
        'name': tutor.name,
        'fullname': tutor.fullname,
        'email': tutor.email,
        'password': generateMd5(tutor.password),
        'phoneNumber': tutor.phoneNumber,
        'tutorType': tutor.tutorType
      });
    } catch (e) {
      print('ERROR: $e');
    }
  }

  // Asigna un usuario a un educador específico
  Future saveUserInEducatorList(
      User user, String userID, String educatorID) async {
    try {
      await educatorCollection
          .document(educatorID)
          .collection("users")
          .document(userID)
          .setData({
        'name': user.name,
        'fullname': user.fullname,
        'age': user.age,
        'teaType': user.teaType,
        'email': user.email,
        'userID': userID,
        'birthDate': user.birthDate,
        'favGenre': user.favGenre,
        'interest': user.interest
      });
    } catch (e) {
      print('ERROR: $e');
    }
  }

  // Asigna un usuario a un tutor específico
  Future saveUserInSpecificTutorList(
      User user, String userID, String tutorID) async {
    try {
      await tutorsCollection
          .document(tutorID)
          .collection("users")
          .document(userID)
          .setData({
        'name': user.name,
        'fullname': user.fullname,
        'age': user.age,
        'teaType': user.teaType,
        'email': user.email,
        'userID': userID,
        'birthDate': user.birthDate,
        'favGenre': user.favGenre,
        'interest': user.interest
      });
    } catch (e) {
      print('ERROR: $e');
    }
  }

  // Actualiza la información de un usuario específico
  Future updateUserInfo(User user) async {

    print('EN BD');
    print(user.userID);
    try {
      await userCollection.document(user.userID).updateData({
        'userID': user.userID,
        'name': user.name,
        'fullname': user.fullname,
        'age': user.age,
        'teaType': user.teaType,
        'email': user.email,
        'birthDate': user.birthDate,
        'favGenre': user.favGenre,
        'interest': user.interest
      });

    } catch (e) {
      print('ERROR: $e');
    }
  }

  //Funciones relacionadas con los cuentos

  // Crea un cuento
  Future createStory(Story story) async {
    try {
      var id = uuid.v1();

      print(id);
      await storiesCollection.document(id).setData({
        'storyID': id,
        'title': story.title,
        'genre': story.genre,
        'author': story.author,
        'numPages': story.numPages,
        "front": story.front,
        "genreImage": story.genreImage
      });
      return id;
    } catch (e) {
      print('ERROR: $e');
    }
  }

  // Snapshots de la entidad de cuentos
  List<Story> _storyListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc) {
      return Story(
          storyID: doc.data['storyID'] ?? '',
          title: doc.data['title'] ?? '',
          genre: doc.data['genre'] ?? '',
          author: doc.data['author'] ?? '',
          front: doc.data['front'] ?? '',
          numPages: doc.data['numPages'] ?? '',
          genreImage: doc.data['genreImage'] ?? '');
    }).toList();
  }

  // Obtenemos todos los cuentos
  Stream<List<Story>> getStories(String userID) {
    return userCollection
        .document(userID)
        .collection('stories')
        .snapshots()
        .map(_storyListFromSnapshot);
  }

  Stream<List<Story>> getStoriesByGenre(String genre, String userID) {
    return userCollection
        .document(userID)
        .collection('stories')
        .where('genre', isEqualTo: genre)
        .snapshots()
        .map(_storyListFromSnapshot);
  }

  // Parsea los datos y los convierte en una entidad Página
  StoryPage _pageDataFromSnapshot(DocumentSnapshot snapshot) {
    return StoryPage(
        words: snapshot.data['words'],
        image: snapshot.data['image'],
        imageText: snapshot.data['imageText']);
  }

  // Parsea los datos y los convierte en una entidad Word
  Word _wordDataFromSnapshot(DocumentSnapshot snapshot) {
    return Word(
        word: snapshot.data['word'],
        picto: snapshot.data['picto'],
        wordID: snapshot.data['wordID']);
  }

  // Obtener info páginas
  Future<List<dynamic>> getPagesInfo(String pageID, String storyID) async {
    var wordsList = [];
    var wordInfo = null;

    try {
      var allWords = await storiesCollection
          .document(storyID)
          .collection("pages")
          .document(pageID)
          .collection("words")
          .getDocuments();

      var numWords = allWords.documents.length;

      for (int i = 1; i <= numWords; i++) {
        await storiesCollection
            .document(storyID)
            .collection("pages")
            .document(pageID)
            .collection("words")
            .document(i.toString())
            .get()
            .then((DocumentSnapshot documentSnapshot) =>
                {wordInfo = _wordDataFromSnapshot(documentSnapshot)});
        wordsList.add(wordInfo);
      }
    } catch (e) {
      print('ERROR: $e');
    }

    return wordsList;
  }

  // Añadir cuento a la lista de cuentos del creador
  Future addStoryToCreator(String storyID, Story story) async {
    FirebaseUser userLogged = await _authService.getCurrentUserLogged();
    var tutorType = await getUserLoggedType();

    try {
      if (tutorType == 'Tutor') {
        await tutorsCollection
            .document(userLogged.uid.toString())
            .collection("stories")
            .document(storyID)
            .setData({
          'title': story.title,
          'storyID': storyID,
          'genre': story.genre,
          'author': story.author,
          'numPages': story.numPages,
          'genreImage': story.genreImage
        });
      } else {
        await educatorCollection
            .document(userLogged.uid.toString())
            .collection("stories")
            .document(storyID)
            .setData({
          'title': story.title,
          'storyID': story.storyID,
          'genre': story.genre,
          'author': story.author,
          'numPages': story.numPages,
          'genreImage': story.genreImage
        });
      }
    } catch (e) {}
  }

  // Obtiene la lista de cuentos creados
  Future<List<dynamic>> getStoriesCreated() async {
    var stories = [];
    FirebaseUser userLogged = await _authService.getCurrentUserLogged();
    var tutorType = await getUserLoggedType();

    try {
      if (tutorType == 'Tutor') {
        await tutorsCollection
            .document(userLogged.uid.toString())
            .collection("stories")
            .getDocuments()
            .then((QuerySnapshot querySnapshot) => {
                  querySnapshot.documents.forEach((element) {
                    stories.add(
                        {'id': element.documentID, 'title': element['title']});
                  })
                });
      } else {
        await educatorCollection
            .document(userLogged.uid.toString())
            .collection("stories")
            .getDocuments()
            .then((QuerySnapshot querySnapshot) => {
                  querySnapshot.documents.forEach((element) {
                    stories.add(
                        {'id': element.documentID, 'title': element['title']});
                  })
                });
      }
    } catch (e) {
      print('ERROR: $e');
    }

    return stories;
  }

  // Eliminamos un cuento
  Future deleteStory(String storyID) async {
    var result;
    FirebaseUser userLogged = await _authService.getCurrentUserLogged();
    var tutorType = await getUserLoggedType();

    try {
      //Eliminamos los cuentos de la lista de usuarios a los que estaban asignados
      result = storiesCollection
          .document(storyID)
          .collection('users')
          .snapshots()
          .map(_userListDataFromSnapshot);

      var listaUsers = await result.first;
      List<String> listUsersAEliminarHistorias = [];
      for (User user in listaUsers) {
        listUsersAEliminarHistorias.add(user.userID);
      }

      for (String usuario in listUsersAEliminarHistorias) {
        print(usuario);
        await userCollection
            .document(usuario)
            .collection('stories')
            .document(storyID)
            .delete();
      }

      // Eliminamos los cuentos de la lista de cuentos de los tutores o educadores
      if (tutorType == 'Tutor') {
        await tutorsCollection
            .document(userLogged.uid.toString())
            .collection("stories")
            .document(storyID)
            .delete();
      } else {
        await educatorCollection
            .document(userLogged.uid.toString())
            .collection("stories")
            .document(storyID)
            .delete();
      }
    } catch (e) {
      print('ERROR: $e');
    }
  }

  // Parsea los datos y los convierte en una entidad Story
  Story _storyDataFromSnapshot(DocumentSnapshot snapshot) {
    return Story(
        title: snapshot.data['title'],
        author: snapshot.data['author'],
        genre: snapshot.data['genre'],
        storyID: snapshot.data['storyID'],
        numPages: snapshot.data['numPages'],
        front: snapshot.data['front'],
        genreImage: snapshot.data['genreImage']);
  }

  // Obtiene la información de cuento específico
  Future<Story> getStoryData(String storyID) async {
    var story = null;

    try {
      await storiesCollection.document(storyID).get().then(
          (DocumentSnapshot documentSnapshot) =>
              {story = _storyDataFromSnapshot(documentSnapshot)});
    } catch (e) {
      print('ERROR: $e');
    }
    return story;
  }

  // Actualiza los datos de un cuento específico
  Future updateStory(Story story, String storyID) async {
    FirebaseUser userLogged = await _authService.getCurrentUserLogged();
    var tutorType = await getUserLoggedType();

    try {
      await storiesCollection.document(storyID).updateData({
        'title': story.title,
        'author': story.author,
        'genre': story.genre,
        'front': story.front,
        'storyID': storyID,
        'genreImage': story.genreImage
      });

      if (tutorType == 'Tutor') {
        await tutorsCollection
            .document(userLogged.uid.toString())
            .collection("stories")
            .document(storyID)
            .updateData({
          'title': story.title,
          'author': story.author,
          'genre': story.genre,
          'front': story.front,
          'storyID': storyID,
          'genreImage': story.genreImage
        });
      } else {
        await educatorCollection
            .document(userLogged.uid.toString())
            .collection("stories")
            .document(storyID)
            .updateData({
          'title': story.title,
          'author': story.author,
          'genre': story.genre,
          'front': story.front,
          'storyID': storyID,
          'genreImage': story.genreImage
        });
      }
    } catch (e) {
      print('ERROR: $e');
    }
  }

  // Función para guardar imágenes
  Future<String> uploadFile(File _image) async {
    String returnURL;

    StorageReference storageReference =
        FirebaseStorage.instance.ref().child('images/${_image.path}');
    StorageUploadTask uploadTask = storageReference.putFile(_image);
    await uploadTask.onComplete;
    print('File Uploaded');

    await storageReference.getDownloadURL().then((fileURL) {
      returnURL = fileURL;
    });
    return returnURL;
  }

  // Guarda la imagen seleccionada en BBDD
  Future<void> saveImages(File _image, String storyID) async {
    String imageURL = await uploadFile(_image);
    storiesCollection.document(storyID).updateData({"front": imageURL});
  }

  // Obtiene los ids de los pictogramas
  Future<List<String>> getPictogramsByKeywords(String keywords) async {
    List<String> idsList = List<String>();

    print(keywords);
    if (keywords != null && keywords != '') {
      var picto = jsonDecode((await http.get(
              'https://api.arasaac.org/api/pictograms/es/search/$keywords'))
          .body);

      for (int i = 0; i < picto.length; i++) {
        idsList.add(picto[i]['_id'].toString());
      }
    } else {
      return null;
    }
    return idsList;
  }

  // Obtiene todos los ids de los pictogramas de BD
  Future<List<String>> getPictograms() async {
    List<String> idsList = List<String>();

    var picto = jsonDecode(
        (await http.get('https://api.arasaac.org/api/pictograms/all/es')).body);

    for (int i = 0; i < picto.length; i++) {
      idsList.add(picto[i]['_id'].toString());
    }

    return idsList;
  }

  // Guarda una página y sus palabras
  Future addPageToStory(
      List<String> page, String pageID, String storyID) async {
    try {
      await storiesCollection
          .document(storyID)
          .collection("pages")
          .document(pageID)
          .setData({'image': null, 'imageText': null});

      for (int i = 0; i < page.length; i++) {
        await storiesCollection
            .document(storyID)
            .collection("pages")
            .document(pageID)
            .collection("words")
            .document((i + 1).toString())
            .setData(
                {'wordID': i + 1, 'word': page.elementAt(i), 'picto': null});
      }
    } catch (e) {
      print('ERROR: $e');
      return null;
    }

    return 0;
  }

  // Actualizar el número de páginas
  Future updateNumPages(String storyID, int numPages) async {
    try {
      await storiesCollection
          .document(storyID)
          .updateData({'numPages': numPages.toString()});
    } catch (e) {
      print('ERROR: $e');
      return null;
    }
  }

  // Añadir un pictograma o foto a una palabra
  Future addPictoToWord(
      String storyID, String pageID, String wordID, String picto) async {
    try {
      await storiesCollection
          .document(storyID)
          .collection('pages')
          .document(pageID)
          .collection('words')
          .document(wordID)
          .updateData({'picto': picto});
    } catch (e) {
      print('ERROR: $e');
      return null;
    }
  }

  // Desaignamos el picto de la palabra
  Future deletePictoFromWord(
      String storyID, String pageID, String wordID) async {
    try {
      await storiesCollection
          .document(storyID)
          .collection('pages')
          .document(pageID)
          .collection('words')
          .document(wordID)
          .updateData({'picto': null});
    } catch (e) {
      print('ERROR: $e');
      return null;
    }
  }

  // Obtenemos los usuarios asignados a un tutor o educador
  Future<List<dynamic>> getUsers() async {
    FirebaseUser userLogged = await _authService.getCurrentUserLogged();
    var tutorType = await getUserLoggedType();
    var users = [];

    try {
      if (tutorType == 'Tutor') {
        await tutorsCollection
            .document(userLogged.uid)
            .collection('users')
            .getDocuments()
            .then((QuerySnapshot querySnapshot) => {
                  querySnapshot.documents.forEach((element) {
                    users.add({
                      'id': element.documentID,
                      'name': element['name'],
                      'fullname': element['fullname']
                    });
                  })
                });
      } else {
        await educatorCollection
            .document(userLogged.uid)
            .collection('users')
            .getDocuments()
            .then((QuerySnapshot querySnapshot) => {
                  querySnapshot.documents.forEach((element) {
                    users.add({
                      'id': element.documentID,
                      'name': element['name'],
                      'fullname': element['fullname']
                    });
                  })
                });
      }
    } catch (e) {
      print('ERROR: $e');
    }
    return users;
  }

  // Asigna una historia a un niño específico
  Future saveStoryInUser(Story story, String userID, String storyID) async {
    try {
      await userCollection
          .document(userID)
          .collection("stories")
          .document(storyID)
          .setData({
        'title': story.title,
        'genre': story.genre,
        'author': story.author,
        'numPages': story.numPages,
        'front': story.front,
        'storyID': story.storyID,
        'genreImage': story.genreImage
      });
    } catch (e) {
      print('ERROR: $e');
    }
  }

  // Asigna una niño a una historia
  Future saveUserInStory(User user, String userID, String storyID) async {
    try {
      await storiesCollection
          .document(storyID)
          .collection("users")
          .document(userID)
          .setData({
        'name': user.name,
        'fullname': user.fullname,
        'age': user.age,
        'teaType': user.teaType,
        'email': user.email,
        'password': generateMd5(user.password),
        'userID': user.userID,
        'birthDate': user.birthDate,
        'favGenre': user.favGenre,
        'interest': user.interest
      });
    } catch (e) {
      print('ERROR: $e');
    }
  }

  // Obtiene la lista de niños asignados al cuento
  Future<List<dynamic>> getUsersAssigned(String storyID) async {
    var users = [];

    print(storyID);
    try {
      await storiesCollection
          .document(storyID)
          .collection("users")
          .getDocuments()
          .then((QuerySnapshot querySnapshot) => {
                querySnapshot.documents.forEach((element) {
                  users.add({
                    'id': element.documentID,
                    'name': element['name'],
                    'fullname': element['fullname']
                  });
                })
              });
    } catch (e) {
      print('ERROR: $e');
    }
    return users;
  }

  // Añadir un pictograma o foto a una página
  Future addImageToPage(String storyID, String pageID, String image) async {
    try {
      await storiesCollection
          .document(storyID)
          .collection('pages')
          .document(pageID)
          .updateData({'image': image});
    } catch (e) {
      print('ERROR: $e');
      return null;
    }
  }

  // Añadir un texto a esuchar al pictograma de la página
  Future addImageTextToPage(
      String storyID, String pageID, String imageText) async {
    try {
      await storiesCollection
          .document(storyID)
          .collection('pages')
          .document(pageID)
          .updateData({'imageText': imageText});
    } catch (e) {
      print('ERROR: $e');
      return null;
    }
  }

  // Obtiene la información del pictograma de una página
  Future<StoryPage> getPageImage(String pageID, String storyID) async {
    var pageInfo = null;

    try {
      await storiesCollection
          .document(storyID)
          .collection("pages")
          .document(pageID)
          .get()
          .then((DocumentSnapshot documentSnapshot) =>
              {pageInfo = _pageDataFromSnapshot(documentSnapshot)});
    } catch (e) {
      print('ERROR: $e');
    }

    return pageInfo;
  }

  // Borra recursivamente un cuento
  void delete(String storyID) async {
    var path = '/stories/' + storyID;
    var data = {'path': path};
    var deleteFn = CloudFunctions.instance
        .getHttpsCallable(functionName: 'recursiveDelete');

    await deleteFn.call(data);
  }

  Future updateWord(
      String wordID, String storyID, String pageID, String word) async {
    try {
      await storiesCollection
          .document(storyID)
          .collection('pages')
          .document(pageID)
          .collection('words')
          .document(wordID)
          .updateData({'word': word});
    } catch (e) {
      print('ERROR: $e');
    }
  }
}
