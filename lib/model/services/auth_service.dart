

import 'package:firebase_auth/firebase_auth.dart';

class AuthService {

  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future getCurrentUserLogged() async{
    return await _auth.currentUser();
  }

  // Función para registrarse con correo y contraseña
  Future register(String email, String password) async {
    try {
      AuthResult result = await _auth.createUserWithEmailAndPassword(email: email, password: password);
      FirebaseUser user = result.user;
      return user.uid;
    } catch(e) {
      print(e);
      return null;
    }
  }

  // Función para iniciar sesión con correo y contraseña
  Future signInWithEmailAndPassword(String email, String password) async {
    try{
      AuthResult result = await _auth.signInWithEmailAndPassword(email: email, password: password);
      FirebaseUser user = result.user;
      return user.uid;
    } catch(e) {
      print(e.toString());
      return null;
    }
  }

  // Cerrar sesión
  Future signOut() async {
    try{
      return await _auth.signOut();
    } catch(e){
      print(e.toString());
      return null;
    }
  }

  // Función para iniciar sesión con correo y contraseña
  Future changePassword(String password) async {
    try{
      FirebaseUser user = await _auth.currentUser();
      user.updatePassword(password);

      return user.uid;
    } catch(e) {
      print(e.toString());
      return null;
    }
  }

}