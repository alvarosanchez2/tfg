class FrontPageArguments {
  final String storyID;
  final String title;
  final String author;
  final String numPages;
  final String front;

  FrontPageArguments({this.storyID, this.title, this.author, this.numPages, this.front});
}
