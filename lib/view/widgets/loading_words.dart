import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:hexcolor/hexcolor.dart';

class LoadingWords extends StatelessWidget {
  LoadingWords({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: SpinKitDoubleBounce(
          color: HexColor('#72CEF2'),
          size: 50.0,
        ),
      ),
    );
  }
}