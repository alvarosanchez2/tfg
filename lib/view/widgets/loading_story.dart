import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:hexcolor/hexcolor.dart';

class LoadingStory extends StatelessWidget {
  LoadingStory({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30,
      child: Center(
        child: SpinKitCircle(
          color: HexColor('#72CEF2'),
          size: 40.0,
        ),
      ),

    );
  }
}
