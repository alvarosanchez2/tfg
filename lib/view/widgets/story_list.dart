import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:provider/provider.dart';
import 'package:tfg_app/model/story.dart';
import 'package:tfg_app/view/screens/select_story_on_genre_screen.dart';

class StoriesList extends StatefulWidget {
  final String userID;
  StoriesList({Key key, this.userID}) : super(key: key);

  @override
  _StoriesListState createState() {
    return _StoriesListState(userID: userID);
  }
}

class _StoriesListState extends State<StoriesList> {
  String userID;
  _StoriesListState({this.userID});

  MediaQueryData queryData;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final stories = Provider.of<List<Story>>(context) ?? [];
    var storiesGenreList = [];

    queryData = MediaQuery.of(context);

    var screenSize = queryData.size;

    Map<String, String> genresMap = new Map();

    stories.forEach((element) {
      if (!genresMap.containsKey(element.genre)) {
        var genreMap = {element.genre: element.genreImage};
        genresMap.addAll(genreMap);
      }
    });

    stories.forEach((element) {
      if (!storiesGenreList.contains(element.genre)) {
        storiesGenreList.add(element.genre);
      }
    });

    if (screenSize.height < 825) {
      return Center(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 10.0),
          child: (storiesGenreList.length == 0
              ? Center(
                  child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.error,
                      size: 28,
                      color: Colors.orange,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text('No se le han asigando cuentos todavía',
                        style: TextStyle(fontSize: 28)),
                  ],
                ))
              : GridView.builder(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                  ),
                  itemCount: storiesGenreList.length,
                  itemBuilder: (BuildContext context, int index) =>
                      GestureDetector(
                    onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => SelectGenreStoryPage(
                              genre: storiesGenreList[index], userID: userID),
                        )),
                    child: Container(
                      height: 200,
                      width: 300,
                      child: Card(
                        //color: HexColor('#96E6F5'),
                        child: Container(
                          padding: EdgeInsets.only(left: 20, right: 20),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(height: 5),
                              (genresMap[storiesGenreList[index]] == null ||
                                      genresMap[storiesGenreList[index]] == ''
                                  ? Expanded(child: Image.asset("cuento.png"))
                                  : Expanded(
                                      child: Image.network(
                                          genresMap[storiesGenreList[index]]))),
                              SizedBox(height: 10),
                              Text(
                                storiesGenreList[index]
                                    .toString()
                                    .toUpperCase(),
                                style: GoogleFonts.lato(
                                    fontWeight: FontWeight.bold, fontSize: 24),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                )),
        ),
      );
    } else {
      return Center(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 10.0),
          child: (storiesGenreList.length == 0
              ? Center(
                  child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.error,
                      size: 32,
                      color: Colors.orange,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text('No se le han asigando cuentos todavía',
                        style: TextStyle(fontSize: 28)),
                  ],
                ))
              : GridView.builder(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                  ),
                  itemCount: storiesGenreList.length,
                  itemBuilder: (BuildContext context, int index) =>
                      GestureDetector(
                    onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => SelectGenreStoryPage(
                              genre: storiesGenreList[index], userID: userID),
                        )),
                    child: Container(
                      height: 200,
                      width: 300,
                      child: Card(
                        //color: HexColor('#96E6F5'),
                        child: Container(
                          padding: EdgeInsets.only(left: 20, right: 20),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(height: 5),
                              (genresMap[storiesGenreList[index]] == null ||
                                      genresMap[storiesGenreList[index]] == ''
                                  ? Expanded(child: Image.asset("cuento.png"))
                                  : Expanded(
                                      child: Image.network(
                                          genresMap[storiesGenreList[index]]))),
                              SizedBox(height: 20),
                              Text(
                                storiesGenreList[index]
                                    .toString()
                                    .toUpperCase(),
                                style: GoogleFonts.lato(
                                    fontWeight: FontWeight.bold, fontSize: 34),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                )),
        ),
      );
    }
  }
}
