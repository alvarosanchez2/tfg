import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class LoadingUsers extends StatelessWidget {
  LoadingUsers({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Center(
        child: SpinKitPulse(
          color: Colors.blueAccent,
          size: 50.0,
        ),
      ),

    );
  }
}
