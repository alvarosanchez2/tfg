import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:tfg_app/model/story.dart';
import 'package:tfg_app/view/screens/add_picto_to_text_screen.dart';
import 'package:tfg_app/view/screens/edit_story_child_view_screen.dart';
import 'package:tfg_app/view/screens/select_image_screen.dart';
import 'package:tfg_app/view/widgets/loading.dart';
import 'package:tfg_app/view/widgets/loading_story.dart';
import 'package:tfg_app/view_model/create_story_view_model.dart';
import 'package:tfg_app/view_model/read_story_view_model.dart';
import 'package:flutter/services.dart';
import 'package:file_picker/file_picker.dart';

class CreateStoryPage extends StatefulWidget {
  final String storyID;
  final bool editing;
  final Story storyInfo;
  final String image;
  CreateStoryPage(
      {Key key, this.storyID, this.editing, this.storyInfo, this.image})
      : super(key: key);

  @override
  _CreateStoryPageState createState() {
    return _CreateStoryPageState(
        storyID: storyID, editing: editing, storyInfo: storyInfo, image: image);
  }
}

class _CreateStoryPageState extends State<CreateStoryPage> {
  String storyID;
  bool editing;
  Story storyInfo;
  String image;
  _CreateStoryPageState(
      {this.storyID, this.editing, this.storyInfo, this.image});

  CreateStoryViewModel _createStoryViewModel = CreateStoryViewModel();
  final _formKey = GlobalKey<FormState>();

  String _fileName = '...';
  String _path = '...';

  Story story;
  String title = '';
  String author = '';
  String genre = '';
  String front = '';
  String error = '';
  String errorLectura = '';
  String errorStory = '';
  String genreImage = '';
  bool loading = false;
  MediaQueryData queryData;

  @override
  void initState() {
    super.initState();
    _createStoryViewModel = new CreateStoryViewModel();

    if (storyInfo != null) {
      setState(() {
        title = storyInfo.title;
        author = storyInfo.author;
        genre = storyInfo.genre;
        front = storyInfo.front;
      });
    }

    setState(() {
      errorLectura = '';
      error = '';
    });

    setState(() {
      loading = false;
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);

    var screenSize = queryData.size;

    void _openFileExplorer() async {
      try {
        _path = await FilePicker.getFilePath(
            type: FileType.custom, allowedExtensions: ['txt']);
      } on PlatformException catch (e) {
        print("Operación no soportada" + e.toString());
      }

      if (!mounted) return;

      setState(() {
        _fileName = _path != null ? _path.split('/').last : '...';
      });
    }

    if (screenSize.height < 925) {
      Future<void> _showMyDialog() async {
        return showDialog<void>(
          context: context,
          barrierDismissible: false, // user must tap button!
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Text('Ayuda en la creación',
                      style: TextStyle(fontWeight: FontWeight.bold, color: HexColor('#071A40')))),
              content: SizedBox(
                height: 130,
                child: Column(
                  children: [
                    Text(
                        'Para crear un cuento debe importar un fichero txt con el siguiente formato: ',
                        style: TextStyle(fontWeight: FontWeight.bold, color: HexColor('#3C3C3C'))),
                    Text(''),
                    Text(
                        'Cada línea del fichero será una página del cuento y deberá contener un máximo de diez palabras. '),
                  ],
                ),
              ),
              actions: [
                TextButton(
                  child: Text('Aceptar',
                      style: TextStyle(color: HexColor('#0D518C'))),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      }

      Future<void> _showEditDialog() async {
        return showDialog<void>(
          context: context,
          barrierDismissible: false, // user must tap button!
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Text('Edición de cuentos',
                    style: TextStyle(fontWeight: FontWeight.bold)),
              ),
              content: SizedBox(
                height: 100,
                child: Column(
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => AddPictoToTextPage(
                                  storyID: storyID, editing: editing),
                            ));
                      },
                      child: Row(
                        children: [
                          Icon(Icons.handyman,
                              size: 40, color: HexColor('#0D518C')),
                          SizedBox(width: 10),
                          Text('Editar de forma manual ',
                              style: TextStyle(fontSize: 18)),
                        ],
                      ),
                    ),
                    SizedBox(height: 20),
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) =>
                                  EditChildViewPage(storyID: storyID),
                            ));
                      },
                      child: Row(
                        children: [
                          Icon(Icons.child_care,
                              size: 40, color: HexColor('#0D518C')),
                          SizedBox(width: 10),
                          Text('Editar desde vista de lectura',
                              style: TextStyle(fontSize: 18)),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              actions: [
                TextButton(
                  child: Text('Cancelar',
                      style: TextStyle(color: HexColor('#0D518C'))),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      }

      return Scaffold(
        appBar: AppBar(
          title:
              (editing == true ? Text('Editar cuento') : Text('Crear cuento')),
          backgroundColor: HexColor('#0D518C'),
          centerTitle: true,
          automaticallyImplyLeading: false,
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 50, horizontal: 50),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  (editing != null
                      // Estamos editando el cuento
                      ? (front == '' || front == null
                          ? IconButton(
                              icon: Icon(Icons.camera_alt),
                              onPressed: () async {
                                var frontSelected = await Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => SelectImagePage(
                                              storyInfo: storyInfo,
                                            )));
                                setState(() {
                                  front = frontSelected;
                                });
                              },
                              iconSize: 90,
                              color: Colors.grey)
                          : Container(
                              child: GestureDetector(
                                  onTap: () async {
                                    var frontSelected = await Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                SelectImagePage(
                                                  storyInfo: storyInfo,
                                                )));
                                    setState(() {
                                      front = frontSelected;
                                    });
                                  },
                                  child: Container(
                                      height: 200,
                                      child:
                                          Image.network(front, scale: 1.0)))))
                      // Cuando estamos creando el cuento
                      : (image == '' || image == null
                          ? IconButton(
                              icon: Icon(Icons.camera_alt),
                              onPressed: () async {
                                var imageSelected = await Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => SelectImagePage(
                                              frontAddScreen: image,
                                            )));
                                setState(() {
                                  image = imageSelected;
                                });
                                setState(() {
                                  error = '';
                                });
                              },
                              iconSize: 90,
                              color: Colors.grey)
                          : Container(
                              child: GestureDetector(
                                  onTap: () async {
                                    var imageSelected = await Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                SelectImagePage(
                                                  frontAddScreen: image,
                                                )));
                                    setState(() {
                                      image = imageSelected;
                                    });
                                    setState(() {
                                      error = '';
                                    });
                                  },
                                  child: Container(
                                      height: 150,
                                      child:
                                          Image.network(image, scale: 1.0)))))),
                  SizedBox(height: 12.0),
                  Text(
                    error,
                    style: TextStyle(color: Colors.red, fontSize: 14.0),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 10),
                  TextFormField(
                    initialValue: (editing != null ? title : ''),
                    decoration: InputDecoration(
                        hintText: 'Título', prefixIcon: Icon(Icons.menu_book)),
                    validator: (val) =>
                        val.isEmpty ? 'Introduzca un título' : null,
                    onChanged: (value) {
                      setState(() => title = value);
                    },
                  ),
                  SizedBox(height: 10),
                  TextFormField(
                    initialValue: (editing != null ? genre : ''),
                    decoration: InputDecoration(
                        hintText: 'Género',
                        prefixIcon: Icon(Icons.my_library_books_outlined)),
                    validator: (val) =>
                        val.isEmpty ? 'Introduzca un género' : null,
                    onChanged: (value) {
                      setState(() => genre = value);
                    },
                  ),
                  SizedBox(height: 10),
                  TextFormField(
                    initialValue: (editing != null ? author : ''),
                    decoration: InputDecoration(
                      hintText: 'Autor/a',
                      prefixIcon: Icon(Icons.person_pin),
                    ),
                    validator: (val) =>
                        val.isEmpty ? 'Introduzca un/a autor/a' : null,
                    onChanged: (value) {
                      setState(() => author = value);
                    },
                  ),
                  SizedBox(height: 20),
                  (editing == null
                      ? Column(
                          children: [
                            GestureDetector(
                                onTap: _showMyDialog,
                                child: Icon(Icons.help_outline,
                                    size: 20, color: HexColor('#0F8DBF'))),
                            new Padding(
                              padding:
                                  const EdgeInsets.only(top: 5, bottom: 10.0),
                              child: new RaisedButton(
                                onPressed: () => _openFileExplorer(),
                                child: new Text("Importar cuento",
                                    style:
                                        TextStyle(color: Colors.black)),
                                color: HexColor('#F2D338'),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 10.0),
                              child: new Text(
                                'Nombre del archivo importado ',
                                textAlign: TextAlign.center,
                                style:
                                    new TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                            Text(
                              _fileName,
                              textAlign: TextAlign.center,
                            )
                          ],
                        )
                      : SizedBox(height: 0)),
                  SizedBox(height: 10),
                  Text(
                    errorStory,
                    style: TextStyle(color: Colors.red, fontSize: 14.0),
                    textAlign: TextAlign.center,
                  ),
                  (editing != null
                      ? RaisedButton(
                          child: Text(
                            'Editar cuento',
                            style: TextStyle(color: Colors.white),
                          ),
                          color: HexColor('#0F8DBF'),
                          onPressed: () async {
                            if (_formKey.currentState.validate()) {
                              var listGenresImages = await _createStoryViewModel
                                  .getPictogramsByKeywords(genre);

                              if (!listGenresImages.isEmpty) {
                                setState(() {
                                  genreImage =
                                      'https://api.arasaac.org/api/pictograms/' +
                                          listGenresImages.elementAt(0);
                                });
                              } else {
                                setState(() {
                                  genreImage = null;
                                });
                              }

                              setState(() {
                                story = Story(
                                    title: title,
                                    genre: genre,
                                    author: author,
                                    front: front,
                                    genreImage: genreImage);
                              });
                              await _createStoryViewModel.updateStory(
                                  story, storyID);
                            }

                            _showEditDialog();

                            /*Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => AddPictoToTextPage(
                                      storyID: storyID, editing: editing),
                                ));*/
                          })
                      : Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            RaisedButton(
                                child: Text(
                                  'Continuar',
                                  style: TextStyle(color: Colors.white),
                                ),
                                color: HexColor('#0F8DBF'),
                                onPressed: () async {
                                  if (_formKey.currentState.validate() &&
                                      image != null &&
                                      _path != '...' &&
                                      _path != null) {
                                    print(_path);
                                    var listGenresImages =
                                        await _createStoryViewModel
                                            .getPictogramsByKeywords(genre);

                                    if (!listGenresImages.isEmpty) {
                                      setState(() {
                                        genreImage =
                                            'https://api.arasaac.org/api/pictograms/' +
                                                listGenresImages.elementAt(0);
                                      });
                                    } else {
                                      setState(() {
                                        genreImage = null;
                                      });
                                    }

                                    setState(() {
                                      story = Story(
                                          title: title,
                                          genre: genre,
                                          author: author,
                                          numPages: "0",
                                          front: image,
                                          genreImage: genreImage);
                                    });
                                    storyID = await _createStoryViewModel
                                        .createStory(story);

                                    if (_path != null) {
                                      File file = File(_path);
                                      setState(() {
                                        loading = true;
                                      });

                                      var a = null;
                                      try {
                                        a = await file.readAsLines(
                                            encoding: utf8);
                                      } catch (e) {
                                        print(
                                            'ERROR: Fallo en la lectura del archivo');
                                      }

                                      if (a != null) {
                                        for (int i = 0; i < a.length; i++) {
                                          var page = a.elementAt(i).split(' ');
                                          await _createStoryViewModel
                                              .addPageToStory(page,
                                                  (i + 1).toString(), storyID);
                                        }

                                        await _createStoryViewModel
                                            .updateStoryNumPages(
                                                storyID, a.length);

                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) =>
                                                  AddPictoToTextPage(
                                                      storyID: storyID,
                                                      editing: editing),
                                            ));
                                      } else {
                                        setState(() {
                                          errorLectura =
                                              'Error al leer al archivo';
                                        });
                                      }
                                    }

                                    setState(() {
                                      loading = false;
                                    });
                                  } else if (image == "" || image == null) {
                                    setState(() {
                                      error =
                                          'Seleccione una portada para el cuento';
                                    });
                                  } else if (_path == "..." || _path == null) {
                                    setState(() {
                                      error = '';
                                    });
                                    setState(() {
                                      errorStory =
                                          'Importe un cuento para continuar';
                                    });
                                  }
                                }),
                            SizedBox(width: 20),
                            Text(errorLectura,
                                style: TextStyle(color: Colors.red)),
                            (loading ? LoadingStory() : SizedBox(height: 0))
                          ],
                        )),
                ],
              ),
            ),
          ),
        ),
      );
    } else {
      Future<void> _showMyDialog() async {
        return showDialog<void>(
          context: context,
          barrierDismissible: false, // user must tap button!
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(child: Text('Ayuda en la creación', style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold, color: HexColor('#071A40')))),
              content: SizedBox(
                height: 160,
                child: Column(
                  children: [
                    Text(
                        'Para crear un cuento debe importar un fichero txt con el siguiente formato: ',
                        style: TextStyle(fontWeight: FontWeight.bold, color: HexColor('#3C3C3C'), fontSize: 24)),
                    Text(''),
                    Text(
                        'cada línea del fichero será una página del cuento y deberá contener un máximo de diez palabras. ', style: TextStyle(fontSize: 24)),
                  ],
                ),
              ),
              actions: [
                TextButton(
                  child: Text('Aceptar', style: TextStyle(fontSize: 26)),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      }

      Future<void> _showEditDialog() async {
        return showDialog<void>(
          context: context,
          barrierDismissible: false, // user must tap button!
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Text('Edición de cuentos',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 26)),
              ),
              content: SizedBox(
                height: 200,
                width: 600,
                child: Column(
                  children: [
                    SizedBox(height: 20),
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => AddPictoToTextPage(
                                  storyID: storyID, editing: editing),
                            ));
                      },
                      child: Row(
                        children: [
                          Icon(Icons.handyman,
                              size: 70, color: HexColor('#0D518C')),
                          SizedBox(width: 10),
                          Text('Editar de forma manual ',
                              style: TextStyle(fontSize: 24)),
                        ],
                      ),
                    ),
                    SizedBox(height: 20),
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) =>
                                  EditChildViewPage(storyID: storyID),
                            ));
                      },
                      child: Row(
                        children: [
                          Icon(Icons.child_care,
                              size: 70, color: HexColor('#0D518C')),
                          SizedBox(width: 10),
                          Text('Editar desde la vista de lectura',
                              style: TextStyle(fontSize: 24)),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              actions: [
                TextButton(
                  child: Text('Cancelar',
                      style: TextStyle(color: HexColor('#0D518C'), fontSize: 26)),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      }

      return Scaffold(
        appBar: AppBar(
          title: (editing == true
              ? Text('Editar cuento', style: TextStyle(fontSize: 26))
              : Text('Crear cuento', style: TextStyle(fontSize: 26))),
          backgroundColor: HexColor('#0D518C'),
          centerTitle: true,
          automaticallyImplyLeading: false,
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 50, horizontal: 50),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  (editing != null
                      // Estamos editando el cuento
                      ? (front == '' || front == null
                          ? IconButton(
                              icon: Icon(
                                Icons.camera_alt,
                                size: 200,
                              ),
                              onPressed: () async {
                                var frontSelected = await Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => SelectImagePage(
                                              storyInfo: storyInfo,
                                            )));
                                setState(() {
                                  front = frontSelected;
                                });
                              },
                              iconSize: 90,
                              color: Colors.grey)
                          : Container(
                              child: GestureDetector(
                                  onTap: () async {
                                    var frontSelected = await Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                SelectImagePage(
                                                  storyInfo: storyInfo,
                                                )));
                                    setState(() {
                                      front = frontSelected;
                                    });
                                  },
                                  child: Container(
                                      height: 400,
                                      child:
                                          Image.network(front, scale: 1.0)))))
                      // Cuando estamos creando el cuento
                      : (image == '' || image == null
                          ? IconButton(
                              icon: Icon(Icons.camera_alt, size: 140),
                              onPressed: () async {
                                var imageSelected = await Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => SelectImagePage(
                                              frontAddScreen: image,
                                            )));
                                setState(() {
                                  image = imageSelected;
                                });
                                setState(() {
                                  error = '';
                                });
                              },
                              iconSize: 90,
                              color: Colors.grey)
                          : Container(
                              child: GestureDetector(
                                  onTap: () async {
                                    var imageSelected = await Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                SelectImagePage(
                                                  frontAddScreen: image,
                                                )));
                                    setState(() {
                                      image = imageSelected;
                                    });
                                    setState(() {
                                      error = '';
                                    });
                                  },
                                  child: Container(
                                      height: 400,
                                      child:
                                          Image.network(image, scale: 1.0)))))),
                  SizedBox(height: 20.0),
                  Text(
                    error,
                    style: TextStyle(color: Colors.red, fontSize: 14.0),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 40),
                  TextFormField(
                    style: TextStyle(fontSize: 22),
                    initialValue: (editing != null ? title : ''),
                    decoration: InputDecoration(
                        hintText: 'Título',
                        prefixIcon: Icon(Icons.menu_book, size: 30)),
                    validator: (val) =>
                        val.isEmpty ? 'Introduzca un título' : null,
                    onChanged: (value) {
                      setState(() => title = value);
                    },
                  ),
                  SizedBox(height: 10),
                  TextFormField(
                    style: TextStyle(fontSize: 22),
                    initialValue: (editing != null ? genre : ''),
                    decoration: InputDecoration(
                        hintText: 'Género',
                        prefixIcon:
                            Icon(Icons.my_library_books_outlined, size: 30)),
                    validator: (val) =>
                        val.isEmpty ? 'Introduzca un género' : null,
                    onChanged: (value) {
                      setState(() => genre = value);
                    },
                  ),
                  SizedBox(height: 10),
                  TextFormField(
                    style: TextStyle(fontSize: 22),
                    initialValue: (editing != null ? author : ''),
                    decoration: InputDecoration(
                      hintText: 'Autor/a',
                      prefixIcon: Icon(Icons.person_pin, size: 30),
                    ),
                    validator: (val) =>
                        val.isEmpty ? 'Introduzca un/a autor/a' : null,
                    onChanged: (value) {
                      setState(() => author = value);
                    },
                  ),
                  SizedBox(height: 10),
                  (editing == null
                      ? Column(
                          children: [
                            GestureDetector(
                                onTap: _showMyDialog,
                                child: Icon(Icons.help_outline,
                                    size: 40, color: HexColor('#0F8DBF'))),
                            new Padding(
                              padding: const EdgeInsets.only(
                                  top: 10.0, bottom: 10.0),
                              child: ButtonTheme(
                                height: 50,
                                child: new RaisedButton(
                                  onPressed: () => _openFileExplorer(),
                                  child: new Text("Importar cuento",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 22)),
                                  color: HexColor('#F2D338'),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 10.0),
                              child: new Text(
                                'Nombre del archivo importado ',
                                textAlign: TextAlign.center,
                                style: new TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 22),
                              ),
                            ),
                            Text(
                              _fileName,
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 22),
                            )
                          ],
                        )
                      : SizedBox(height: 0)),
                  SizedBox(height: 10),
                  Text(
                    errorStory,
                    style: TextStyle(color: Colors.red, fontSize: 20),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 120),
                  (editing != null
                      ? Column(
                          children: [
                            ButtonTheme(
                              height: 50,
                              minWidth: 200,
                              child: RaisedButton(
                                  child: Text(
                                    'Editar cuento',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 22),
                                  ),
                                  color: HexColor('#0F8DBF'),
                                  onPressed: () async {
                                    if (_formKey.currentState.validate()) {
                                      var listGenresImages =
                                          await _createStoryViewModel
                                              .getPictogramsByKeywords(genre);

                                      if (!listGenresImages.isEmpty) {
                                        setState(() {
                                          genreImage =
                                              'https://api.arasaac.org/api/pictograms/' +
                                                  listGenresImages.elementAt(0);
                                        });
                                      } else {
                                        setState(() {
                                          genreImage = null;
                                        });
                                      }

                                      setState(() {
                                        story = Story(
                                            title: title,
                                            genre: genre,
                                            author: author,
                                            front: front,
                                            genreImage: genreImage,
                                            storyID: storyID);
                                      });
                                      await _createStoryViewModel.updateStory(
                                          story, storyID);
                                    }

                                    _showEditDialog();
                                    /*Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              AddPictoToTextPage(
                                                  storyID: storyID,
                                                  editing: editing),
                                        ));*/
                                  }),
                            ),
                          ],
                        )
                      : Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ButtonTheme(
                              height: 60,
                              minWidth: 200,
                              child: RaisedButton(
                                  child: Text(
                                    'Continuar',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 22),
                                  ),
                                  color: HexColor('#0F8DBF'),
                                  onPressed: () async {
                                    if (_formKey.currentState.validate() &&
                                        image != null &&
                                        _path != '...' &&
                                        _path != null) {
                                      var listGenresImages =
                                          await _createStoryViewModel
                                              .getPictogramsByKeywords(genre);

                                      if (!listGenresImages.isEmpty) {
                                        setState(() {
                                          genreImage =
                                              'https://api.arasaac.org/api/pictograms/' +
                                                  listGenresImages.elementAt(0);
                                        });
                                      } else {
                                        setState(() {
                                          genreImage = null;
                                        });
                                      }

                                      setState(() {
                                        story = Story(
                                            title: title,
                                            genre: genre,
                                            author: author,
                                            numPages: "0",
                                            front: image,
                                            genreImage: genreImage);
                                      });
                                      storyID = await _createStoryViewModel
                                          .createStory(story);

                                      if (_path != null) {
                                        File file = File(_path);
                                        setState(() {
                                          loading = true;
                                        });

                                        var a = null;
                                        try {
                                          a = await file.readAsLines(
                                              encoding: utf8);
                                        } catch (e) {
                                          print(
                                              'ERROR: Fallo en la lectura del archivo');
                                        }

                                        if (a != null) {
                                          for (int i = 0; i < a.length; i++) {
                                            var page =
                                                a.elementAt(i).split(' ');
                                            await _createStoryViewModel
                                                .addPageToStory(
                                                    page,
                                                    (i + 1).toString(),
                                                    storyID);
                                          }

                                          await _createStoryViewModel
                                              .updateStoryNumPages(
                                                  storyID, a.length);

                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) =>
                                                    AddPictoToTextPage(
                                                        storyID: storyID,
                                                        editing: editing),
                                              ));
                                        } else {
                                          setState(() {
                                            errorLectura =
                                                'Error al leer al archivo';
                                          });
                                        }
                                      }

                                      setState(() {
                                        loading = false;
                                      });
                                    } else if (image == "" || image == null) {
                                      setState(() {
                                        error =
                                            'Seleccione una portada para el cuento';
                                      });
                                    } else if (_path == "..." ||
                                        _path == null) {
                                      setState(() {
                                        error = '';
                                      });
                                      setState(() {
                                        errorStory =
                                            'Importe un cuento para continuar';
                                      });
                                    }
                                  }),
                            ),
                            SizedBox(width: 20),
                            Text(errorLectura,
                                style:
                                    TextStyle(color: Colors.red, fontSize: 22)),
                            (loading ? LoadingStory() : SizedBox(height: 0))
                          ],
                        )),
                ],
              ),
            ),
          ),
        ),
      );
    }
  }
}
