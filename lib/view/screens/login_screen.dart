import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:tfg_app/view/widgets/loading.dart';
import 'package:tfg_app/view/widgets/textInputDecoration.dart';
import 'package:tfg_app/view_model/sign_up_view_model.dart';

class LogIn extends StatefulWidget {
  LogIn({Key key}) : super(key: key);

  @override
  _LogInState createState() {
    return _LogInState();
  }
}

class _LogInState extends State<LogIn> {
  // Variable clave para realizar la validación del formulario
  final _formKey = GlobalKey<FormState>();

  // Variables internas del formulario
  bool _passwordVisibility = true;
  String email = '';
  String password = '';
  bool loading = false;
  String error = '';
  MediaQueryData queryData;

  SignUpViewModel _viewModel = SignUpViewModel();

  @override
  void initState() {
    super.initState();
    _viewModel = new SignUpViewModel();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _showPassword() {
    setState(() {
      _passwordVisibility = !_passwordVisibility;
    });
  }

  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);

    var screenSize = queryData.size;
    //print(queryData.size);

    if (screenSize.height < 925) {
      return loading
          ? Loading()
          : Scaffold(
              body: Container(
                padding: EdgeInsets.symmetric(vertical: 50, horizontal: 10),
                child: Form(
                  key: _formKey,
                  child: Center(
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset("assets/portada.jpg", height: 275),
                          SizedBox(height: 20),
                          SingleChildScrollView(
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 30, horizontal: 20),
                              alignment: Alignment.center,
                              width: 300,
                              height: 290,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                border: Border.all(color: Colors.grey),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.1),
                                    spreadRadius: 5,
                                    blurRadius: 7,
                                    offset: Offset(
                                        0, 3), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Column(
                                children: [
                                  SizedBox(height: 20),
                                  TextFormField(
                                    decoration: textInputDecoration.copyWith(
                                        hintText: 'Correo',
                                        prefixIcon: Icon(Icons.email)),
                                    validator: (val) => val.isEmpty
                                        ? 'Introduzca un correo'
                                        : null,
                                    onChanged: (value) {
                                      setState(() => email = value);
                                    },
                                    keyboardType: TextInputType.emailAddress,
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  TextFormField(
                                    decoration: textInputDecoration.copyWith(
                                        hintText: 'Contraseña',
                                        prefixIcon: Icon(Icons.vpn_key_rounded),
                                        suffixIcon: IconButton(
                                            icon: Icon(
                                              _passwordVisibility
                                                  ? Icons.visibility_off
                                                  : Icons.visibility,
                                            ),
                                            onPressed: _showPassword)),
                                    obscureText: _passwordVisibility,
                                    validator: (val) => val.length < 6
                                        ? 'Introduzca una contraseña de más de 6 caracteres'
                                        : null,
                                    onChanged: (value) {
                                      setState(() => password = value);
                                    },
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  ButtonTheme(
                                    minWidth: 200,
                                    child: RaisedButton(
                                        child: Text('Iniciar Sesión'),
                                        textColor: Colors.white,
                                        color: HexColor('#72CEF2'),
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(18.0),
                                        ),
                                        onPressed: () async {
                                          if (_formKey.currentState
                                              .validate()) {
                                            setState(() => loading = true);
                                            dynamic result = await _viewModel
                                                .signInUser(email, password);
                                            if (result == null) {
                                              setState(() {
                                                error =
                                                    'No se ha podido iniciar sesión. Credenciales incorrectos.';
                                                loading = false;
                                              });
                                            } else {
                                              Navigator.pushReplacementNamed(
                                                  context, "/select-user");
                                            }
                                          }
                                        }),
                                  ),
                                  /**/
                                ],
                              ),
                            ),
                          ),
                          Column(
                            children: [
                              SizedBox(height: 12.0),
                              Text(
                                error,
                                style: TextStyle(
                                    color: Colors.red, fontSize: 14.0),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(height: 8.0),
                              Text('¿No tienes usuario todavía?'),
                              FlatButton(
                                  onPressed: () {
                                    Navigator.pushNamed(context, "/register");
                                  },
                                  textColor: HexColor('#0D518C'),
                                  child: Text('Regístrate aquí'))
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            );
    } else {
      return loading
          ? Loading()
          : Scaffold(
              body: Container(
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 50),
                child: Form(
                  key: _formKey,
                  child: Center(
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset("assets/portada.jpg", height: 450),
                          SizedBox(height: 30),
                          SingleChildScrollView(
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 40, horizontal: 30),
                              alignment: Alignment.center,
                              width: 700,
                              height: 500,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                border: Border.all(color: Colors.grey),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.1),
                                    spreadRadius: 5,
                                    blurRadius: 7,
                                    offset: Offset(
                                        0, 3), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SizedBox(height: 20),
                                  Container(
                                    height: 100,
                                    width: 500,
                                    child: TextFormField(
                                      style: TextStyle(fontSize: 24),
                                      decoration: textInputDecoration.copyWith(
                                          hintText: 'Correo',
                                          prefixIcon: Icon(Icons.email)),
                                      validator: (val) => val.isEmpty
                                          ? 'Introduzca un correo'
                                          : null,
                                      onChanged: (value) {
                                        setState(() => email = value);
                                      },
                                      keyboardType: TextInputType.emailAddress,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Container(
                                    height: 100,
                                    width: 500,
                                    child: TextFormField(
                                      style: TextStyle(fontSize: 24),
                                      decoration: textInputDecoration.copyWith(
                                          hintText: 'Contraseña',
                                          prefixIcon:
                                              Icon(Icons.vpn_key_rounded),
                                          suffixIcon: IconButton(
                                              icon: Icon(
                                                _passwordVisibility
                                                    ? Icons.visibility_off
                                                    : Icons.visibility,
                                              ),
                                              onPressed: _showPassword)),
                                      obscureText: _passwordVisibility,
                                      validator: (val) => val.length < 6
                                          ? 'Introduzca una contraseña de más de 6 caracteres'
                                          : null,
                                      onChanged: (value) {
                                        setState(() => password = value);
                                      },
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  ButtonTheme(
                                    minWidth: 400,
                                    height: 50,
                                    child: RaisedButton(
                                        child: Text(
                                          'Iniciar Sesión',
                                          style: TextStyle(fontSize: 24),
                                        ),
                                        textColor: Colors.white,
                                        color: HexColor('#72CEF2'),
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(20.0),
                                        ),
                                        onPressed: () async {
                                          if (_formKey.currentState
                                              .validate()) {
                                            setState(() => loading = true);
                                            dynamic result = await _viewModel
                                                .signInUser(email, password);
                                            if (result == null) {
                                              setState(() {
                                                error =
                                                    'No se ha podido iniciar sesión. Credenciales incorrectos.';
                                                loading = false;
                                              });
                                            } else {
                                              Navigator.pushReplacementNamed(
                                                  context, "/select-user");
                                            }
                                          }
                                        }),
                                  ),
                                  /**/
                                ],
                              ),
                            ),
                          ),
                          Column(
                            children: [
                              SizedBox(height: 12.0),
                              Text(
                                error,
                                style: TextStyle(
                                    color: Colors.red, fontSize: 18.0),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(height: 12.0),
                              Text(
                                '¿No tienes usuario todavía?',
                                style: TextStyle(fontSize: 22),
                              ),
                              FlatButton(
                                  onPressed: () {
                                    Navigator.pushNamed(context, "/register");
                                  },
                                  textColor: HexColor('#0D518C'),
                                  child: Text('Regístrate aquí',
                                      style: TextStyle(fontSize: 22)))
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            );
    }
  }
}
