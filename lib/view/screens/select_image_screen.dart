import 'dart:io';

import 'package:animated_search_bar/animated_search_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:image_picker/image_picker.dart';
import 'package:tfg_app/model/story_page.dart';
import 'package:tfg_app/model/story.dart';
import 'package:tfg_app/view/screens/create_story_screen.dart';
import 'package:tfg_app/view/widgets/loading.dart';
import 'package:tfg_app/view_model/create_story_view_model.dart';
import 'package:tfg_app/view_model/read_story_view_model.dart';

class SelectImagePage extends StatefulWidget {
  final Story storyInfo;
  final StoryPage pageInfo;
  final String frontAddScreen;
  final String pictoEdit;
  SelectImagePage(
      {Key key,
      this.storyInfo,
      this.frontAddScreen,
      this.pageInfo,
      this.pictoEdit})
      : super(key: key);

  @override
  _SelectImagePageState createState() {
    return _SelectImagePageState(
        storyInfo: storyInfo,
        frontAddScreen: frontAddScreen,
        pageInfo: pageInfo,
        pictoEdit: pictoEdit);
  }
}

class _SelectImagePageState extends State<SelectImagePage> {
  Story storyInfo;
  String frontAddScreen;
  String pictoEdit;
  StoryPage pageInfo;
  _SelectImagePageState(
      {this.storyInfo, this.frontAddScreen, this.pageInfo, this.pictoEdit});

  final ImagePicker _picker = ImagePicker();
  File _image;
  ReadStoryViewModel _readStoryViewModel = ReadStoryViewModel();
  CreateStoryViewModel _createStoryViewModel = CreateStoryViewModel();

  String title = '';
  String author = '';
  String genre = '';
  String front = '';
  List<String> idsList = List<String>();
  List<String> allIdsList = List<String>();
  bool loading = false;
  String pageImage = '';
  MediaQueryData queryData;

  @override
  void initState() {
    super.initState();
    _readStoryViewModel = new ReadStoryViewModel();
    _createStoryViewModel = new CreateStoryViewModel();

    if (storyInfo != null) {
      setState(() {
        front = storyInfo.front;
        title = storyInfo.title;
        author = storyInfo.author;
        genre = storyInfo.genre;
      });
    }

    if (pageInfo != null) {
      setState(() {
        pageImage = pageInfo.image;
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);

    var screenSize = queryData.size;

    if (screenSize.height < 925) {
      return loading
          ? Loading()
          : WillPopScope(
              onWillPop: _onWillScope,
              child: Scaffold(
                appBar: AppBar(
                  title: Text(
                    'Selección de imágenes',
                  ),
                  leading: IconButton(
                    icon: Icon(Icons.arrow_back, color: Colors.white),
                    onPressed: () {
                      if (storyInfo != null) {
                        Navigator.pop(context, front);
                      } else if (pageInfo != null) {
                        Navigator.pop(context, pageImage);
                      } else if (pictoEdit != null) {
                        Navigator.pop(context, pictoEdit);
                      } else if (frontAddScreen != null){
                        Navigator.pop(context, frontAddScreen);
                      } else {
                        Navigator.pop(context, null);
                      }
                    },
                  ),
                  centerTitle: true,
                  backgroundColor: HexColor('#0D518C'),
                ),
                body: Container(
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 55, top: 10, right: 30),
                        child: AnimatedSearchBar(
                          label: "Busca un pictograma",
                          labelStyle: TextStyle(
                              color: HexColor('#071A40'),
                              fontWeight: FontWeight.bold),
                          searchDecoration: InputDecoration(
                            hintText: "Buscar...",
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: HexColor('#0F8DBF'), width: 2.0)),
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: HexColor('#0F8DBF'), width: 2.0)),
                          ),
                          onChanged: (value) {
                            setState(() {
                              _createStoryViewModel
                                  .getPictogramsByKeywords(value)
                                  .then((value) {
                                setState(() {
                                  idsList = value;
                                });
                              });
                            });
                            print(idsList);
                          },
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            child: GestureDetector(
                              onTap: () =>
                                  _showPickImage(context, ImageSource.camera),
                              child: Container(
                                height: 80,
                                width: 120,
                                child: Card(
                                  color: Colors.white,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(Icons.camera_alt,
                                          color: HexColor('#0F8DBF')),
                                      Text('Cámara',
                                          style: TextStyle(
                                              color: HexColor('#0F8DBF')))
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            child: GestureDetector(
                              onTap: () =>
                                  _showPickImage(context, ImageSource.gallery),
                              child: Container(
                                height: 80,
                                width: 120,
                                child: Card(
                                  color: Colors.white,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.image,
                                        color: HexColor('#0F8DBF'),
                                      ),
                                      Text(
                                        'Galería',
                                        style: TextStyle(
                                            color: HexColor('#0F8DBF')),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      ((idsList == null)
                          ? Text('No se ha buscado ningún pictograma',
                              style: TextStyle(color: Colors.red))
                          : (idsList.length == 0)
                              ? Text('No se ha buscado ningún pictograma',
                                  style: TextStyle(color: Colors.red))
                              : Expanded(
                                  child: GridView.builder(
                                      gridDelegate:
                                          const SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: 3,
                                      ),
                                      itemCount: idsList.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        final id = idsList[index];
                                        return GestureDetector(
                                          onTap: () => Navigator.pop(context,
                                              'https://api.arasaac.org/api/pictograms/$id'),
                                          child: Container(
                                              height: 20,
                                              child: Card(
                                                child: Image.network(
                                                    'https://api.arasaac.org/api/pictograms/$id'),
                                              )),
                                        );
                                      }),
                                ))
                    ],
                  ),
                ),
              ),
            );
    } else {
      return loading
          ? Loading()
          : WillPopScope(
              onWillPop: _onWillScope,
              child: Scaffold(
                appBar: AppBar(
                  title: Text(
                    'Selección de imágenes',
                    style: TextStyle(fontSize: 26),
                  ),
                  leading: IconButton(
                    icon: Icon(Icons.arrow_back, color: Colors.white, size: 35),
                    onPressed: () {
                      if (storyInfo != null) {
                        Navigator.pop(context, front);
                      } else if (pageInfo != null) {
                        Navigator.pop(context, pageImage);
                      } else if (pictoEdit != null) {
                        Navigator.pop(context, pictoEdit);
                      } else if (frontAddScreen != null){
                        Navigator.pop(context, frontAddScreen);
                      } else {
                        Navigator.pop(context, null);
                      }
                    },
                  ),
                  centerTitle: true,
                  backgroundColor: HexColor('#0D518C'),
                ),
                body: Container(
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 80, top: 40, right: 55),
                        child: AnimatedSearchBar(
                          searchStyle: TextStyle(fontSize: 22),
                          label: "Busca un pictograma",
                          labelStyle: TextStyle(
                              color: HexColor('#071A40'),
                              fontSize: 22,
                              fontWeight: FontWeight.bold),
                          searchDecoration: InputDecoration(
                            hintText: "Buscar...",
                            hintStyle: TextStyle(fontSize: 22),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: HexColor('#0F8DBF'), width: 3.0)),
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: HexColor('#0F8DBF'), width: 3.0)),
                          ),
                          onChanged: (value) {
                            setState(() {
                              _createStoryViewModel
                                  .getPictogramsByKeywords(value)
                                  .then((value) {
                                setState(() {
                                  idsList = value;
                                });
                              });
                            });
                          },
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            child: GestureDetector(
                              onTap: () =>
                                  _showPickImage(context, ImageSource.camera),
                              child: Container(
                                height: 150,
                                width: 120,
                                child: Card(
                                  color: Colors.white,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(Icons.camera_alt,
                                          color: HexColor('#0F8DBF'), size: 60),
                                      Text('Cámara',
                                          style: TextStyle(
                                              color: HexColor('#0F8DBF'),
                                              fontSize: 20))
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            child: GestureDetector(
                              onTap: () =>
                                  _showPickImage(context, ImageSource.gallery),
                              child: Container(
                                height: 150,
                                width: 120,
                                child: Card(
                                  color: Colors.white,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(Icons.image,
                                          color: HexColor('#0F8DBF'), size: 60),
                                      Text(
                                        'Galería',
                                        style: TextStyle(
                                            color: HexColor('#0F8DBF'),
                                            fontSize: 20),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 20),
                      ((idsList == null)
                          ? Text('No se ha buscado ningún pictograma',
                              style: TextStyle(color: Colors.red, fontSize: 22))
                          : (idsList.length == 0)
                              ? Text('No se ha buscado ningún pictograma',
                                  style: TextStyle(
                                      color: Colors.red, fontSize: 22))
                              : Expanded(
                                  child: GridView.builder(
                                      gridDelegate:
                                          const SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: 3,
                                      ),
                                      itemCount: idsList.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        final id = idsList[index];
                                        return GestureDetector(
                                          onTap: () => Navigator.pop(context,
                                              'https://api.arasaac.org/api/pictograms/$id'),
                                          child: Container(
                                              height: 20,
                                              child: Card(
                                                child: Image.network(
                                                    'https://api.arasaac.org/api/pictograms/$id'),
                                              )),
                                        );
                                      }),
                                ))
                    ],
                  ),
                ),
              ),
            );
    }
  }

  void _showPickImage(BuildContext context, source) async {
    var pickedImage = await _picker.getImage(source: source);
    String imageURL = '';

    setState(() {
      loading = true;
    });

    setState(() {
      if (pickedImage != null) {
        _image = File(pickedImage.path);
      } else {
        print('No image selected.');
      }
    });

    if (_image != null) {
      imageURL = await _createStoryViewModel.uploadFile(_image);
    }

    Navigator.pop(context, imageURL);
  }

  Future<bool> _onWillScope() {
    if (storyInfo != null) {
      Navigator.pop(context, front);
    } else if (pageInfo != null) {
      Navigator.pop(context, pageImage);
    } else if (pictoEdit != null) {
      Navigator.pop(context, pictoEdit);
    } else if (frontAddScreen != null){
      Navigator.pop(context, frontAddScreen);
    } else {
      Navigator.pop(context, null);
    }
    return Future.value(true);
  }
}
