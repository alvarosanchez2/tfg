import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:tfg_app/model/services/auth_service.dart';
import 'package:tfg_app/model/story.dart';
import 'package:tfg_app/view/screens/create_story_screen.dart';
import 'package:tfg_app/view/widgets/loading_story.dart';
import 'package:tfg_app/view/widgets/loading_users.dart';
import 'package:tfg_app/view/widgets/loading_words.dart';
import 'package:tfg_app/view_model/read_story_view_model.dart';

class ManageBooksPage extends StatefulWidget {
  ManageBooksPage({Key key}) : super(key: key);

  @override
  _ManageBooksPageState createState() {
    return _ManageBooksPageState();
  }
}

class _ManageBooksPageState extends State<ManageBooksPage> {
  ReadStoryViewModel _readStoryViewModel = ReadStoryViewModel();
  List<dynamic> storiesCreated = [];
  Story storyInfo;
  bool loading = false;
  MediaQueryData queryData;

  @override
  void initState() {
    super.initState();
    _readStoryViewModel = new ReadStoryViewModel();

    _readStoryViewModel.getStoriesCreated().then((List<dynamic> result) {
      setState(() {
        storiesCreated = result;
      });
    });

    setState(() {
      loading = false;
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);

    var screenSize = queryData.size;

    _readStoryViewModel.getStoriesCreated().then((List<dynamic> result) {
      if (mounted) {
        setState(() {
          storiesCreated = result;
        });
      }
    });

    if (screenSize.height < 925) {
      Future<void> _showMyDialog(String storyID) async {
        return showDialog<void>(
          context: context,
          barrierDismissible: false, // user must tap button!
          builder: (BuildContext context) {
            return StatefulBuilder(builder: (context, setState) {
              return AlertDialog(
                title: Center(child: Text('Eliminar cuento')),
                content: (loading == false
                    ? SingleChildScrollView(
                        child: ListBody(
                          children: [
                            Text(
                                '¿Está seguro/a de que quiere eliminar el cuento?')
                          ],
                        ),
                      )
                    : Container(height: 100,child: LoadingStory())),
                actions: [
                  TextButton(
                    child: (loading == false
                        ? Text('Aceptar')
                        : SizedBox(height: 0)),
                    onPressed: () async {
                      setState(() {
                        loading = true;
                      });
                      print(loading);
                      await _readStoryViewModel.deleteStory(storyID);
                      await _readStoryViewModel
                          .getStoriesCreated()
                          .then((List<dynamic> result) {
                        setState(() {
                          storiesCreated = result;
                        });
                      });
                      setState(() {
                        loading = false;
                      });
                      Navigator.of(context).pop();
                    },
                  ),
                  TextButton(
                    child: (loading == false
                        ? Text('Cancelar')
                        : SizedBox(height: 0)),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            });
          },
        );
      }

      return Scaffold(
        appBar: AppBar(
          title: Text('Gestionar cuentos'),
          backgroundColor: HexColor('#0D518C'),
          centerTitle: true,
          automaticallyImplyLeading: false,
        ),
        body: Column(
          children: [
            SizedBox(height: 10),
            Center(
                child: Text(
              'Listado de cuentos creados',
              style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: HexColor('#0D518C')),
            )),
            Container(
              margin: const EdgeInsets.only(left: 80.0, right: 80.0),
              child: Divider(
                color: HexColor('#071A40'),
                height: 16,
                thickness: 2,
              ),
            ),
            (storiesCreated.length == 0)
                ? Expanded(
                    child: Center(
                      child: Text('No se han creado cuentos todavía',
                          style: TextStyle(color: Colors.red)),
                    ),
                  )
                : Expanded(
                    child: ListView.builder(
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemCount: storiesCreated.length,
                        itemBuilder: (BuildContext context, int index) {
                          final story = storiesCreated[index];
                          return Column(
                            children: [
                              ListTile(
                                title: Text(story['title']),
                                leading: Icon(Icons.menu_book_rounded),
                                trailing: Container(
                                  width: 100,
                                  child: Row(
                                    children: [
                                      IconButton(
                                          icon: Icon(Icons.edit),
                                          color: HexColor('#72CEF2'),
                                          onPressed: () async {
                                            if (story['id'] != null) {
                                              await _readStoryViewModel
                                                  .getStory(story['id'])
                                                  .then((value) {
                                                setState(() {
                                                  storyInfo = value;
                                                });
                                              });
                                            }
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) =>
                                                      CreateStoryPage(
                                                          storyID: story['id'],
                                                          storyInfo: storyInfo,
                                                          editing: true),
                                                ));
                                          }),
                                      IconButton(
                                          icon: Icon(Icons.delete,
                                              color: HexColor('#72CEF2')),
                                          onPressed: () {
                                            _showMyDialog(story['id']);
                                          }),
                                    ],
                                  ),
                                ),
                              ),
                              Divider(
                                color: Colors.grey,
                                height: 12,
                              )
                            ],
                          );
                        }),
                  ),
            RaisedButton(
                onPressed: () => Navigator.pushNamed(context, '/create-story'),
                child: Text(
                  'Añadir cuento',
                  style: TextStyle(color: Colors.white),
                ),
                color: HexColor('#0F8DBF')),
            SizedBox(height: 20)
          ],
        ),
      );
    } else {
      Future<void> _showMyDialog(String storyID) async {
        return showDialog<void>(
          context: context,
          barrierDismissible: false, // user must tap button!
          builder: (BuildContext context) {
            return StatefulBuilder(builder: (context, setState) {
              return AlertDialog(
                title: Center(child: Text('Eliminar cuento', style: TextStyle(fontSize: 22))),
                content: (loading == false
                    ? SingleChildScrollView(
                        child: ListBody(
                          children: [
                            Text(
                                '¿Está seguro/a de que quiere eliminar el cuento?',
                                style: TextStyle(fontSize: 22))
                          ],
                        ),
                      )
                    : LoadingStory()),
                actions: [
                  TextButton(
                    child: (loading == false
                        ? Text('Aceptar', style: TextStyle(fontSize: 22))
                        : SizedBox(height: 0)),
                    onPressed: () async {
                      setState(() {
                        loading = true;
                      });
                      print(loading);
                      await _readStoryViewModel.deleteStory(storyID);
                      await _readStoryViewModel
                          .getStoriesCreated()
                          .then((List<dynamic> result) {
                        setState(() {
                          storiesCreated = result;
                        });
                      });
                      setState(() {
                        loading = false;
                      });
                      Navigator.of(context).pop();
                    },
                  ),
                  TextButton(
                    child: (loading == false
                        ? Text('Cancelar', style: TextStyle(fontSize: 22))
                        : SizedBox(height: 0)),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            });
          },
        );
      }

      return Scaffold(
        appBar: AppBar(
          title: Text('Gestionar cuentos', style: TextStyle(fontSize: 24)),
          backgroundColor: HexColor('#0D518C'),
          centerTitle: true,
          automaticallyImplyLeading: false,
        ),
        body: Column(
          children: [
            SizedBox(height: 10),
            Center(
                child: Text(
              'Listado de cuentos creados',
              style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                  color: HexColor('#0D518C')),
            )),
            Container(
              margin: const EdgeInsets.only(left: 100.0, right: 100.0),
              child: Divider(
                color: HexColor('#071A40'),
                height: 22,
                thickness: 2,
              ),
            ),
            (storiesCreated.length == 0)
                ? Expanded(
                    child: Center(
                      child: Text('No se han creado cuentos todavía',
                          style: TextStyle(color: Colors.red)),
                    ),
                  )
                : Expanded(
                    child: ListView.builder(
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemCount: storiesCreated.length,
                        itemBuilder: (BuildContext context, int index) {
                          final story = storiesCreated[index];
                          return Column(
                            children: [
                              ListTile(
                                title: Text(story['title'],
                                    style: TextStyle(fontSize: 24)),
                                leading:
                                    Icon(Icons.menu_book_rounded, size: 40),
                                trailing: Container(
                                  width: 100,
                                  child: Row(
                                    children: [
                                      IconButton(
                                          icon: Icon(Icons.edit, size: 40),
                                          color: HexColor('#72CEF2'),
                                          onPressed: () async {
                                            if (story['id'] != null) {
                                              await _readStoryViewModel
                                                  .getStory(story['id'])
                                                  .then((value) {
                                                setState(() {
                                                  storyInfo = value;
                                                });
                                              });
                                            }
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) =>
                                                      CreateStoryPage(
                                                          storyID: story['id'],
                                                          storyInfo: storyInfo,
                                                          editing: true),
                                                ));
                                          }),
                                      IconButton(
                                          icon: Icon(Icons.delete,
                                              color: HexColor('#72CEF2'),
                                              size: 40),
                                          onPressed: () {
                                            _showMyDialog(story['id']);
                                          }),
                                    ],
                                  ),
                                ),
                              ),
                              Divider(
                                color: Colors.grey,
                                height: 20,
                              )
                            ],
                          );
                        }),
                  ),
            ButtonTheme(
              height: 60,
              minWidth: 200,
              child: RaisedButton(
                  onPressed: () =>
                      Navigator.pushNamed(context, '/create-story'),
                  child: Text(
                    'Añadir cuento',
                    style: TextStyle(fontSize: 24, color: Colors.white),
                  ),
                  color: HexColor('#0F8DBF')),
            ),
            SizedBox(height: 60)
          ],
        ),
      );
    }
  }
}
