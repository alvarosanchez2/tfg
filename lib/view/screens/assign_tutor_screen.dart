import 'package:flutter/material.dart';
import 'package:tfg_app/model/educator.dart';
import 'package:tfg_app/model/tutor.dart';
import 'package:tfg_app/model/user.dart';
import 'package:tfg_app/view_model/sign_up_view_model.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';

class AssignTutorPage extends StatefulWidget {
  final String userID;
  AssignTutorPage({Key key, this.userID}) : super(key: key);

  @override
  _AssignTutorPageState createState() {
    return _AssignTutorPageState(userID: userID);
  }
}

class _AssignTutorPageState extends State<AssignTutorPage> {
  String userID;
  _AssignTutorPageState({this.userID});

  String selectedValue;
  final List<DropdownMenuItem> items = [];
  SignUpViewModel _viewModel = SignUpViewModel();
  List<dynamic> educatorsList = [];
  List<dynamic> educatorsListAssigned = [];
  String error = '';
  MediaQueryData queryData;

  @override
  void initState() {
    super.initState();
    _viewModel = new SignUpViewModel();

    _viewModel.getEducators().then((List<dynamic> result) {
      setState(() {
        result.forEach((educator) {
          items.add(DropdownMenuItem(
            child: Text(educator['name'] + " " + educator['fullname'],
                style: TextStyle(fontSize: 20)),
            value: educator['id'] +
                " " +
                educator['name'] +
                " " +
                educator['fullname'] +
                " " +
                educator['tutorType'],
          ));
        });
        educatorsList = result;
      });
    });

    _viewModel.getEducatorsAssigned(userID).then((List<dynamic> result) {
      setState(() {
        educatorsListAssigned = result;
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);

    var screenSize = queryData.size;

    if (screenSize.height < 925) {

      return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
              title: const Text("Asignación de tutores"),
              leading: IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.white),
                onPressed: () => Navigator.of(context).pop(),
              ),
              centerTitle: true,
              backgroundColor: HexColor('#0D518C')),
          body: Column(
            children: [
              Column(children: [
                Center(
                    child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                          side: BorderSide(
                            color: Colors.grey,
                            width: 1.0,
                          ),
                        ),
                        margin: EdgeInsets.all(20),
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            children: <Widget>[
                              Text("Lista de tutores:"),
                              SearchableDropdown.single(
                                items: items,
                                value: selectedValue,
                                hint: "Elige uno",
                                searchHint: "Elige uno",
                                onChanged: (value) {
                                  setState(() {
                                    print(value);
                                    selectedValue = value;
                                  });
                                },
                                isExpanded: true,
                              ),
                            ],
                          ),
                        )))
              ]),
              ButtonTheme(
                minWidth: 300,
                child: RaisedButton(
                  onPressed: ((selectedValue == null)
                      ? null
                      : () async {
                          var tutorInfo = selectedValue.split(' ');
                          String tutorID = tutorInfo.first;
                          String tutorType = tutorInfo.last;

                          var result = await _viewModel.assignTutors(
                              tutorID, tutorType, userID);

                          if (result == null) {
                            setState(() {
                              error = 'No se ha podido asignar el tutor.';
                            });
                          }

                          // Volvemos a obtener la lista de tutores para recargar la lista que se muestra por pantalla
                          _viewModel
                              .getEducatorsAssigned(userID)
                              .then((List<dynamic> result) {
                            setState(() {
                              educatorsListAssigned = result;
                            });
                          });

                          setState(() {
                            selectedValue = null;
                          });
                        }),
                  child: Text(
                    'Asignar',
                    style: TextStyle(color: Colors.white),
                  ),
                  color: HexColor('#72CEF2'),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                  ),
                ),
              ),
              SizedBox(height: 5),
              Text(
                error,
                style: TextStyle(color: Colors.red, fontSize: 14.0),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 20),
              Text(
                'Tutores asignados',
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: HexColor('#0D518C')),
              ),
              Container(
                margin: const EdgeInsets.only(left: 100.0, right: 100.0),
                child: Divider(
                  color: HexColor('#071A40'),
                  height: 16,
                  thickness: 1,
                ),
              ),
              SizedBox(height: 10),
              (educatorsListAssigned.length == 0)
                  ? Text('No se han asignado educadores todavía')
                  : Expanded(
                      child: ListView.builder(
                          itemCount: educatorsListAssigned.length,
                          itemBuilder: (BuildContext context, int index) {
                            final educator = educatorsListAssigned[index];
                            return Column(
                              children: [
                                ListTile(
                                  title: (Text(educator['name'] +
                                      " " +
                                      educator['fullname'])),
                                  leading: Icon(Icons.person),
                                ),
                                Divider(
                                  color: Colors.grey,
                                  height: 12,
                                )
                              ],
                            );
                          }),
                    )
            ],
          ),
        ),
      );
    } else {
      Map<String, Widget> widget;
      widget = {
        "Lista de tutores": SearchableDropdown.single(
          items: items,
          style: TextStyle(fontSize: 24, color: Colors.black),
          value: selectedValue,
          hint: "Elige uno",
          iconSize: 40,
          searchHint: "Elige uno",
          onChanged: (value) {
            setState(() {
              selectedValue = value;
            });
          },
          isExpanded: true,
        ),
      };

      return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
              title: const Text("Asignación de tutores",
                  style: TextStyle(fontSize: 26)),
              leading: IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.white, size: 35),
                onPressed: () => Navigator.of(context).pop(),
              ),
              centerTitle: true,
              backgroundColor: HexColor('#0D518C')),
          body: Column(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: widget
                    .map((k, v) {
                      return (MapEntry(
                          k,
                          Center(
                              child: Container(
                            height: 200,
                            child: Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  side: BorderSide(
                                    color: Colors.grey,
                                    width: 1.0,
                                  ),
                                ),
                                margin: EdgeInsets.all(20),
                                child: Padding(
                                  padding: const EdgeInsets.all(20.0),
                                  child: Column(
                                    children: <Widget>[
                                      Text("$k:",
                                          style: TextStyle(fontSize: 24)),
                                      SizedBox(height: 20),
                                      v,
                                    ],
                                  ),
                                )),
                          ))));
                    })
                    .values
                    .toList(),
              ),
              ButtonTheme(
                height: 60,
                minWidth: 400,
                child: RaisedButton(
                  onPressed: ((selectedValue == null)
                      ? null
                      : () async {
                          var tutorInfo = selectedValue.split(' ');
                          String tutorID = tutorInfo.first;
                          String tutorType = tutorInfo.last;

                          var result = await _viewModel.assignTutors(
                              tutorID, tutorType, userID);

                          if (result == null) {
                            setState(() {
                              error = 'No se ha podido asignar el tutor.';
                            });
                          }

                          // Volvemos a obtener la lista de tutores para recargar la lista que se muestra por pantalla
                          _viewModel
                              .getEducatorsAssigned(userID)
                              .then((List<dynamic> result) {
                            setState(() {
                              educatorsListAssigned = result;
                            });
                          });

                          setState(() {
                            selectedValue = null;
                          });
                        }),
                  child: Text(
                    'Asignar',
                    style: TextStyle(color: Colors.white, fontSize: 24),
                  ),
                  color: HexColor('#72CEF2'),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(26.0),
                  ),
                ),
              ),
              SizedBox(height: 5),
              Text(
                error,
                style: TextStyle(color: Colors.red, fontSize: 14.0),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 20),
              Text(
                'Tutores asignados',
                style: TextStyle(
                    fontSize: 28,
                    fontWeight: FontWeight.bold,
                    color: HexColor('#0D518C')),
              ),
              Container(
                margin: const EdgeInsets.only(left: 100.0, right: 100.0),
                child: Divider(
                  color: HexColor('#071A40'),
                  height: 22,
                  thickness: 2,
                ),
              ),
              SizedBox(height: 10),
              (educatorsListAssigned.length == 0)
                  ? Text('No se han asignado educadores todavía',
                      style: TextStyle(fontSize: 24))
                  : Expanded(
                      child: ListView.builder(
                          itemCount: educatorsListAssigned.length,
                          itemBuilder: (BuildContext context, int index) {
                            final educator = educatorsListAssigned[index];
                            return Column(
                              children: [
                                ListTile(
                                  title: (Text(
                                      educator['name'] +
                                          " " +
                                          educator['fullname'],
                                      style: TextStyle(fontSize: 24))),
                                  leading: Icon(Icons.person, size: 40),
                                ),
                                Divider(
                                  color: Colors.grey,
                                  height: 20,
                                )
                              ],
                            );
                          }),
                    )
            ],
          ),
        ),
      );
    }
  }
}
