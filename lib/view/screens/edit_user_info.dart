import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:tfg_app/model/user.dart';
import 'package:tfg_app/view/widgets/textInputDecoration.dart';
import 'package:intl/intl.dart';
import 'package:tfg_app/view_model/sign_up_view_model.dart';

class EditUserInfo extends StatefulWidget {
  final User userInfo;
  final bool isEducator;
  EditUserInfo({Key key, this.userInfo, this.isEducator}) : super(key: key);

  @override
  _EditUserInfoState createState() {
    return _EditUserInfoState(userInfo: userInfo, isEducator: isEducator);
  }
}

class _EditUserInfoState extends State<EditUserInfo> {
  User userInfo;
  bool isEducator;
  _EditUserInfoState({this.userInfo, this.isEducator});

  SignUpViewModel _viewModel = SignUpViewModel();

  MediaQueryData queryData;
  String userID = '';
  String name = '';
  String fullname = '';
  String email = '';
  String teaType = '';
  String favGenre = '';
  String interest = '';
  bool isEducatorReadOnly = false;
  int age = 0;
  User user = new User();
  String error = '';
  TextEditingController dateinput = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _viewModel = new SignUpViewModel();

    setState(() {
      isEducatorReadOnly = isEducator;
    });

    if (userInfo != null) {
      setState(() {
        userID = userInfo.userID;
        name = userInfo.name;
        fullname = userInfo.fullname;
        email = userInfo.email;
        teaType = userInfo.teaType;
        favGenre = userInfo.favGenre;
        interest = userInfo.interest;
        dateinput.text = userInfo.birthDate;
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);

    var screenSize = queryData.size;

    if (screenSize.height < 925) {
      return Scaffold(
        appBar: AppBar(
            title: (isEducator == false ? Text("Edición de datos del niño/a") : Text('Consulta de sus datos')),
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () => Navigator.of(context).pop(),
            ),
            centerTitle: true,
            backgroundColor: HexColor('#0D518C')),
        body: Container(
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 50),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Text(
                  'Información del niño/a',
                  style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      color: HexColor('#0D518C')),
                ),
                Container(
                  margin: const EdgeInsets.only(left: 50.0, right: 50.0),
                  child: Divider(
                    color: HexColor('#071A40'),
                    height: 16,
                    thickness: 1.8,
                  ),
                ),
                Container(
                    padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
                    alignment: Alignment.center,
                    width: 400,
                    height: 550,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(color: Colors.grey),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.1),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          TextFormField(
                            readOnly: isEducatorReadOnly,
                            initialValue: name,
                            decoration: textInputDecoration.copyWith(
                                hintText: 'Nombre',
                                prefixIcon: Icon(Icons.person)),
                            validator: (val) =>
                                val.isEmpty ? 'Introduzca un nombre' : null,
                            onChanged: (value) {
                              setState(() => name = value);
                            },
                          ),
                          SizedBox(height: 20),
                          TextFormField(
                            readOnly: isEducatorReadOnly,
                            initialValue: fullname,
                            decoration: textInputDecoration.copyWith(
                                hintText: 'Apellidos',
                                prefixIcon: Icon(Icons.person_outline)),
                            validator: (val) =>
                                val.isEmpty ? 'Introduzca los apellidos' : null,
                            onChanged: (value) {
                              setState(() => fullname = value);
                            },
                          ),
                          SizedBox(height: 20),
                          TextFormField(
                            readOnly: isEducatorReadOnly,
                            initialValue: email,
                            decoration: textInputDecoration.copyWith(
                                hintText: 'Correo',
                                prefixIcon: Icon(Icons.email)),
                            keyboardType: TextInputType.emailAddress,
                            onChanged: (value) {
                              setState(() => email = value);
                            },
                          ),
                          SizedBox(height: 20),
                          (isEducatorReadOnly == false ? TextFormField(
                            validator: (val) => val.isEmpty
                                ? 'Introduzca una fecha de nacimiento'
                                : null,
                            controller:
                                dateinput, //editing controller of this TextField
                            decoration: InputDecoration(
                                prefixIcon: Icon(
                                    Icons.calendar_today), //icon of text field
                                labelText:
                                    "Introduzca la fecha de nacimiento" //label text of field
                                ),
                            readOnly:
                                true, //set it true, so that user will not able to edit text
                            onTap: () async {
                              DateTime pickedDate = await showDatePicker(
                                  context: context,
                                  initialDate: DateTime.now(),
                                  firstDate: DateTime(
                                      1980), //DateTime.now() - not to allow to choose before today.
                                  lastDate: DateTime(2101));

                              if (pickedDate != null) {
                                print(
                                    pickedDate); //pickedDate output format => 2021-03-10 00:00:00.000
                                String formattedDate =
                                    DateFormat('dd-MM-yyyy').format(pickedDate);
                                print(
                                    formattedDate); //formatted date output using intl package =>  2021-03-16
                                //you can implement different kind of Date Format here according to your requirement

                                setState(() {
                                  dateinput.text =
                                      formattedDate; //set output date to TextField value.
                                });
                              } else {
                                print("La fecha no ha sido seleccionada");
                              }
                            },
                          ) : TextFormField(
                            readOnly: isEducatorReadOnly,
                            initialValue: dateinput.text,
                            decoration: textInputDecoration.copyWith(
                                hintText: 'Fecha de Nacimiento',
                                prefixIcon: Icon(Icons.calendar_today)),
                          )),
                          SizedBox(height: 20),
                          TextFormField(
                            readOnly: isEducatorReadOnly,
                            initialValue: teaType,
                            decoration: textInputDecoration.copyWith(
                                hintText: 'Tipo de diversidad funcional',
                                prefixIcon: Icon(Icons.accessibility)),
                            validator: (val) => val.isEmpty
                                ? 'Introduzca el tipo de diversidad funcional'
                                : null,
                            onChanged: (value) {
                              setState(() => teaType = value);
                            },
                          ),
                          SizedBox(height: 20),
                          TextFormField(
                            readOnly: isEducatorReadOnly,
                            initialValue: favGenre,
                            decoration: textInputDecoration.copyWith(
                                hintText: 'Género favorito',
                                prefixIcon: Icon(Icons.menu_book)),
                            validator: (val) => val.isEmpty
                                ? 'Introduzca el género favorito'
                                : null,
                            onChanged: (value) {
                              setState(() => favGenre = value);
                            },
                          ),
                          SizedBox(height: 20),
                          TextFormField(
                            readOnly: isEducatorReadOnly,
                            initialValue: interest,
                            decoration: textInputDecoration.copyWith(
                                hintText: 'Intereses',
                                prefixIcon: Icon(Icons.favorite)),
                            validator: (val) => val.isEmpty
                                ? 'Introduzca los intereses del niño/a'
                                : null,
                            onChanged: (value) {
                              setState(() => interest = value);
                            },
                          ),
                        ],
                      ),
                    )),
                SizedBox(height: 10),
                (isEducatorReadOnly == false ? ButtonTheme(
                  minWidth: 300,
                  child: RaisedButton(
                      child: Text('Editar',
                          style: TextStyle(fontSize: 16, color: Colors.white)),
                      color: HexColor('#0F8DBF'),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                      ),
                      onPressed: () async {
                        if (_formKey.currentState.validate()) {
                          String birthYear =
                              dateinput.text.split('-').elementAt(2);

                          String actualYear =
                              DateTime.now().toString().split('-').elementAt(0);

                          setState(() {
                            age = int.parse(actualYear) - int.parse(birthYear);
                          });

                          print(birthYear);
                          print(actualYear);
                          print(age);

                          setState(() {
                            user = User(
                                userID: userID,
                                name: name,
                                fullname: fullname,
                                age: age,
                                teaType: teaType,
                                email: email,
                                birthDate: dateinput.text,
                                favGenre: favGenre,
                                interest: interest);
                          });

                          print('USERID: ' + user.userID);

                          await _viewModel.updateUser(user);

                          Navigator.pushReplacementNamed(context, "/select-user");

                        }
                      }),
                ) : SizedBox(height: 10)),
                SizedBox(height: 12.0),
                Text(
                  error,
                  style: TextStyle(color: Colors.red, fontSize: 14.0),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        ),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
            title: (isEducator == false ? Text("Edición de datos del niño/a", style: TextStyle(fontSize: 26)) : Text('Consulta de sus datos', style: TextStyle(fontSize: 26))),
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () => Navigator.of(context).pop(),
            ),
            centerTitle: true,
            backgroundColor: HexColor('#0D518C')),
        body: Container(
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 50),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Text(
                  'Información del niño/a',
                  style: TextStyle(
                      fontSize: 26,
                      fontWeight: FontWeight.bold,
                      color: HexColor('#0D518C')),
                ),
                Container(
                  margin: const EdgeInsets.only(left: 120.0, right: 120.0),
                  child: Divider(
                    color: HexColor('#071A40'),
                    height: 16,
                    thickness: 2.0,
                  ),
                ),
                SizedBox(height: 40),
                Container(
                    padding: EdgeInsets.symmetric(vertical: 30, horizontal: 40),
                    alignment: Alignment.center,
                    width: 600,
                    height: 800,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(color: Colors.grey),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.1),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          TextFormField(
                            style: TextStyle(fontSize: 24),
                            readOnly: isEducatorReadOnly,
                            initialValue: name,
                            decoration: textInputDecoration.copyWith(
                                hintText: 'Nombre',
                                prefixIcon: Icon(Icons.person)),
                            validator: (val) =>
                            val.isEmpty ? 'Introduzca un nombre' : null,
                            onChanged: (value) {
                              setState(() => name = value);
                            },
                          ),
                          SizedBox(height: 20),
                          TextFormField(
                            style: TextStyle(fontSize: 24),
                            readOnly: isEducatorReadOnly,
                            initialValue: fullname,
                            decoration: textInputDecoration.copyWith(
                                hintText: 'Apellidos',
                                prefixIcon: Icon(Icons.person_outline)),
                            validator: (val) =>
                            val.isEmpty ? 'Introduzca los apellidos' : null,
                            onChanged: (value) {
                              setState(() => fullname = value);
                            },
                          ),
                          SizedBox(height: 20),
                          TextFormField(
                            style: TextStyle(fontSize: 24),
                            readOnly: isEducatorReadOnly,
                            initialValue: email,
                            decoration: textInputDecoration.copyWith(
                                hintText: 'Correo',
                                prefixIcon: Icon(Icons.email)),
                            keyboardType: TextInputType.emailAddress,
                            onChanged: (value) {
                              setState(() => email = value);
                            },
                          ),
                          SizedBox(height: 20),
                          (isEducatorReadOnly == false ? TextFormField(
                            style: TextStyle(fontSize: 24),
                            validator: (val) => val.isEmpty
                                ? 'Introduzca una fecha de nacimiento'
                                : null,
                            controller:
                            dateinput, //editing controller of this TextField
                            decoration: InputDecoration(
                                prefixIcon: Icon(
                                    Icons.calendar_today), //icon of text field
                                labelText:
                                "Introduzca la fecha de nacimiento" //label text of field
                            ),
                            readOnly:
                            true, //set it true, so that user will not able to edit text
                            onTap: () async {
                              DateTime pickedDate = await showDatePicker(
                                  context: context,
                                  initialDate: DateTime.now(),
                                  firstDate: DateTime(
                                      1980), //DateTime.now() - not to allow to choose before today.
                                  lastDate: DateTime(2101));

                              if (pickedDate != null) {
                                print(
                                    pickedDate); //pickedDate output format => 2021-03-10 00:00:00.000
                                String formattedDate =
                                DateFormat('dd-MM-yyyy').format(pickedDate);
                                print(
                                    formattedDate); //formatted date output using intl package =>  2021-03-16
                                //you can implement different kind of Date Format here according to your requirement

                                setState(() {
                                  dateinput.text =
                                      formattedDate; //set output date to TextField value.
                                });
                              } else {
                                print("La fecha no ha sido seleccionada");
                              }
                            },
                          ) : TextFormField(
                            style: TextStyle(fontSize: 24),
                            readOnly: isEducatorReadOnly,
                            initialValue: dateinput.text,
                            decoration: textInputDecoration.copyWith(
                                hintText: 'Fecha de Nacimiento',
                                prefixIcon: Icon(Icons.calendar_today)),
                          )),
                          SizedBox(height: 20),
                          TextFormField(
                            style: TextStyle(fontSize: 24),
                            readOnly: isEducatorReadOnly,
                            initialValue: teaType,
                            decoration: textInputDecoration.copyWith(
                                hintText: 'Tipo de diversidad funcional',
                                prefixIcon: Icon(Icons.accessibility)),
                            validator: (val) => val.isEmpty
                                ? 'Introduzca el tipo de diversidad funcional'
                                : null,
                            onChanged: (value) {
                              setState(() => teaType = value);
                            },
                          ),
                          SizedBox(height: 20),
                          TextFormField(
                            style: TextStyle(fontSize: 24),
                            readOnly: isEducatorReadOnly,
                            initialValue: favGenre,
                            decoration: textInputDecoration.copyWith(
                                hintText: 'Género favorito',
                                prefixIcon: Icon(Icons.menu_book)),
                            validator: (val) => val.isEmpty
                                ? 'Introduzca el género favorito'
                                : null,
                            onChanged: (value) {
                              setState(() => favGenre = value);
                            },
                          ),
                          SizedBox(height: 20),
                          TextFormField(
                            style: TextStyle(fontSize: 24),
                            readOnly: isEducatorReadOnly,
                            initialValue: interest,
                            decoration: textInputDecoration.copyWith(
                                hintText: 'Intereses',
                                prefixIcon: Icon(Icons.favorite)),
                            validator: (val) => val.isEmpty
                                ? 'Introduzca los intereses del niño/a'
                                : null,
                            onChanged: (value) {
                              setState(() => interest = value);
                            },
                          ),
                        ],
                      ),
                    )),
                SizedBox(height: 20),
                (isEducatorReadOnly == false ? ButtonTheme(
                  minWidth: 400,
                  height: 50,
                  child: RaisedButton(
                      child: Text('Editar',
                          style: TextStyle(fontSize: 24, color: Colors.white)),
                      color: HexColor('#0F8DBF'),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(22.0),
                      ),
                      onPressed: () async {
                        if (_formKey.currentState.validate()) {
                          String birthYear =
                          dateinput.text.split('-').elementAt(2);

                          String actualYear =
                          DateTime.now().toString().split('-').elementAt(0);

                          setState(() {
                            age = int.parse(actualYear) - int.parse(birthYear);
                          });

                          print(birthYear);
                          print(actualYear);
                          print(age);

                          setState(() {
                            user = User(
                                userID: userID,
                                name: name,
                                fullname: fullname,
                                age: age,
                                teaType: teaType,
                                email: email,
                                birthDate: dateinput.text,
                                favGenre: favGenre,
                                interest: interest);
                          });

                          print('USERID: ' + user.userID);

                          await _viewModel.updateUser(user);

                          Navigator.pushReplacementNamed(context, "/select-user");

                        }
                      }),
                ) : SizedBox(height: 10)),
                SizedBox(height: 12.0),
                Text(
                  error,
                  style: TextStyle(color: Colors.red, fontSize: 14.0),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        ),
      );
    }
  }
}
