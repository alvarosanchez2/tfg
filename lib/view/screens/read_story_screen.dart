import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:tfg_app/view/widgets/loading_words.dart';
import 'package:tfg_app/view_model/create_story_view_model.dart';
import 'package:tfg_app/view_model/read_story_view_model.dart';
import 'package:flutter_tts/flutter_tts.dart';

class ReadStoryPage extends StatefulWidget {
  final String storyID;
  final String numPages;
  final bool audio;
  ReadStoryPage({Key key, this.storyID, this.numPages, this.audio})
      : super(key: key);

  @override
  _ReadStoryPageState createState() {
    return _ReadStoryPageState(
        storyID: storyID, numPages: numPages, audio: audio);
  }
}

class _ReadStoryPageState extends State<ReadStoryPage> {
  String storyID;
  String numPages;
  bool audio;
  var imageInfo;
  _ReadStoryPageState({this.storyID, this.numPages, this.audio});

  ReadStoryViewModel _storyViewModel = ReadStoryViewModel();
  CreateStoryViewModel _createStoryViewModel = CreateStoryViewModel();
  int pageID = 1;
  var currentPageInfo;
  String audioCuento = '';
  bool tablet = false;
  final FlutterTts flutterTts = FlutterTts();
  MediaQueryData queryData;
  ScrollController _gridController = ScrollController();

  @override
  void initState() {
    super.initState();
    _storyViewModel = new ReadStoryViewModel();
    _createStoryViewModel = new CreateStoryViewModel();

    _storyViewModel.getPageInfo(pageID.toString(), storyID).then((result) {
      setState(() {
        currentPageInfo = result;
      });
    });

    _createStoryViewModel
        .getPageImageInfo(storyID, pageID.toString())
        .then((result) {
      setState(() {
        imageInfo = result;
      });
    });

    print(audio);

    speak(text) async {
      await flutterTts.setSharedInstance(true);
      await flutterTts
          .setIosAudioCategory(IosTextToSpeechAudioCategory.playAndRecord, [
        IosTextToSpeechAudioCategoryOptions.allowBluetooth,
        IosTextToSpeechAudioCategoryOptions.allowBluetoothA2DP,
        IosTextToSpeechAudioCategoryOptions.mixWithOthers
      ]);
      await flutterTts.awaitSpeakCompletion(true);
      await flutterTts
          .setVoice({"name": "es-es-x-ana-network", "locale": "es-ES"});
      await flutterTts.setPitch(1);
      await flutterTts.setVolume(1.0);
      await flutterTts.setSpeechRate(0.5);
      await flutterTts.speak(text);
    }

    readStoryFunction() async {
      if (audio) {

        await _storyViewModel.getPageInfo(pageID.toString(), storyID).then((result) {
          setState(() {
            currentPageInfo = result;
          });
        });

        // Función para la reproducción en la audio-lectura
        if (currentPageInfo != null) {
          audioCuento = '';
          for (int i = 0; i < currentPageInfo.length; i++) {
            await speak(currentPageInfo.elementAt(i).word);
            if (i == 4 && !tablet) {
              _gridController.animateTo(200,
                  duration: Duration(milliseconds: 400),
                  curve: Curves.easeInOut);
            }
          }
        }
      }
    }
    readStoryFunction();

  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);

    var screenSize = queryData.size;

    print(screenSize);

    if (screenSize.height < 825) {
      return Scaffold(
        body: Container(
          padding: EdgeInsets.only(top: 60),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              (currentPageInfo == null || imageInfo == null
                  ? Expanded(child: LoadingWords())
                  : Expanded(
                      child: Column(
                        children: [
                          (currentPageInfo == null
                              ? Text('')
                              : Container(
                                  height: 200,
                                  width: double.infinity,
                                  padding: EdgeInsets.symmetric(
                                      vertical: 10, horizontal: 10),
                                  child: GridView.builder(
                                      controller: _gridController,
                                      gridDelegate:
                                          const SliverGridDelegateWithFixedCrossAxisCount(
                                              crossAxisCount: 5,
                                              mainAxisSpacing: 22,
                                              crossAxisSpacing: 5),
                                      //scrollDirection: Axis.horizontal,
                                      itemCount: currentPageInfo.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        final wordsInfo =
                                            currentPageInfo[index];
                                        return Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            GestureDetector(
                                              onTap: () =>
                                                  speak(wordsInfo.word),
                                              onLongPress: () =>
                                                  speak(wordsInfo.word),
                                              child: (wordsInfo.picto != null
                                                  ? Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: [
                                                        Container(
                                                          height: 100,
                                                          child: Image.network(
                                                            wordsInfo.picto,
                                                          ),
                                                        ),
                                                        Container(
                                                          height: 30,
                                                          child: (wordsInfo.word
                                                                      .toString()
                                                                      .length <=
                                                                  6
                                                              ? Text(
                                                                  wordsInfo.word
                                                                          .toString()
                                                                          .toUpperCase() +
                                                                      '  ',
                                                                  style: GoogleFonts.lato(
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                      fontSize:
                                                                          20))
                                                              : Text(
                                                                  wordsInfo.word
                                                                          .toString()
                                                                          .toUpperCase() +
                                                                      '  ',
                                                                  style: GoogleFonts.lato(
                                                                      fontWeight: FontWeight.bold,
                                                                      fontSize: 15))),
                                                        )
                                                      ],
                                                    )
                                                  : (wordsInfo.word
                                                              .toString()
                                                              .length <=
                                                          6
                                                      ? SizedBox(
                                                          width: 120,
                                                          child: Center(
                                                            child: Text(
                                                                wordsInfo.word
                                                                        .toString()
                                                                        .toUpperCase() +
                                                                    '  ',
                                                                style: GoogleFonts.lato(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    fontSize:
                                                                        25)),
                                                          ),
                                                        )
                                                      : SizedBox(
                                                          width: 120,
                                                          child: Center(
                                                            child: Text(
                                                                wordsInfo.word
                                                                        .toString()
                                                                        .toUpperCase() +
                                                                    '  ',
                                                                style: GoogleFonts.lato(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    fontSize:
                                                                        16)),
                                                          ),
                                                        ))),
                                            ),
                                            SizedBox(width: 20)
                                          ],
                                        );
                                      }),
                                )),
                        ],
                      ),
                    )),
              Divider(
                color: Colors.grey,
                height: 16,
                thickness: 1,
              ),
              Container(
                padding: EdgeInsets.all(2),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    (pageID != 1
                        ? GestureDetector(
                            child: Icon(Icons.navigate_before_rounded,
                                size: 70, color: HexColor('#071A40')),
                            onTap: () async {

                              _gridController.animateTo(0,
                                  duration: Duration(milliseconds: 500),
                                  curve: Curves.easeInOut);

                              pageID = pageID - 1;
                              await _storyViewModel
                                  .getPageInfo((pageID).toString(), storyID)
                                  .then((result) {
                                setState(() {
                                  currentPageInfo = result;
                                });
                              });

                              await _createStoryViewModel
                                  .getPageImageInfo(storyID, pageID.toString())
                                  .then((result) {
                                setState(() {
                                  imageInfo = result;
                                });
                              });

                              readStoryFunction();
                            })
                        : Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              GestureDetector(
                                  child: Icon(Icons.navigate_before_rounded,
                                      size: 70, color: HexColor('#071A40')),
                                  onTap: () => Navigator.pop(context)),
                            ],
                          )),
                    (imageInfo != null && imageInfo.image != null
                        ? Column(
                            children: [
                              GestureDetector(
                                  onTap: () {
                                    if (imageInfo.imageText != null) {
                                      speak(imageInfo.imageText);
                                    }
                                  },
                                  child: Image.network(imageInfo.image,
                                      height: 70)),
                              SizedBox(height: 10),
                              Text('Página ' + pageID.toString(),
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold)),
                            ],
                          )
                        : Text('Página ' + pageID.toString(),
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold))),
                    (pageID < int.parse(numPages)
                        ? GestureDetector(
                            child: Icon(
                              Icons.navigate_next_rounded,
                              size: 70,
                              color: HexColor('#071A40'),
                            ),
                            onTap: () async {
                              _gridController.animateTo(0,
                                  duration: Duration(milliseconds: 500),
                                  curve: Curves.easeInOut);

                              pageID = pageID + 1;
                              await _storyViewModel
                                  .getPageInfo((pageID).toString(), storyID)
                                  .then((result) {
                                setState(() {
                                  currentPageInfo = result;
                                });
                              });

                              await _createStoryViewModel
                                  .getPageImageInfo(storyID, pageID.toString())
                                  .then((result) {
                                setState(() {
                                  imageInfo = result;
                                });
                              });

                              readStoryFunction();
                            })
                        : GestureDetector(
                            child: Icon(
                              Icons.navigate_next_rounded,
                              size: 70,
                              color: HexColor('#071A40'),
                            ),
                            onTap: () => Navigator.pop(context)))
                  ],
                ),
              ),
              SizedBox(height: 10)
            ],
          ),
        ),
      );
    } else {

      setState(() {
        tablet = true;
      });

      print(tablet);
      return Scaffold(
        body: Container(
          padding: EdgeInsets.only(top: 100),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              (currentPageInfo == null || imageInfo == null
                  ? Expanded(child: LoadingWords())
                  : Expanded(
                      child: Column(
                        children: [
                          (currentPageInfo == null
                              ? Text('')
                              : Container(
                                  height: 540,
                                  width: double.infinity,
                                  padding: EdgeInsets.only(left: 60),
                                  child: GridView.builder(
                                      controller: _gridController,
                                      gridDelegate:
                                          const SliverGridDelegateWithFixedCrossAxisCount(
                                              crossAxisCount: 5,
                                              mainAxisSpacing: 30,
                                              crossAxisSpacing: 5),
                                      itemCount: currentPageInfo.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        final wordsInfo =
                                            currentPageInfo[index];
                                        return Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            GestureDetector(
                                              onTap: () =>
                                                  speak(wordsInfo.word),
                                              onLongPress: () =>
                                                  speak(wordsInfo.word),
                                              child: (wordsInfo.picto != null
                                                  ? Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: [
                                                        Container(
                                                          height: 200,
                                                          child: Image.network(
                                                            wordsInfo.picto,
                                                          ),
                                                        ),
                                                        Container(
                                                          height: 40,
                                                          child: (wordsInfo.word
                                                                      .toString()
                                                                      .length <=
                                                                  6
                                                              ? Text(
                                                                  wordsInfo.word
                                                                          .toString()
                                                                          .toUpperCase() +
                                                                      '  ',
                                                                  style: GoogleFonts.lato(
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                      fontSize:
                                                                          35))
                                                              : Text(
                                                                  wordsInfo.word
                                                                          .toString()
                                                                          .toUpperCase() +
                                                                      '  ',
                                                                  style: GoogleFonts.lato(
                                                                      fontWeight: FontWeight.bold,
                                                                      fontSize: 30))),
                                                        )
                                                      ],
                                                    )
                                                  : (wordsInfo.word
                                                              .toString()
                                                              .length <=
                                                          6
                                                      ? SizedBox(
                                                          width: 220,
                                                          child: Center(
                                                            child: Text(
                                                                wordsInfo.word
                                                                        .toString()
                                                                        .toUpperCase() +
                                                                    '  ',
                                                                style: GoogleFonts.lato(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    fontSize:
                                                                        40)),
                                                          ),
                                                        )
                                                      : SizedBox(
                                                          width: 220,
                                                          child: Center(
                                                            child: Text(
                                                                wordsInfo.word
                                                                        .toString()
                                                                        .toUpperCase() +
                                                                    '  ',
                                                                style: GoogleFonts.lato(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    fontSize:
                                                                        32)),
                                                          ),
                                                        ))),
                                            ),
                                            SizedBox(width: 20)
                                          ],
                                        );
                                      }),
                                )),
                        ],
                      ),
                    )),
              Divider(
                color: Colors.grey,
                height: 18,
                thickness: 1,
              ),
              Container(
                padding: EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    (pageID != 1
                        ? GestureDetector(
                            onTap: () async {
                              _gridController.animateTo(0,
                                  duration: Duration(milliseconds: 500),
                                  curve: Curves.easeInOut);

                              pageID = pageID - 1;
                              await _storyViewModel
                                  .getPageInfo((pageID).toString(), storyID)
                                  .then((result) {
                                setState(() {
                                  currentPageInfo = result;
                                });
                              });

                              await _createStoryViewModel
                                  .getPageImageInfo(storyID, pageID.toString())
                                  .then((result) {
                                setState(() {
                                  imageInfo = result;
                                });
                              });

                              readStoryFunction();
                            },
                            child: Icon(Icons.navigate_before_rounded,
                                size: 100, color: HexColor('#071A40')),
                          )
                        : Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              GestureDetector(
                                  child: Icon(Icons.navigate_before_rounded,
                                      size: 100, color: HexColor('#071A40')),
                                  onTap: () => Navigator.pop(context)),
                            ],
                          )),
                    (imageInfo != null && imageInfo.image != null
                        ? Column(
                            children: [
                              GestureDetector(
                                  onTap: () {
                                    if (imageInfo.imageText != null) {
                                      speak(imageInfo.imageText);
                                    }
                                  },
                                  child: Image.network(imageInfo.image,
                                      height: 120)),
                              SizedBox(height: 10),
                              Text('Página ' + pageID.toString(),
                                  style: TextStyle(
                                      fontSize: 22,
                                      fontWeight: FontWeight.bold)),
                            ],
                          )
                        : Text('Página ' + pageID.toString(),
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.bold))),
                    (pageID < int.parse(numPages)
                        ? GestureDetector(
                            onTap: () async {
                              _gridController.animateTo(0,
                                  duration: Duration(milliseconds: 500),
                                  curve: Curves.easeInOut);

                              pageID = pageID + 1;
                              await _storyViewModel
                                  .getPageInfo((pageID).toString(), storyID)
                                  .then((result) {
                                setState(() {
                                  currentPageInfo = result;
                                });
                              });

                              await _createStoryViewModel
                                  .getPageImageInfo(storyID, pageID.toString())
                                  .then((result) {
                                setState(() {
                                  imageInfo = result;
                                });
                              });

                              readStoryFunction();
                            },
                            child: Icon(
                              Icons.navigate_next_rounded,
                              size: 100,
                              color: HexColor('#071A40'),
                            ),
                          )
                        : GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Icon(
                              Icons.navigate_next_rounded,
                              size: 100,
                              color: HexColor('#071A40'),
                            ),
                          ))
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    }
  }

  void readStoryFunction() async {
    if (audio) {
      if (currentPageInfo != null) {
        audioCuento = '';
        for (int i = 0; i < currentPageInfo.length; i++) {
          print(i);
          await speak(currentPageInfo.elementAt(i).word);
          if (i == 4 && !tablet) {
            _gridController.animateTo(200,
                duration: Duration(milliseconds: 400),
                curve: Curves.easeInOut);
          }
        }
      }
    }

  }

  speak(text) async {
    await flutterTts.setSharedInstance(true);
    await flutterTts
        .setIosAudioCategory(IosTextToSpeechAudioCategory.playAndRecord, [
      IosTextToSpeechAudioCategoryOptions.allowBluetooth,
      IosTextToSpeechAudioCategoryOptions.allowBluetoothA2DP,
      IosTextToSpeechAudioCategoryOptions.mixWithOthers
    ]);
    await flutterTts.awaitSpeakCompletion(true);
    await flutterTts
        .setVoice({"name": "es-es-x-ana-network", "locale": "es-ES"});
    await flutterTts.setPitch(1);
    await flutterTts.setVolume(1.0);
    await flutterTts.setSpeechRate(0.5);
    await flutterTts.speak(text);
  }

}
