import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:tfg_app/model/story_page.dart';
import 'package:tfg_app/view/screens/add_image_to_page_screen.dart';
import 'package:tfg_app/view/screens/select_image_screen.dart';
import 'package:tfg_app/view/widgets/loading_story.dart';
import 'package:tfg_app/view/widgets/loading_words.dart';
import 'package:tfg_app/view_model/create_story_view_model.dart';
import 'package:tfg_app/view_model/read_story_view_model.dart';

import 'assign_story_to_user.dart';

class AddPictoToTextPage extends StatefulWidget {
  final String storyID;
  final bool editing;
  AddPictoToTextPage({Key key, this.storyID, this.editing}) : super(key: key);

  @override
  _AddPictoToTextPageState createState() {
    return _AddPictoToTextPageState(storyID: storyID, editing: editing);
  }
}

class _AddPictoToTextPageState extends State<AddPictoToTextPage> {
  String storyID;
  bool editing;
  _AddPictoToTextPageState({this.storyID, this.editing});

  ReadStoryViewModel _storyViewModel = ReadStoryViewModel();
  CreateStoryViewModel _createStoryViewModel = CreateStoryViewModel();
  int pageID = 1;
  String picto = '';
  String pageImage = '';
  String imageText = '';
  String error = '';
  var currentPageInfo;
  StoryPage imageInfo;
  var story;
  List pages = [];
  String textEdited = '';

  TextEditingController _controller;
  MediaQueryData queryData;

  @override
  void initState() {
    super.initState();
    _storyViewModel = new ReadStoryViewModel();
    _createStoryViewModel = new CreateStoryViewModel();

    _storyViewModel.getPageInfo(pageID.toString(), storyID).then((result) {
      setState(() {
        currentPageInfo = result;
      });
    });

    _createStoryViewModel
        .getPageImageInfo(storyID, pageID.toString())
        .then((result) {
      setState(() {
        imageInfo = result;
      });
    });

    print(imageInfo);

    _storyViewModel.getStory(storyID).then((result) {
      setState(() {
        story = result;
      });
    });

    _controller = TextEditingController(text: '');

    setState(() {
      error = '';
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    pages = [];
    if (story != null) {
      for (int i = 1; i <= int.parse(story.numPages); i++) {
        var a = {"id": i.toString(), "description": "Página " + i.toString()};
        pages.add(a);
      }
    }

    queryData = MediaQuery.of(context);

    var screenSize = queryData.size;

    if (screenSize.height < 925) {
      Future<void> _showMyDialog(String picto) async {
        await _storyViewModel
            .getPageInfo(
            pageID.toString(), storyID)
            .then((result) {
          setState(() {
            currentPageInfo = result;
          });
        });

        return showDialog<void>(
          context: context,
          barrierDismissible: false, // user must tap button!
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Ver Picto'),
              content: (picto != null
                  ? SingleChildScrollView(
                      child: ListBody(
                        children: [Image.network(picto)],
                      ),
                    )
                  : Text('No se ha asignado un pictograma todavía')),
              actions: [
                TextButton(
                  child: Text('Aceptar'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      }

      Future<void> _showEditingDialog(String word, int index) async {
        setState(() {
          textEdited = word;
        });

        return showDialog<void>(
          context: context,
          barrierDismissible: false, // user must tap button!
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Editando palabra'),
              content: TextFormField(
                initialValue: word,
                onChanged: (value) {
                  textEdited = value;
                },
              ),
              actions: [
                TextButton(
                  child: Text('Aceptar'),
                  onPressed: () async {
                    await _createStoryViewModel.updateWord(
                        (index + 1).toString(),
                        storyID,
                        pageID.toString(),
                        textEdited);
                    await _storyViewModel
                        .getPageInfo(pageID.toString(), storyID)
                        .then((result) {
                      setState(() {
                        currentPageInfo = result;
                      });
                    });
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      }

      Future<bool> _onWillScope() {
        print(storyID);

        return showDialog<bool>(
            context: context,
            child: AlertDialog(
              title: Text('Aviso Alta de Cuento'),
              content: Text(
                  '¿Seguro de que quieres volver?, se eliminará el cuento que se acaba de importar'),
              actions: [
                FlatButton(
                    onPressed: () async {
                      if (storyID != null) {
                        await _storyViewModel.deleteStory(storyID);
                      }
                      Navigator.of(context).pop(true);
                    },
                    child: Text('Aceptar')),
                FlatButton(
                    onPressed: () => Navigator.of(context).pop(false),
                    child: Text('Cancelar'))
              ],
            ));
      }

      return WillPopScope(
        onWillPop: (editing == null ? _onWillScope : null),
        child: Scaffold(
          appBar: AppBar(
              title: const Text("Asignación de pictogramas"),
              leading: IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.white),
                onPressed: () async {
                  print(storyID);
                  if (storyID != null && editing == null) {
                    await _storyViewModel.deleteStory(storyID);
                  }
                  Navigator.of(context).pop();
                },
              ),
              centerTitle: true,
              backgroundColor: HexColor('#0D518C')),
          body: Column(
            children: [
              SizedBox(height: 10),
              (editing != null
                  ? Center(
                      child: Text('Toca una palabra para editarla',
                          style: TextStyle(color: Colors.lightBlue)))
                  : SizedBox(height: 0)),
              SizedBox(height: 5),
              (currentPageInfo == null
                  ? Expanded(child: LoadingWords())
                  : Expanded(
                      child: ListView.builder(
                          itemCount: currentPageInfo.length,
                          itemBuilder: (BuildContext context, int index) {
                            final wordsInfo = currentPageInfo[index];
                            return ListTile(
                              title: GestureDetector(
                                onTap: (editing != null
                                    ? () => _showEditingDialog(
                                        wordsInfo.word.toString(), index)
                                    : null),
                                child: Text(
                                  wordsInfo.word.toString().toUpperCase() + ' ',
                                  style: TextStyle(fontSize: 18),
                                ),
                              ),
                              trailing: Container(
                                width: 145,
                                child: Row(
                                  children: [
                                    IconButton(
                                        icon: Icon(Icons.add),
                                        onPressed: () async {
                                          var pictoSelected =
                                              await Navigator.pushNamed(
                                                  context, '/select-image');
                                          setState(() {
                                            picto = pictoSelected;
                                          });
                                          await _createStoryViewModel
                                              .addPictoToWord(
                                                  storyID,
                                                  pageID.toString(),
                                                  wordsInfo.wordID.toString(),
                                                  picto);

                                          await _storyViewModel
                                              .getPageInfo(
                                                  pageID.toString(), storyID)
                                              .then((result) {
                                            setState(() {
                                              currentPageInfo = result;
                                            });
                                          });
                                        }),
                                    IconButton(
                                        icon: Icon(Icons.delete),
                                        onPressed: () async {
                                          await _createStoryViewModel
                                              .deletePictoFromWord(
                                                  storyID,
                                                  pageID.toString(),
                                                  wordsInfo.wordID.toString());
                                          await _storyViewModel
                                              .getPageInfo(
                                                  pageID.toString(), storyID)
                                              .then((result) {
                                            setState(() {
                                              currentPageInfo = result;
                                            });
                                          });
                                        }),
                                    IconButton(
                                        icon: Icon(Icons.search),
                                        onPressed: () =>
                                            _showMyDialog(wordsInfo.picto)),
                                  ],
                                ),
                              ),
                            );
                          }),
                    )),
              GestureDetector(
                onTap: () async {
                  print(imageInfo);
                  var imageInfoEdited = await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => AddImageToPage(
                            storyID: storyID,
                            pageID: pageID,
                            imageInfo: imageInfo),
                      ));

                  if (imageInfoEdited != null) {
                    setState(() {
                      imageInfo = imageInfoEdited;
                    });
                  }

                  _createStoryViewModel
                      .getPageImageInfo(storyID, pageID.toString())
                      .then((result) {
                    setState(() {
                      imageInfo = result;
                    });
                  });
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.camera_alt_rounded,
                      color: HexColor('#72CEF2'),
                      size: 50,
                    ),
                    Text('Añadir imagen a la página',
                        style: TextStyle(
                            color: HexColor('#0F8DBF'), fontSize: 16))
                  ],
                ),
              ),
              SizedBox(height: 40),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  (story != null && story.numPages != null && pageID != 1
                      ? IconButton(
                          onPressed: () async {
                            setState(() {
                              pageID = pageID - 1;
                            });
                            await _storyViewModel
                                .getPageInfo(pageID.toString(), storyID)
                                .then((result) {
                              setState(() {
                                currentPageInfo = result;
                              });
                            });

                            await _createStoryViewModel
                                .getPageImageInfo(
                                    storyID, pageID.toString())
                                .then((result) {
                              setState(() {
                                imageInfo = result;
                              });
                            });

                            print(imageInfo.imageText);

                            if (imageInfo.imageText != null) {
                              _controller = TextEditingController(
                                  text: imageInfo.imageText);
                            } else {
                              _controller =
                                  TextEditingController(text: '');
                            }

                            setState(() {
                              imageText = '';
                            });

                            setState(() {
                              error = '';
                            });
                          },
                          icon: Icon(Icons.arrow_back_rounded),
                          color: HexColor('#0F8DBF'),
                          iconSize: 50,
                        )
                      : SizedBox(width: 65)),
                  SizedBox(height: 10),
                  Container(
                    width: 200,
                    child: DropdownButtonFormField(
                        isExpanded: true,
                        value: (pages.length > 0
                            ? pages.elementAt(pageID - 1)['id']
                            : "1"),
                        icon: const Icon(Icons.arrow_downward),
                        iconSize: 16,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(20.0),
                              ),
                            ),
                            filled: true,
                            hintStyle: TextStyle(color: Colors.grey[800]),
                            fillColor: HexColor('#72CEF2')),
                        items: pages.map((dynamic value) {
                          return DropdownMenuItem<String>(
                            value: value['id'],
                            child: Text(value['description']),
                          );
                        }).toList(),
                        onChanged: (dynamic newValue) async {
                          setState(() {
                            pageID = int.parse(newValue);
                          });
                          await _storyViewModel
                              .getPageInfo(pageID.toString(), storyID)
                              .then((result) {
                            setState(() {
                              currentPageInfo = result;
                            });
                          });
                          await _createStoryViewModel
                              .getPageImageInfo(
                                  storyID, pageID.toString())
                              .then((result) {
                            setState(() {
                              imageInfo = result;
                            });
                          });

                          print(imageInfo.imageText);
                          if (imageInfo.imageText != null) {
                            _controller = TextEditingController(
                                text: imageInfo.imageText);
                          } else {
                            _controller = TextEditingController(text: '');
                          }

                          setState(() {
                            imageText = '';
                          });
                          print(pageID);
                        }),
                  ),
                  (story != null &&
                          story.numPages != null &&
                          pageID < int.parse(story.numPages)
                      ? IconButton(
                          onPressed: () async {
                            setState(() {
                              pageID = pageID + 1;
                            });
                            await _storyViewModel
                                .getPageInfo(pageID.toString(), storyID)
                                .then((result) {
                              setState(() {
                                currentPageInfo = result;
                              });
                            });

                            await _createStoryViewModel
                                .getPageImageInfo(
                                    storyID, pageID.toString())
                                .then((result) {
                              setState(() {
                                imageInfo = result;
                              });
                            });

                            print(imageInfo.imageText);

                            if (imageInfo.imageText != null) {
                              _controller = TextEditingController(
                                  text: imageInfo.imageText);
                            } else {
                              _controller =
                                  TextEditingController(text: '');
                            }

                            setState(() {
                              imageText = '';
                            });

                            setState(() {
                              error = '';
                            });
                          },
                          icon: Icon(Icons.arrow_forward_rounded),
                          color: HexColor('#0F8DBF'),
                          iconSize: 50,
                        )
                      : SizedBox(width: 65)),
                ],
              ),
              SizedBox(height: 5),
              RaisedButton(
                onPressed: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>
                          AssignStoryPage(storyID: storyID),
                    )),
                child: Text(
                  'Asignación de usuarios',
                  style: TextStyle(color: Colors.white),
                ),
                color: HexColor('#0F8DBF'),
              ),
              SizedBox(height: 20),
            ],
          ),
        ),
      );
    } else {
      Future<void> _showMyDialog(String picto) async {
        return showDialog<void>(
          context: context,
          barrierDismissible: false, // user must tap button!
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(child: Text('Ver Picto', style: TextStyle(fontSize: 26))),
              content: (picto != null
                  ? SingleChildScrollView(
                child: ListBody(
                  children: [SizedBox(height: 300, width: 500,child: Image.network(picto))],
                ),
              )
                  : Text('No se ha asignado un pictograma todavía', style: TextStyle( fontSize: 22))),
              actions: [
                TextButton(
                  child: Text('Aceptar', style: TextStyle(color: HexColor('#0D518C'), fontSize: 20)),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      }

      Future<void> _showEditingDialog(String word, int index) async {
        setState(() {
          textEdited = word;
        });

        return showDialog<void>(
          context: context,
          barrierDismissible: false, // user must tap button!
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Editando palabra', style: TextStyle(fontSize: 26)),
              content: TextFormField(
                style: TextStyle(fontSize: 24),
                initialValue: word,
                onChanged: (value) {
                  textEdited = value;
                },
              ),
              actions: [
                TextButton(
                  child: Text('Aceptar', style: TextStyle(color: HexColor('#0D518C'), fontSize: 20)),
                  onPressed: () async {
                    await _createStoryViewModel.updateWord(
                        (index + 1).toString(),
                        storyID,
                        pageID.toString(),
                        textEdited);
                    await _storyViewModel
                        .getPageInfo(pageID.toString(), storyID)
                        .then((result) {
                      setState(() {
                        currentPageInfo = result;
                      });
                    });
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      }

      Future<bool> _onWillScope() {
        print(storyID);

        return showDialog<bool>(
            context: context,
            child: AlertDialog(
              title: Text('Aviso Alta de Cuento', style: TextStyle(fontSize: 26)),
              content: Text(
                  '¿Seguro de que quieres volver?, se eliminará el cuento que se acaba de importar'),
              actions: [
                FlatButton(
                    onPressed: () async {
                      if (storyID != null) {
                        await _storyViewModel.deleteStory(storyID);
                      }
                      Navigator.of(context).pop(true);
                    },
                    child: Text('Aceptar')),
                FlatButton(
                    onPressed: () => Navigator.of(context).pop(false),
                    child: Text('Cancelar'))
              ],
            ));
      }

      return WillPopScope(
        onWillPop: (editing == null ? _onWillScope : null),
        child: Scaffold(
          appBar: AppBar(
              title: const Text("Asignación de pictogramas", style: TextStyle(fontSize: 26)),
              leading: IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.white, size: 35,),
                onPressed: () async {
                  print(storyID);
                  if (storyID != null && editing == null) {
                    await _storyViewModel.deleteStory(storyID);
                  }
                  Navigator.of(context).pop();
                },
              ),
              centerTitle: true,
              backgroundColor: HexColor('#0D518C')),
          body: Column(
            children: [
              SizedBox(height: 20),
              (editing != null
                  ? Center(
                  child: Text('Toca una palabra para editarla',
                      style: TextStyle(color: Colors.lightBlue, fontSize: 24)))
                  : SizedBox(height: 0)),
              SizedBox(height: 10),
              (currentPageInfo == null
                  ? Expanded(child: LoadingWords())
                  : Expanded(
                child: ListView.builder(
                    itemCount: currentPageInfo.length,
                    itemBuilder: (BuildContext context, int index) {
                      final wordsInfo = currentPageInfo[index];
                      return ListTile(
                        title: GestureDetector(
                          onTap: (editing != null
                              ? () => _showEditingDialog(
                              wordsInfo.word.toString(), index)
                              : null),
                          child: Text(
                            wordsInfo.word.toString().toUpperCase() + ' ',
                            style: TextStyle(fontSize: 24),
                          ),
                        ),
                        trailing: Container(
                          width: 165,
                          child: Row(
                            children: [
                              IconButton(
                                  icon: Icon(Icons.add, size: 40),
                                  onPressed: () async {
                                    var pictoSelected =
                                    await Navigator.pushNamed(
                                        context, '/select-image');
                                    setState(() {
                                      picto = pictoSelected;
                                    });
                                    await _createStoryViewModel
                                        .addPictoToWord(
                                        storyID,
                                        pageID.toString(),
                                        wordsInfo.wordID.toString(),
                                        picto);

                                    await _storyViewModel
                                        .getPageInfo(
                                        pageID.toString(), storyID)
                                        .then((result) {
                                      setState(() {
                                        currentPageInfo = result;
                                      });
                                    });
                                  }),
                              IconButton(
                                  icon: Icon(Icons.delete, size: 40),
                                  onPressed: () async {
                                    await _createStoryViewModel
                                        .deletePictoFromWord(
                                        storyID,
                                        pageID.toString(),
                                        wordsInfo.wordID.toString());

                                    await _storyViewModel
                                        .getPageInfo(
                                        pageID.toString(), storyID)
                                        .then((result) {
                                      setState(() {
                                        currentPageInfo = result;
                                      });
                                    });
                                  }),
                              IconButton(
                                  icon: Icon(Icons.search, size: 40),
                                  onPressed: () =>
                                      _showMyDialog(wordsInfo.picto)),
                            ],
                          ),
                        ),
                      );
                    }),
              )),
              GestureDetector(
                onTap: () async {
                  print(imageInfo);
                  var imageInfoEdited = await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => AddImageToPage(
                            storyID: storyID,
                            pageID: pageID,
                            imageInfo: imageInfo),
                      ));

                  if (imageInfoEdited != null) {
                    setState(() {
                      imageInfo = imageInfoEdited;
                    });
                  }

                  _createStoryViewModel
                      .getPageImageInfo(storyID, pageID.toString())
                      .then((result) {
                    setState(() {
                      imageInfo = result;
                    });
                  });
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.camera_alt_rounded,
                      color: HexColor('#72CEF2'),
                      size: 80,
                    ),
                    Text('Añadir imagen a la página',
                        style: TextStyle(
                            color: HexColor('#0F8DBF'), fontSize: 26))
                  ],
                ),
              ),
              SizedBox(height: 220),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  (story != null && story.numPages != null && pageID != 1
                      ? IconButton(
                    onPressed: () async {
                      setState(() {
                        pageID = pageID - 1;
                      });
                      await _storyViewModel
                          .getPageInfo(pageID.toString(), storyID)
                          .then((result) {
                        setState(() {
                          currentPageInfo = result;
                        });
                      });

                      await _createStoryViewModel
                          .getPageImageInfo(
                          storyID, pageID.toString())
                          .then((result) {
                        setState(() {
                          imageInfo = result;
                        });
                      });

                      print(imageInfo.imageText);

                      if (imageInfo.imageText != null) {
                        _controller = TextEditingController(
                            text: imageInfo.imageText);
                      } else {
                        _controller =
                            TextEditingController(text: '');
                      }

                      setState(() {
                        imageText = '';
                      });

                      setState(() {
                        error = '';
                      });
                    },
                    icon: Icon(Icons.arrow_back_rounded, size: 60),
                    color: HexColor('#0F8DBF'),
                    iconSize: 60,
                  )
                      : SizedBox(width: 65)),
                  SizedBox(height: 10),
                  Container(
                    width: 400,
                    child: DropdownButtonFormField(
                        isExpanded: true,
                        value: (pages.length > 0
                            ? pages.elementAt(pageID - 1)['id']
                            : "1"),
                        icon: const Icon(Icons.arrow_downward),
                        iconSize: 24,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(20.0),
                              ),
                            ),
                            filled: true,
                            hintStyle: TextStyle(color: Colors.grey[800]),
                            fillColor: HexColor('#72CEF2')),
                        items: pages.map((dynamic value) {
                          return DropdownMenuItem<String>(
                            value: value['id'],
                            child: Center(child: Text(value['description'], style: TextStyle(fontSize: 20))),
                          );
                        }).toList(),
                        onChanged: (dynamic newValue) async {
                          setState(() {
                            pageID = int.parse(newValue);
                          });
                          await _storyViewModel
                              .getPageInfo(pageID.toString(), storyID)
                              .then((result) {
                            setState(() {
                              currentPageInfo = result;
                            });
                          });
                          await _createStoryViewModel
                              .getPageImageInfo(
                              storyID, pageID.toString())
                              .then((result) {
                            setState(() {
                              imageInfo = result;
                            });
                          });

                          print(imageInfo.imageText);
                          if (imageInfo.imageText != null) {
                            _controller = TextEditingController(
                                text: imageInfo.imageText);
                          } else {
                            _controller = TextEditingController(text: '');
                          }

                          setState(() {
                            imageText = '';
                          });
                          print(pageID);
                        }),
                  ),
                  (story != null &&
                      story.numPages != null &&
                      pageID < int.parse(story.numPages)
                      ? IconButton(
                    onPressed: () async {
                      setState(() {
                        pageID = pageID + 1;
                      });
                      await _storyViewModel
                          .getPageInfo(pageID.toString(), storyID)
                          .then((result) {
                        setState(() {
                          currentPageInfo = result;
                        });
                      });

                      await _createStoryViewModel
                          .getPageImageInfo(
                          storyID, pageID.toString())
                          .then((result) {
                        setState(() {
                          imageInfo = result;
                        });
                      });

                      print(imageInfo.imageText);

                      if (imageInfo.imageText != null) {
                        _controller = TextEditingController(
                            text: imageInfo.imageText);
                      } else {
                        _controller =
                            TextEditingController(text: '');
                      }

                      setState(() {
                        imageText = '';
                      });

                      setState(() {
                        error = '';
                      });
                    },
                    icon: Icon(Icons.arrow_forward_rounded, size: 60),
                    color: HexColor('#0F8DBF'),
                    iconSize: 60,
                  )
                      : SizedBox(width: 65)),
                ],
              ),
              SizedBox(height: 40),
              ButtonTheme(
                height: 60,
                minWidth: 400,
                child: RaisedButton(
                  onPressed: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) =>
                            AssignStoryPage(storyID: storyID),
                      )),
                  child: Text(
                    'Asignación de usuarios',
                    style: TextStyle(color: Colors.white, fontSize: 22),
                  ),
                  color: HexColor('#0F8DBF'),
                ),
              ),
              SizedBox(height: 60),
            ],
          ),
        ),
      );
    }
  }
}
