import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:tfg_app/view/widgets/textInputDecoration.dart';
import 'package:tfg_app/view_model/sign_up_view_model.dart';

class ChangePasswordPage extends StatefulWidget {
  ChangePasswordPage({Key key}) : super(key: key);

  @override
  _ChangePasswordPageState createState() {
    return _ChangePasswordPageState();
  }
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {
  // Variable clave para realizar la validación del formulario
  final _formKey = GlobalKey<FormState>();

  MediaQueryData queryData;

  String email = '';
  String password = '';
  bool _passwordVisibility = true;

  SignUpViewModel _viewModel = SignUpViewModel();

  @override
  void initState() {
    super.initState();
    _viewModel = new SignUpViewModel();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _showPassword() {
    setState(() {
      _passwordVisibility = !_passwordVisibility;
    });
  }

  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);
    var screenSize = queryData.size;

    if (screenSize.height < 925) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Cambiar contraseña del usuario'),
          backgroundColor: HexColor('#0D518C'),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 50),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  /*SizedBox(height: 20),
                  TextFormField(
                    decoration: textInputDecoration.copyWith(
                        hintText: 'Correo',
                        prefixIcon: Icon(Icons.email)),
                    validator: (val) =>
                    val.isEmpty ? 'Introduzca un correo' : null,
                    keyboardType: TextInputType.emailAddress,
                    onChanged: (value) {
                      setState(() => email = value);
                    },
                  ),*/
                  SizedBox(height: 20),
                  TextFormField(
                    decoration: textInputDecoration.copyWith(
                        hintText: 'Contraseña Nueva',
                        prefixIcon: Icon(Icons.vpn_key_rounded),
                        suffixIcon: IconButton(
                            icon: Icon(
                              _passwordVisibility
                                  ? Icons.visibility_off
                                  : Icons.visibility,
                            ),
                            onPressed: _showPassword)),
                    obscureText: _passwordVisibility,
                    validator: (val) => val.length < 6
                        ? 'Introduzca una contraseña de más de 6 caracteres'
                        : null,
                    onChanged: (value) {
                      setState(() => password = value);
                    },
                  ),
                  SizedBox(height: 20),
                  TextFormField(
                      decoration: textInputDecoration.copyWith(
                          hintText: 'Repita Contraseña Nueva',
                          prefixIcon: Icon(Icons.vpn_key_rounded),
                          suffixIcon: IconButton(
                              icon: Icon(
                                _passwordVisibility
                                    ? Icons.visibility_off
                                    : Icons.visibility,
                              ),
                              onPressed: _showPassword)),
                      obscureText: _passwordVisibility,
                      validator: (val) => val != password
                          ? 'La contraseña debe ser igual a la anterior'
                          : null),
                  SizedBox(height: 20),
                  ButtonTheme(
                    minWidth: 300,
                    child: RaisedButton(
                        child: Text(
                          'Aceptar',
                          style: TextStyle(color: Colors.white),
                        ),
                        color: HexColor('#0F8DBF'),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                        ),
                        onPressed: () async {
                          if (_formKey.currentState.validate()) {
                            await _viewModel.changePassword(password);

                            Navigator.pushReplacementNamed(context, "/");
                          }
                        }),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          title: Text(
            'Cambiar contraseña',
            style: TextStyle(fontSize: 26),
          ),
          backgroundColor: HexColor('#0D518C'),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 50, horizontal: 60),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  SizedBox(height: 20),
                  TextFormField(
                    style: TextStyle(fontSize: 24),
                    decoration: textInputDecoration.copyWith(
                        hintText: 'Contraseña Nueva',
                        prefixIcon: Icon(Icons.vpn_key_rounded),
                        suffixIcon: IconButton(
                            icon: Icon(
                              _passwordVisibility
                                  ? Icons.visibility_off
                                  : Icons.visibility,
                            ),
                            onPressed: _showPassword)),
                    obscureText: _passwordVisibility,
                    validator: (val) => val.length < 6
                        ? 'Introduzca una contraseña de más de 6 caracteres'
                        : null,
                    onChanged: (value) {
                      setState(() => password = value);
                    },
                  ),
                  SizedBox(height: 20),
                  TextFormField(
                      style: TextStyle(fontSize: 24),
                      decoration: textInputDecoration.copyWith(
                          hintText: 'Repita Contraseña Nueva',
                          prefixIcon: Icon(Icons.vpn_key_rounded),
                          suffixIcon: IconButton(
                              icon: Icon(
                                _passwordVisibility
                                    ? Icons.visibility_off
                                    : Icons.visibility,
                              ),
                              onPressed: _showPassword)),
                      obscureText: _passwordVisibility,
                      validator: (val) => val != password
                          ? 'La contraseña debe ser igual a la anterior'
                          : null),
                  SizedBox(height: 20),
                  ButtonTheme(
                    height: 50,
                    minWidth: 600,
                    child: RaisedButton(
                        child: Text(
                          'Aceptar',
                          style: TextStyle(color: Colors.white, fontSize: 24),
                        ),
                        color: HexColor('#0F8DBF'),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(22.0),
                        ),
                        onPressed: () async {
                          if (_formKey.currentState.validate()) {
                            await _viewModel.changePassword(password);

                            Navigator.pushReplacementNamed(context, "/");
                          }
                        }),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    }
  }
}
