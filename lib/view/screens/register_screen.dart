import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:tfg_app/model/tutor.dart';
import 'package:tfg_app/model/educator.dart';
import 'package:tfg_app/view/widgets/loading.dart';
import 'package:tfg_app/view/widgets/textInputDecoration.dart';
import 'package:tfg_app/view_model/sign_up_view_model.dart';

class RegisterPage extends StatefulWidget {
  RegisterPage({Key key}) : super(key: key);

  @override
  _RegisterPageState createState() {
    return _RegisterPageState();
  }
}

class _RegisterPageState extends State<RegisterPage> {
  final _formKey = GlobalKey<FormState>();

  Tutor tutor;
  Educator educator;
  String email = '';
  String password = '';
  bool _passwordVisibility = true;
  bool loading = false;
  String error = '';
  String name = '';
  String fullname = '';
  String tutorType = 'Tutor';
  int phoneNumber;
  MediaQueryData queryData;

  SignUpViewModel _viewModel = SignUpViewModel();

  @override
  void initState() {
    super.initState();
    _viewModel = new SignUpViewModel();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _showPassword() {
    setState(() {
      _passwordVisibility = !_passwordVisibility;
    });
  }

  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);

    var screenSize = queryData.size;

    if (screenSize.height < 925) {
      return loading
          ? Loading()
          : Scaffold(
              appBar: AppBar(
                title: Text('Registro'),
                leading: IconButton(
                  icon: Icon(Icons.arrow_back, color: Colors.white),
                  onPressed: () => Navigator.of(context).pop(),
                ),
                centerTitle: true,
                backgroundColor: HexColor('#0D518C'),
              ),
              body: SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 20, horizontal: 50),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        SizedBox(height: 20),
                        TextFormField(
                          decoration: textInputDecoration.copyWith(
                              hintText: 'Nombre',
                              prefixIcon: Icon(Icons.person)),
                          validator: (val) =>
                              val.isEmpty ? 'Introduzca un nombre' : null,
                          onChanged: (value) {
                            setState(() => name = value);
                          },
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          decoration: textInputDecoration.copyWith(
                              hintText: 'Apellidos',
                              prefixIcon: Icon(Icons.person_outline)),
                          validator: (val) =>
                              val.isEmpty ? 'Introduzca los apellidos' : null,
                          onChanged: (value) {
                            setState(() => fullname = value);
                          },
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          decoration: textInputDecoration.copyWith(
                              hintText: 'Correo',
                              prefixIcon: Icon(Icons.email)),
                          validator: (val) =>
                              val.isEmpty ? 'Introduzca un correo' : null,
                          keyboardType: TextInputType.emailAddress,
                          onChanged: (value) {
                            setState(() => email = value);
                          },
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          decoration: textInputDecoration.copyWith(
                              hintText: 'Teléfono móvil',
                              prefixIcon: Icon(Icons.phone)),
                          validator: (val) => val.length > 9
                              ? 'El número de teléfono es inválido'
                              : null,
                          keyboardType: TextInputType.phone,
                          onChanged: (value) {
                            setState(() => phoneNumber = int.parse(value));
                          },
                        ),
                        SizedBox(height: 20),
                        DropdownButton(
                            isExpanded: true,
                            value: tutorType,
                            icon: const Icon(Icons.arrow_downward),
                            iconSize: 16,
                            underline: Container(
                              height: 2,
                              color: Colors.lightBlue,
                            ),
                            items: <String>['Tutor', 'Educador']
                                .map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                            onChanged: (String newValue) {
                              setState(() {
                                tutorType = newValue;
                              });
                            }),
                        SizedBox(height: 20),
                        TextFormField(
                          decoration: textInputDecoration.copyWith(
                              hintText: 'Contraseña',
                              prefixIcon: Icon(Icons.vpn_key_rounded),
                              suffixIcon: IconButton(
                                  icon: Icon(
                                    _passwordVisibility
                                        ? Icons.visibility_off
                                        : Icons.visibility,
                                  ),
                                  onPressed: _showPassword)),
                          obscureText: _passwordVisibility,
                          validator: (val) => val.length < 6
                              ? 'Introduzca una contraseña de más de 6 caracteres'
                              : null,
                          onChanged: (value) {
                            setState(() => password = value);
                          },
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                            decoration: textInputDecoration.copyWith(
                                hintText: 'Repita Contraseña',
                                prefixIcon: Icon(Icons.vpn_key_rounded),
                                suffixIcon: IconButton(
                                    icon: Icon(
                                      _passwordVisibility
                                          ? Icons.visibility_off
                                          : Icons.visibility,
                                    ),
                                    onPressed: _showPassword)),
                            obscureText: _passwordVisibility,
                            validator: (val) => val != password
                                ? 'La contraseña debe ser igual a la anterior'
                                : null),
                        SizedBox(height: 20),
                        ButtonTheme(
                          minWidth: 300,
                          child: RaisedButton(
                              child: Text(
                                'Regístrate',
                                style: TextStyle(color: Colors.white),
                              ),
                              color: HexColor('#0F8DBF'),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                              ),
                              onPressed: () async {
                                if (_formKey.currentState.validate()) {
                                  setState(() => loading = true);

                                  dynamic result = null;

                                  if (tutorType == 'Tutor') {
                                    setState(() {
                                      tutor = Tutor(
                                          name: name,
                                          fullname: fullname,
                                          phoneNumber: phoneNumber,
                                          email: email,
                                          password: password,
                                          tutorType: tutorType);
                                    });

                                    result =
                                        await _viewModel.registerTutor(tutor);
                                  } else {
                                    setState(() {
                                      educator = Educator(
                                          name: name,
                                          fullname: fullname,
                                          phoneNumber: phoneNumber,
                                          email: email,
                                          password: password,
                                          tutorType: tutorType);
                                    });

                                    result = await _viewModel
                                        .registerEducator(educator);
                                  }

                                  if (result != null) {
                                    print('Registrado');
                                    error = 'No se ha podido registrar.';
                                    Navigator.pushNamed(context, "/");
                                  } else {
                                    setState(() => loading = false);
                                    print('No se pudo registrar');
                                  }
                                }
                              }),
                        ),
                        SizedBox(height: 12.0),
                        Text(
                          error,
                          style: TextStyle(color: Colors.red, fontSize: 14.0),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
    } else {
      return loading
          ? Loading()
          : Scaffold(
              appBar: AppBar(
                title: Text(
                  'Registro',
                  style: TextStyle(fontSize: 26),
                ),
                leading: IconButton(
                  icon: Icon(Icons.arrow_back, color: Colors.white),
                  onPressed: () => Navigator.of(context).pop(),
                ),
                centerTitle: true,
                backgroundColor: HexColor('#0D518C'),
              ),
              body: SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 40, horizontal: 50),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(height: 20),
                        TextFormField(
                          style: TextStyle(fontSize: 24),
                          decoration: textInputDecoration.copyWith(
                              hintText: 'Nombre',
                              prefixIcon: Icon(Icons.person)),
                          validator: (val) =>
                              val.isEmpty ? 'Introduzca un nombre' : null,
                          onChanged: (value) {
                            setState(() => name = value);
                          },
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          style: TextStyle(fontSize: 24),
                          decoration: textInputDecoration.copyWith(
                              hintText: 'Apellidos',
                              prefixIcon: Icon(Icons.person_outline)),
                          validator: (val) =>
                              val.isEmpty ? 'Introduzca los apellidos' : null,
                          onChanged: (value) {
                            setState(() => fullname = value);
                          },
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          style: TextStyle(fontSize: 24),
                          decoration: textInputDecoration.copyWith(
                              hintText: 'Correo',
                              prefixIcon: Icon(Icons.email)),
                          validator: (val) =>
                              val.isEmpty ? 'Introduzca un correo' : null,
                          keyboardType: TextInputType.emailAddress,
                          onChanged: (value) {
                            setState(() => email = value);
                          },
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          style: TextStyle(fontSize: 24),
                          decoration: textInputDecoration.copyWith(
                              hintText: 'Teléfono móvil',
                              prefixIcon: Icon(Icons.phone)),
                          validator: (val) => val.length > 9
                              ? 'El número de teléfono es inválido'
                              : null,
                          keyboardType: TextInputType.phone,
                          onChanged: (value) {
                            setState(() => phoneNumber = int.parse(value));
                          },
                        ),
                        SizedBox(height: 20),
                        DropdownButton(
                            isExpanded: true,
                            value: tutorType,
                            icon: const Icon(Icons.arrow_downward),
                            iconSize: 24,
                            underline: Container(
                              height: 2,
                              color: Colors.lightBlue,
                            ),
                            items: <String>['Tutor', 'Educador']
                                .map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(
                                  value,
                                  style: TextStyle(fontSize: 24),
                                ),
                              );
                            }).toList(),
                            onChanged: (String newValue) {
                              setState(() {
                                tutorType = newValue;
                              });
                            }),
                        SizedBox(height: 20),
                        TextFormField(
                          style: TextStyle(fontSize: 24),
                          decoration: textInputDecoration.copyWith(
                              hintText: 'Contraseña',
                              prefixIcon: Icon(Icons.vpn_key_rounded),
                              suffixIcon: IconButton(
                                  icon: Icon(
                                    _passwordVisibility
                                        ? Icons.visibility_off
                                        : Icons.visibility,
                                  ),
                                  onPressed: _showPassword)),
                          obscureText: _passwordVisibility,
                          validator: (val) => val.length < 6
                              ? 'Introduzca una contraseña de más de 6 caracteres'
                              : null,
                          onChanged: (value) {
                            setState(() => password = value);
                          },
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                            style: TextStyle(fontSize: 24),
                            decoration: textInputDecoration.copyWith(
                                hintText: 'Repita Contraseña',
                                prefixIcon: Icon(Icons.vpn_key_rounded),
                                suffixIcon: IconButton(
                                    icon: Icon(
                                      _passwordVisibility
                                          ? Icons.visibility_off
                                          : Icons.visibility,
                                    ),
                                    onPressed: _showPassword)),
                            obscureText: _passwordVisibility,
                            validator: (val) => val != password
                                ? 'La contraseña debe ser igual a la anterior'
                                : null),
                        SizedBox(height: 150),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: ButtonTheme(
                            height: 50,
                            minWidth: 600,
                            child: RaisedButton(
                                child: Text(
                                  'Regístrate',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 24),
                                ),
                                color: HexColor('#0F8DBF'),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(22.0),
                                ),
                                onPressed: () async {
                                  if (_formKey.currentState.validate()) {
                                    setState(() => loading = true);

                                    dynamic result = null;

                                    if (tutorType == 'Tutor') {
                                      setState(() {
                                        tutor = Tutor(
                                            name: name,
                                            fullname: fullname,
                                            phoneNumber: phoneNumber,
                                            email: email,
                                            password: password,
                                            tutorType: tutorType);
                                      });

                                      result =
                                          await _viewModel.registerTutor(tutor);
                                    } else {
                                      setState(() {
                                        educator = Educator(
                                            name: name,
                                            fullname: fullname,
                                            phoneNumber: phoneNumber,
                                            email: email,
                                            password: password,
                                            tutorType: tutorType);
                                      });

                                      result = await _viewModel
                                          .registerEducator(educator);
                                    }

                                    if (result != null) {
                                      print('Registrado');
                                      error = 'No se ha podido registrar.';
                                      Navigator.pushNamed(context, "/");
                                    } else {
                                      setState(() => loading = false);
                                      print('No se pudo registrar');
                                    }
                                  }
                                }),
                          ),
                        ),
                        SizedBox(height: 12.0),
                        Text(
                          error,
                          style: TextStyle(color: Colors.red, fontSize: 20.0),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
    }
  }
}
