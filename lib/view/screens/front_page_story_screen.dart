import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:tfg_app/model/front_page_arguments.dart';
import 'package:tfg_app/view/screens/read_story_screen.dart';
import 'package:tfg_app/view_model/read_story_view_model.dart';

class StoryFrontPage extends StatefulWidget {
  StoryFrontPage({Key key}) : super(key: key);

  @override
  _StoryFrontPageState createState() {
    return _StoryFrontPageState();
  }
}

class _StoryFrontPageState extends State<StoryFrontPage> {
  ReadStoryViewModel _storyViewModel = ReadStoryViewModel();

  MediaQueryData queryData;

  @override
  void initState() {
    super.initState();
    _storyViewModel = new ReadStoryViewModel();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    FrontPageArguments arguments = ModalRoute.of(context).settings.arguments;

    queryData = MediaQuery.of(context);

    var screenSize = queryData.size;

    if (screenSize.height < 825) {
      return Scaffold(
        body: Container(
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    GestureDetector(
                        child: Icon(Icons.navigate_before_rounded,
                            size: 50, color: HexColor('#071A40')),
                        onTap: () => Navigator.pop(context)),
                  ],
                ),
                Container(
                    height: 200,
                    child: (arguments.front == null || arguments.front == ''
                        ? SizedBox(height: 20)
                        : Image.network(arguments.front))),
                SizedBox(height: 10),
                Text(
                  arguments.title.toUpperCase(),
                  style: GoogleFonts.lato(
                      fontWeight: FontWeight.bold, fontSize: 22),
                ),
                SizedBox(height: 5),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Creado por: '),
                    Text(arguments.author,
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ],
                ),
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    FlatButton(
                        child: Text(
                          'Leer Cuento'.toUpperCase(),
                          style: GoogleFonts.lato(
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                              color: HexColor('#0D518C')),
                        ),
                        onPressed: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => ReadStoryPage(
                                  storyID: arguments.storyID,
                                  numPages: arguments.numPages,
                                  audio: false),
                            ))),
                    FlatButton(
                        child: Text(
                          'Audiolectura'.toUpperCase(),
                          style: GoogleFonts.lato(
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                              color: HexColor('#0D518C')),
                        ),
                        onPressed: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => ReadStoryPage(
                                  storyID: arguments.storyID,
                                  numPages: arguments.numPages,
                                  audio: true),
                            )))
                  ],
                ),
              ],
            ),
          ),
        ),
      );
    } else {
      return Scaffold(
        body: Container(
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    GestureDetector(
                        child: Icon(Icons.navigate_before_rounded,
                            size: 100, color: HexColor('#071A40')),
                        onTap: () => Navigator.pop(context)),
                  ],
                ),
                SizedBox(height: 10),
                Container(
                    height: 440,
                    child: (arguments.front == null || arguments.front == ''
                        ? SizedBox(height: 20)
                        : Image.network(arguments.front))),
                SizedBox(height: 10),
                Text(
                  arguments.title.toUpperCase(),
                  style: GoogleFonts.lato(
                      fontWeight: FontWeight.bold, fontSize: 40),
                ),
                SizedBox(height: 5),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Creado por: ', style: TextStyle(fontSize: 20)),
                    Text(arguments.author,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20)),
                  ],
                ),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    FlatButton(
                        child: Text(
                          'Leer Cuento'.toUpperCase(),
                          style: GoogleFonts.lato(
                              fontWeight: FontWeight.bold,
                              fontSize: 36,
                              color: HexColor('#0D518C')),
                        ),
                        onPressed: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => ReadStoryPage(
                                  storyID: arguments.storyID,
                                  numPages: arguments.numPages,
                                  audio: false),
                            ))),
                    SizedBox(width: 60),
                    FlatButton(
                        child: Text(
                          'Audiolectura'.toUpperCase(),
                          style: GoogleFonts.lato(
                              fontWeight: FontWeight.bold,
                              fontSize: 36,
                              color: HexColor('#0D518C')),
                        ),
                        onPressed: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => ReadStoryPage(
                                  storyID: arguments.storyID,
                                  numPages: arguments.numPages,
                                  audio: true),
                            )))
                  ],
                ),
              ],
            ),
          ),
        ),
      );
    }
  }
}
