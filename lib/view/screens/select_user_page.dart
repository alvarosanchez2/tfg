import 'package:animated_search_bar/animated_search_bar.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:tfg_app/model/user.dart';
import 'package:tfg_app/view/screens/assign_tutor_screen.dart';
import 'package:tfg_app/view/screens/edit_user_info.dart';
import 'package:tfg_app/view/screens/select_genre_screen.dart';
import 'package:tfg_app/view/widgets/loading_users.dart';
import 'package:tfg_app/view_model/create_story_view_model.dart';
import 'package:tfg_app/view_model/read_story_view_model.dart';
import 'package:tfg_app/view_model/sign_up_view_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SelectUserPage extends StatefulWidget {
  SelectUserPage({Key key}) : super(key: key);

  @override
  _SelectUserPageState createState() {
    return _SelectUserPageState();
  }
}

class _SelectUserPageState extends State<SelectUserPage> {
  SignUpViewModel _viewModel = SignUpViewModel();
  ReadStoryViewModel _storyViewModel = ReadStoryViewModel();

  String tutorType = '';
  String name = "";
  bool isEducator = false;
  MediaQueryData queryData;

  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]);
    _viewModel = new SignUpViewModel();
    _storyViewModel = new ReadStoryViewModel();
    _viewModel.getTutorType().then((String result) {
      setState(() {
        tutorType = result;
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);

    var screenSize = queryData.size;

    Future<bool> _onWillScope() async {
      await _viewModel.logOut();
      Navigator.pushReplacementNamed(context, '/');
      return Future.value(true);
    }

    if (screenSize.height < 925) {
      return WillPopScope(
        onWillPop: _onWillScope,
        child: Scaffold(
          appBar: AppBar(
            title: Center(child: Text('Gestión de niños')),
            backgroundColor: HexColor('#0D518C'),
            centerTitle: true,
            leading: IconButton(
              icon: const Icon(Icons.vpn_key),
              tooltip: 'Ajustes',
              onPressed: () => Navigator.pushNamed(context, '/change-password'),
            ),
            actions: [
              IconButton(
                icon: const Icon(Icons.logout),
                tooltip: 'Cerrar sesión',
                onPressed: () async {
                  await _viewModel.logOut();
                  Navigator.pushReplacementNamed(context, '/');
                },
              ),
            ],
            automaticallyImplyLeading: false,
          ),
          body: StreamBuilder<QuerySnapshot>(
            stream: Stream.fromFuture((name != null || name != '')
                ? _viewModel.getUsersData(tutorType, name)
                : _viewModel.getUsersData(tutorType, null)),
            builder:
                (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (!snapshot.hasData) return new LoadingUsers();
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 55, top: 10, right: 30),
                    child: AnimatedSearchBar(
                      label: "Busca un niño/a",
                      labelStyle: TextStyle(
                          color: HexColor('#071A40'),
                          fontWeight: FontWeight.bold),
                      searchDecoration: InputDecoration(
                        hintText: "Buscar...",
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: HexColor('#0F8DBF'), width: 2.0)),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: HexColor('#0F8DBF'), width: 2.0)),
                      ),
                      onChanged: (value) {
                        setState(() {
                          name = value;
                        });
                      },
                    ),
                  ),
                  SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      (tutorType.toString() == 'Tutor')
                          ? RaisedButton(
                              onPressed: () => Navigator.pushNamed(
                                  context, '/register-user'),
                              child: Text(
                                'Dar de alta niño/a',
                                style: TextStyle(color: Colors.white),
                              ),
                              color: HexColor('#0F8DBF'))
                          : SizedBox(height: 0),
                      (tutorType.toString() == 'Tutor'
                          ? SizedBox(width: 20)
                          : SizedBox(width: 0)),
                      RaisedButton(
                          onPressed: () =>
                              Navigator.pushNamed(context, '/manage-book'),
                          child: Text(
                            'Gestionar cuentos',
                            style: TextStyle(color: Colors.white),
                          ),
                          color: HexColor('#0F8DBF'))
                    ],
                  ),
                  (snapshot.data.documents.length == 0
                      ? Center(
                          child: Column(
                            children: [
                              SizedBox(height: 10),
                              Text('No se han encontrado usuarios'),
                            ],
                          ),
                        )
                      : Expanded(
                          child: ListView.builder(
                              itemCount: snapshot.data.documents.length,
                              itemBuilder: (BuildContext context, int index) {
                                return new Column(
                                  children: [
                                    GestureDetector(
                                      onTap: () {
                                        SystemChrome.setPreferredOrientations([
                                          DeviceOrientation.landscapeLeft,
                                          DeviceOrientation.landscapeRight
                                        ]);

                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) =>
                                                  SelectGenrePage(
                                                      userID: snapshot
                                                          .data
                                                          .documents[index]
                                                          .documentID),
                                            ));
                                        //Navigator.pushNamed(context, "/home");
                                      },
                                      child: Container(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 20, horizontal: 50),
                                        child: Card(
                                          child: SizedBox(
                                              height: 170,
                                              child: Column(
                                                children: [
                                                  (tutorType.toString() ==
                                                          'Tutor')
                                                      ? Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .end,
                                                          children: [
                                                            RaisedButton(
                                                                onPressed: () =>
                                                                    Navigator
                                                                        .push(
                                                                            context,
                                                                            MaterialPageRoute(
                                                                              builder: (context) => AssignTutorPage(userID: snapshot.data.documents[index].documentID),
                                                                            )),
                                                                child: Text(
                                                                  'Asignar responsable',
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                          .white),
                                                                ),
                                                                color: HexColor(
                                                                    '#72CEF2')),
                                                            SizedBox(width: 30),
                                                            GestureDetector(
                                                              onTap: () async {
                                                                User user = await _viewModel
                                                                    .getUserInfo(snapshot
                                                                        .data
                                                                        .documents[
                                                                            index]
                                                                        .documentID);

                                                                if (tutorType
                                                                        .toString() !=
                                                                    'Tutor') {
                                                                  isEducator =
                                                                      true;
                                                                }

                                                                Navigator.push(
                                                                    context,
                                                                    MaterialPageRoute(
                                                                        builder: (context) => EditUserInfo(
                                                                            userInfo:
                                                                                user,
                                                                            isEducator:
                                                                                isEducator)));
                                                              },
                                                              child: Icon(
                                                                  Icons
                                                                      .settings,
                                                                  size: 30,
                                                                  color: Colors
                                                                      .grey),
                                                            ),
                                                            SizedBox(width: 10)
                                                          ],
                                                        )
                                                      : RaisedButton(
                                                          child: Text(
                                                            'Consultar datos',
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white),
                                                          ),
                                                          color: HexColor(
                                                              '#72CEF2'),
                                                          onPressed: () async {
                                                            User user = await _viewModel
                                                                .getUserInfo(snapshot
                                                                    .data
                                                                    .documents[
                                                                        index]
                                                                    .documentID);

                                                            if (tutorType
                                                                    .toString() !=
                                                                'Tutor') {
                                                              isEducator = true;
                                                            }

                                                            Navigator.push(
                                                                context,
                                                                MaterialPageRoute(
                                                                    builder: (context) => EditUserInfo(
                                                                        userInfo:
                                                                            user,
                                                                        isEducator:
                                                                            isEducator)));
                                                          }),
                                                  ListTile(
                                                    title: Center(
                                                      child: new Text(snapshot
                                                                      .data
                                                                      .documents[
                                                                          index]
                                                                      .data[
                                                                  'name'] +
                                                              " " +
                                                              snapshot
                                                                      .data
                                                                      .documents[
                                                                          index]
                                                                      .data[
                                                                  'fullname'] ??
                                                          ''),
                                                    ),
                                                    subtitle: Center(
                                                      child: Column(
                                                        children: [
                                                          SizedBox(height: 5),
                                                          Text("Tiene " +
                                                                  snapshot
                                                                      .data
                                                                      .documents[
                                                                          index]
                                                                      .data[
                                                                          'age']
                                                                      .toString() +
                                                                  ' años' ??
                                                              ''),
                                                          SizedBox(height: 5),
                                                          Text("Su tipo de TEA es: " +
                                                                  snapshot
                                                                      .data
                                                                      .documents[
                                                                          index]
                                                                      .data['teaType'] ??
                                                              '')
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  FlatButton(
                                                      child: Text(
                                                          "Comenzar la lectura",
                                                          style: TextStyle(
                                                              color: HexColor(
                                                                  '#0F8DBF'))),
                                                      onPressed: () {
                                                        SystemChrome
                                                            .setPreferredOrientations([
                                                          DeviceOrientation
                                                              .landscapeLeft,
                                                          DeviceOrientation
                                                              .landscapeRight
                                                        ]);

                                                        Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                              builder: (context) => SelectGenrePage(
                                                                  userID: snapshot
                                                                      .data
                                                                      .documents[
                                                                          index]
                                                                      .documentID),
                                                            ));
                                                      })
                                                ],
                                              )),
                                        ),
                                      ),
                                    )
                                  ],
                                );
                              }),
                        )),
                ],
              );
            },
          ),
        ),
      );
    } else {
      return WillPopScope(
        onWillPop: _onWillScope,
        child: Scaffold(
          appBar: AppBar(
            title: Center(
                child:
                    Text('Gestión de niños', style: TextStyle(fontSize: 24))),
            backgroundColor: HexColor('#0D518C'),
            centerTitle: true,
            leading: IconButton(
              icon: const Icon(Icons.vpn_key, size: 30),
              tooltip: 'Ajustes',
              onPressed: () => Navigator.pushNamed(context, '/change-password'),
            ),
            actions: [
              IconButton(
                icon: const Icon(
                  Icons.logout,
                  size: 30,
                ),
                tooltip: 'Cerrar sesión',
                onPressed: () async {
                  await _viewModel.logOut();
                  Navigator.pushReplacementNamed(context, '/');
                },
              ),
            ],
            automaticallyImplyLeading: false,
          ),
          body: StreamBuilder<QuerySnapshot>(
            stream: Stream.fromFuture((name != null || name != '')
                ? _viewModel.getUsersData(tutorType, name)
                : _viewModel.getUsersData(tutorType, null)),
            builder:
                (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (!snapshot.hasData) return new LoadingUsers();
              return Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 200, top: 20, right: 150),
                    child: AnimatedSearchBar(
                      label: "Busca un niño/a",
                      labelStyle: TextStyle(
                          color: HexColor('#071A40'),
                          fontWeight: FontWeight.bold,
                          fontSize: 20),
                      searchDecoration: InputDecoration(
                        hintText: "Buscar...",
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: HexColor('#0F8DBF'), width: 2.0)),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: HexColor('#0F8DBF'), width: 2.0)),
                      ),
                      onChanged: (value) {
                        setState(() {
                          name = value;
                        });
                      },
                    ),
                  ),
                  SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      (tutorType.toString() == 'Tutor')
                          ? ButtonTheme(
                              height: 50,
                              child: RaisedButton(
                                  onPressed: () => Navigator.pushNamed(
                                      context, '/register-user'),
                                  child: Text(
                                    'Dar de alta niño/a',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 20),
                                  ),
                                  color: HexColor('#0F8DBF')),
                            )
                          : SizedBox(height: 20),
                      SizedBox(width: 20),
                      ButtonTheme(
                        height: 50,
                        child: RaisedButton(
                            onPressed: () =>
                                Navigator.pushNamed(context, '/manage-book'),
                            child: Text(
                              'Gestionar cuentos',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                            ),
                            color: HexColor('#0F8DBF')),
                      )
                    ],
                  ),
                  (snapshot.data.documents.length == 0
                      ? Center(
                          child: Column(
                            children: [
                              SizedBox(height: 10),
                              Text('No se han encontrado usuarios',
                                  style: TextStyle(fontSize: 20)),
                            ],
                          ),
                        )
                      : Expanded(
                          child: ListView.builder(
                              itemCount: snapshot.data.documents.length,
                              itemBuilder: (BuildContext context, int index) {
                                return new Column(
                                  children: [
                                    GestureDetector(
                                      onTap: () {
                                        SystemChrome.setPreferredOrientations([
                                          DeviceOrientation.landscapeLeft,
                                          DeviceOrientation.landscapeRight
                                        ]);

                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) =>
                                                  SelectGenrePage(
                                                      userID: snapshot
                                                          .data
                                                          .documents[index]
                                                          .documentID),
                                            ));
                                        //Navigator.pushNamed(context, "/home");
                                      },
                                      child: Container(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 10, horizontal: 200),
                                        child: Card(
                                          child: SizedBox(
                                              height: 230,
                                              child: Column(
                                                children: [
                                                  SizedBox(height: 15),
                                                  (tutorType.toString() ==
                                                          'Tutor')
                                                      ? Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .end,
                                                          children: [
                                                            ButtonTheme(
                                                              height: 50,
                                                              child: RaisedButton(
                                                                  onPressed: () =>
                                                                      Navigator
                                                                          .push(
                                                                              context,
                                                                              MaterialPageRoute(
                                                                                builder: (context) => AssignTutorPage(userID: snapshot.data.documents[index].documentID),
                                                                              )),
                                                                  child: Text(
                                                                    'Asignar responsable',
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .white, fontSize: 20),
                                                                  ),
                                                                  color: HexColor(
                                                                      '#72CEF2')),
                                                            ),
                                                            SizedBox(width: 73),
                                                            GestureDetector(
                                                              onTap: () async {
                                                                User user = await _viewModel
                                                                    .getUserInfo(snapshot
                                                                        .data
                                                                        .documents[
                                                                            index]
                                                                        .documentID);

                                                                if (tutorType
                                                                        .toString() !=
                                                                    'Tutor') {
                                                                  isEducator =
                                                                      true;
                                                                }

                                                                Navigator.push(
                                                                    context,
                                                                    MaterialPageRoute(
                                                                        builder: (context) => EditUserInfo(
                                                                            userInfo:
                                                                                user,
                                                                            isEducator:
                                                                                isEducator)));
                                                              },
                                                              child: Icon(
                                                                  Icons
                                                                      .settings,
                                                                  size: 45,
                                                                  color: Colors
                                                                      .grey),
                                                            ),
                                                            SizedBox(width: 20)
                                                          ],
                                                        )
                                                      : ButtonTheme(
                                                        height: 50,
                                                        child: RaisedButton(
                                                            child: Text(
                                                              'Consultar datos',
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white, fontSize: 20),
                                                            ),
                                                            color: HexColor(
                                                                '#72CEF2'),
                                                            onPressed: () async {
                                                              User user = await _viewModel
                                                                  .getUserInfo(snapshot
                                                                      .data
                                                                      .documents[
                                                                          index]
                                                                      .documentID);

                                                              if (tutorType
                                                                      .toString() !=
                                                                  'Tutor') {
                                                                isEducator = true;
                                                              }

                                                              Navigator.push(
                                                                  context,
                                                                  MaterialPageRoute(
                                                                      builder: (context) => EditUserInfo(
                                                                          userInfo:
                                                                              user,
                                                                          isEducator:
                                                                              isEducator)));
                                                            }),
                                                      ),
                                                  SizedBox(height: 10),
                                                  ListTile(
                                                    title: Center(
                                                      child: new Text(
                                                          snapshot
                                                                          .data
                                                                          .documents[
                                                                              index]
                                                                          .data[
                                                                      'name'] +
                                                                  " " +
                                                                  snapshot
                                                                          .data
                                                                          .documents[
                                                                              index]
                                                                          .data[
                                                                      'fullname'] ??
                                                              '',
                                                          style: TextStyle(
                                                              fontSize: 22)),
                                                    ),
                                                    subtitle: Center(
                                                      child: Column(
                                                        children: [
                                                          SizedBox(height: 5),
                                                          Text(
                                                              "Tiene " +
                                                                      snapshot
                                                                          .data
                                                                          .documents[
                                                                              index]
                                                                          .data[
                                                                              'age']
                                                                          .toString() +
                                                                      ' años' ??
                                                                  '',
                                                              style: TextStyle(
                                                                  fontSize:
                                                                      20)),
                                                          SizedBox(height: 5),
                                                          Text(
                                                              "Su tipo de TEA es: " +
                                                                      snapshot.data
                                                                              .documents[index].data[
                                                                          'teaType'] ??
                                                                  '',
                                                              style: TextStyle(
                                                                  fontSize:
                                                                      20)),
                                                          FlatButton(
                                                              child: Text(
                                                                  "Comenzar la lectura",
                                                                  style: TextStyle(
                                                                      color: HexColor(
                                                                          '#0F8DBF'),
                                                                      fontSize:
                                                                          22)),
                                                              onPressed: () {
                                                                SystemChrome
                                                                    .setPreferredOrientations([
                                                                  DeviceOrientation
                                                                      .landscapeLeft,
                                                                  DeviceOrientation
                                                                      .landscapeRight
                                                                ]);

                                                                Navigator.push(
                                                                    context,
                                                                    MaterialPageRoute(
                                                                      builder: (context) => SelectGenrePage(
                                                                          userID: snapshot
                                                                              .data
                                                                              .documents[index]
                                                                              .documentID),
                                                                    ));
                                                              })
                                                        ],
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              )),
                                        ),
                                      ),
                                    )
                                  ],
                                );
                              }),
                        )),
                ],
              );
            },
          ),
        ),
      );
    }
  }
}
