import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:tfg_app/model/services/auth_service.dart';
import 'package:tfg_app/model/user.dart';
import 'package:tfg_app/view/widgets/loading.dart';
import 'package:tfg_app/view/widgets/textInputDecoration.dart';
import 'package:provider/provider.dart';
import 'package:tfg_app/view_model/sign_up_view_model.dart';
import 'package:intl/intl.dart';

class RegisterUserPage extends StatefulWidget {
  RegisterUserPage({Key key}) : super(key: key);

  @override
  _RegisterUserPageState createState() {
    return _RegisterUserPageState();
  }
}

class _RegisterUserPageState extends State<RegisterUserPage> {
  final _formKey = GlobalKey<FormState>();

  User user;
  String email = '';
  String password = '';
  String name = '';
  String fullname = '';
  int age = 0;
  String teaType = '';
  String favGenre = '';
  String interest = '';
  bool loading = false;
  String error = '';
  MediaQueryData queryData;
  TextEditingController dateinput = TextEditingController();

  SignUpViewModel _viewModel = SignUpViewModel();

  @override
  void initState() {
    super.initState();
    dateinput.text = "";
    _viewModel = new SignUpViewModel();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);

    var screenSize = queryData.size;

    if (screenSize.height < 925) {
      return loading
          ? Loading()
          : Scaffold(
              appBar: AppBar(
                title: Text(
                  'Registro de niños',
                ),
                leading: IconButton(
                  icon: Icon(Icons.arrow_back, color: Colors.white),
                  onPressed: () => Navigator.of(context).pop(),
                ),
                centerTitle: true,
                backgroundColor: HexColor('#0D518C'),
              ),
              body: SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 50, horizontal: 50),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        TextFormField(
                          decoration: textInputDecoration.copyWith(
                              hintText: 'Nombre',
                              prefixIcon: Icon(Icons.person)),
                          validator: (val) =>
                              val.isEmpty ? 'Introduzca un nombre' : null,
                          onChanged: (value) {
                            setState(() => name = value);
                          },
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          decoration: textInputDecoration.copyWith(
                              hintText: 'Apellidos',
                              prefixIcon: Icon(Icons.person_outline)),
                          validator: (val) =>
                              val.isEmpty ? 'Introduzca los apellidos' : null,
                          onChanged: (value) {
                            setState(() => fullname = value);
                          },
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          decoration: textInputDecoration.copyWith(
                              hintText: 'Correo (Opcional)',
                              prefixIcon: Icon(Icons.email)),
                          keyboardType: TextInputType.emailAddress,
                          onChanged: (value) {
                            setState(() => email = value);
                          },
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          validator: (val) =>
                          val.isEmpty ? 'Introduzca una fecha de nacimiento' : null ,
                          controller: dateinput, //editing controller of this TextField
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.calendar_today), //icon of text field
                              labelText: "Introduzca la fecha de nacimiento" //label text of field
                          ),
                          readOnly: true,  //set it true, so that user will not able to edit text
                          onTap: () async {
                            DateTime pickedDate = await showDatePicker(
                                context: context, initialDate: DateTime.now(),
                                firstDate: DateTime(1980), //DateTime.now() - not to allow to choose before today.
                                lastDate: DateTime(2101)
                            );

                            if(pickedDate != null ){
                              print(pickedDate);  //pickedDate output format => 2021-03-10 00:00:00.000
                              String formattedDate = DateFormat('dd-MM-yyyy').format(pickedDate);
                              print(formattedDate); //formatted date output using intl package =>  2021-03-16
                              //you can implement different kind of Date Format here according to your requirement

                              setState(() {
                                dateinput.text = formattedDate; //set output date to TextField value.
                              });
                            }else{
                              print("La fecha no ha sido seleccionada");
                            }
                          },
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          decoration: textInputDecoration.copyWith(
                              hintText: 'Tipo de diversidad funcional',
                              prefixIcon: Icon(Icons.accessibility)),
                          validator: (val) =>
                              val.isEmpty ? 'Introduzca el tipo de diversidad funcional' : null,
                          onChanged: (value) {
                            setState(() => teaType = value);
                          },
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          decoration: textInputDecoration.copyWith(
                              hintText: 'Género favorito',
                              prefixIcon: Icon(Icons.menu_book)),
                          validator: (val) =>
                          val.isEmpty ? 'Introduzca el género favorito' : null,
                          onChanged: (value) {
                            setState(() => favGenre = value);
                          },
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          decoration: textInputDecoration.copyWith(
                              hintText: 'Intereses',
                              prefixIcon: Icon(Icons.favorite)),
                          validator: (val) =>
                          val.isEmpty ? 'Introduzca los intereses del niño/a' : null,
                          onChanged: (value) {
                            setState(() => interest = value);
                          },
                        ),
                        SizedBox(height: 20),
                        ButtonTheme(
                          minWidth: 300,
                          child: RaisedButton(
                              child: Text('Registrar',
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.white)),
                              color: HexColor('#0F8DBF'),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                              ),
                              onPressed: () async {
                                if (_formKey.currentState.validate()) {
                                  setState(() => loading = true);

                                  String birthYear = dateinput.text.split('-').elementAt(2);

                                  String actualYear = DateTime.now().toString().split('-').elementAt(0);

                                  setState(() {
                                    age = int.parse(actualYear) - int.parse(birthYear) ;
                                  });

                                  print(birthYear);
                                  print(actualYear);
                                  print(age);

                                  setState(() {
                                    user = User(
                                        name: name,
                                        fullname: fullname,
                                        age: age,
                                        teaType: teaType,
                                        email: email,
                                        password: password,
                                        birthDate: dateinput.text,
                                        favGenre: favGenre,
                                        interest: interest
                                    );
                                  });
                                  dynamic result =
                                      await _viewModel.registerUser(user);

                                  if (result != null) {
                                    print('Registrado');
                                    Navigator.pushNamed(
                                        context, "/select-user");
                                  } else {
                                    setState(() {
                                      error = 'No se ha podido registrar.';
                                      setState(() => loading = false);
                                    });
                                    print('No se pudo registrar');
                                  }
                                }
                              }),
                        ),
                        SizedBox(height: 12.0),
                        Text(
                          error,
                          style: TextStyle(color: Colors.red, fontSize: 14.0),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
    } else {
      return loading
          ? Loading()
          : Scaffold(
              appBar: AppBar(
                title: Text(
                  'Registro de niños',
                  style: TextStyle(fontSize: 26),
                ),
                leading: IconButton(
                  icon: Icon(Icons.arrow_back, color: Colors.white),
                  onPressed: () => Navigator.of(context).pop(),
                ),
                centerTitle: true,
                backgroundColor: HexColor('#0D518C'),
              ),
              body: SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 40, horizontal: 50),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        TextFormField(
                          style: TextStyle(fontSize: 24),
                          decoration: textInputDecoration.copyWith(
                              hintText: 'Nombre',
                              prefixIcon: Icon(Icons.person)),
                          validator: (val) =>
                              val.isEmpty ? 'Introduzca un nombre' : null,
                          onChanged: (value) {
                            setState(() => name = value);
                          },
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          style: TextStyle(fontSize: 24),
                          decoration: textInputDecoration.copyWith(
                              hintText: 'Apellidos',
                              prefixIcon: Icon(Icons.person_outline)),
                          validator: (val) =>
                              val.isEmpty ? 'Introduzca los apellidos' : null,
                          onChanged: (value) {
                            setState(() => fullname = value);
                          },
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          style: TextStyle(fontSize: 24),
                          decoration: textInputDecoration.copyWith(
                              hintText: 'Correo (Opcional)',
                              prefixIcon: Icon(Icons.email)),
                          keyboardType: TextInputType.emailAddress,
                          onChanged: (value) {
                            setState(() => email = value);
                          },
                        ),
                        SizedBox(height: 10),
                        TextFormField(
                          style: TextStyle(fontSize: 24),
                          validator: (val) =>
                          val.isEmpty ? 'Introduzca una fecha de nacimiento' : null ,
                          controller: dateinput, //editing controller of this TextField
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.calendar_today), //icon of text field
                              labelText: "Introduzca la fecha de nacimiento" //label text of field
                          ),
                          readOnly: true,  //set it true, so that user will not able to edit text
                          onTap: () async {
                            DateTime pickedDate = await showDatePicker(
                                context: context, initialDate: DateTime.now(),
                                firstDate: DateTime(1980), //DateTime.now() - not to allow to choose before today.
                                lastDate: DateTime(2101)
                            );

                            if(pickedDate != null ){
                              print(pickedDate);  //pickedDate output format => 2021-03-10 00:00:00.000
                              String formattedDate = DateFormat('dd-MM-yyyy').format(pickedDate);
                              print(formattedDate); //formatted date output using intl package =>  2021-03-16
                              //you can implement different kind of Date Format here according to your requirement

                              setState(() {
                                dateinput.text = formattedDate; //set output date to TextField value.
                              });
                            }else{
                              print("La fecha no ha sido seleccionada");
                            }
                          },
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          style: TextStyle(fontSize: 24),
                          decoration: textInputDecoration.copyWith(
                              hintText: 'Tipo de diversidad funcional',
                              prefixIcon: Icon(Icons.accessibility)),
                          validator: (val) =>
                          val.isEmpty ? 'Introduzca el tipo de diversidad funcional' : null,
                          onChanged: (value) {
                            setState(() => teaType = value);
                          },
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          style: TextStyle(fontSize: 24),
                          decoration: textInputDecoration.copyWith(
                              hintText: 'Género favorito',
                              prefixIcon: Icon(Icons.menu_book)),
                          validator: (val) =>
                          val.isEmpty ? 'Introduzca el género favorito' : null,
                          onChanged: (value) {
                            setState(() => favGenre = value);
                          },
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          style: TextStyle(fontSize: 24),
                          decoration: textInputDecoration.copyWith(
                              hintText: 'Intereses',
                              prefixIcon: Icon(Icons.favorite)),
                          validator: (val) =>
                          val.isEmpty ? 'Introduzca los intereses del niño/a' : null,
                          onChanged: (value) {
                            setState(() => interest = value);
                          },
                        ),
                        SizedBox(height: 100),
                        ButtonTheme(
                          height: 50,
                          minWidth: 600,
                          child: RaisedButton(
                              child: Text('Registrar',
                                  style: TextStyle(
                                      fontSize: 24, color: Colors.white)),
                              color: HexColor('#0F8DBF'),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(24.0),
                              ),
                              onPressed: () async {
                                if (_formKey.currentState.validate()) {
                                  setState(() => loading = true);

                                  String birthYear = dateinput.text.split('-').elementAt(2);

                                  String actualYear = DateTime.now().toString().split('-').elementAt(0);

                                  setState(() {
                                    age = int.parse(actualYear) - int.parse(birthYear) ;
                                  });

                                  print(birthYear);
                                  print(actualYear);
                                  print(age);

                                  setState(() {
                                    user = User(
                                        name: name,
                                        fullname: fullname,
                                        age: age,
                                        teaType: teaType,
                                        email: email,
                                        password: password,
                                        birthDate: dateinput.text,
                                        favGenre: favGenre,
                                        interest: interest
                                    );
                                  });
                                  dynamic result =
                                  await _viewModel.registerUser(user);

                                  if (result != null) {
                                    print('Registrado');
                                    Navigator.pushNamed(
                                        context, "/select-user");
                                  } else {
                                    setState(() {
                                      error = 'No se ha podido registrar.';
                                      setState(() => loading = false);
                                    });
                                    print('No se pudo registrar');
                                  }
                                }
                              }),
                        ),
                        SizedBox(height: 12.0),
                        Text(
                          error,
                          style: TextStyle(color: Colors.red, fontSize: 20.0),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
    }
  }
}
