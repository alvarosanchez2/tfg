import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tfg_app/model/front_page_arguments.dart';
import 'package:tfg_app/model/story.dart';
import 'package:tfg_app/view/widgets/loading_users.dart';
import 'package:tfg_app/view_model/read_story_view_model.dart';

class SelectGenreStoryPage extends StatefulWidget {
  final String genre;
  final String userID;
  SelectGenreStoryPage({Key key, this.genre, this.userID}) : super(key: key);

  @override
  _SelectGenreStoryPageState createState() {
    return _SelectGenreStoryPageState(genre: genre, userID: userID);
  }
}

class _SelectGenreStoryPageState extends State<SelectGenreStoryPage> {
  String genre;
  String userID;
  _SelectGenreStoryPageState({this.genre, this.userID});

  ReadStoryViewModel _storyViewModel = ReadStoryViewModel();

  String title = '';
  String author = '';
  String numPages = '';
  String storyID = '';
  String front = '';
  MediaQueryData queryData;

  @override
  void initState() {
    super.initState();
    _storyViewModel = new ReadStoryViewModel();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);

    var screenSize = queryData.size;

    if (screenSize.height < 825) {
      return Scaffold(
        body: StreamBuilder<List<Story>>(
            stream: _storyViewModel.getStoriesByGenre(genre, userID),
            builder:
                (BuildContext context, AsyncSnapshot<List<Story>> snapshot) {
              if (!snapshot.hasData) return new LoadingUsers();
              return Center(
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 10.0),
                  child: GridView.builder(
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,
                      ),
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () {
                            title = snapshot.data[index].title;
                            author = snapshot.data[index].author;
                            numPages = snapshot.data[index].numPages;
                            storyID = snapshot.data[index].storyID;
                            front = snapshot.data[index].front;
                            Navigator.of(context).pushNamed("/story-front-page",
                                arguments: FrontPageArguments(
                                    storyID: storyID,
                                    title: title,
                                    author: author,
                                    numPages: numPages,
                                    front: front));
                          },
                          child: Card(
                            //color: HexColor('#FEA8B7'),
                            child: Container(
                              padding: EdgeInsets.only(left: 10, right: 10),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  (snapshot.data[index].front ==
                                              null ||
                                          snapshot.data[index].front == ''
                                      ? Expanded(child: SizedBox(height: 20))
                                      : Expanded(
                                        child: Image.network(
                                            snapshot.data[index].front),
                                      )),
                                  SizedBox(height: 10),
                                  Text(
                                    snapshot.data[index].title
                                        .toUpperCase(),
                                    style: GoogleFonts.lato(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      }),
                ),
              );
            }),
      );
    } else {
      return Scaffold(
        body: StreamBuilder<List<Story>>(
            stream: _storyViewModel.getStoriesByGenre(genre, userID),
            builder:
                (BuildContext context, AsyncSnapshot<List<Story>> snapshot) {
              if (!snapshot.hasData) return new LoadingUsers();
              return Center(
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 10.0),
                  child: GridView.builder(
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,
                      ),
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () {
                            title = snapshot.data[index].title;
                            author = snapshot.data[index].author;
                            numPages = snapshot.data[index].numPages;
                            storyID = snapshot.data[index].storyID;
                            front = snapshot.data[index].front;
                            Navigator.of(context).pushNamed("/story-front-page",
                                arguments: FrontPageArguments(
                                    storyID: storyID,
                                    title: title,
                                    author: author,
                                    numPages: numPages,
                                    front: front));
                          },
                          child: Container(
                            height: 300,
                            width: 400,
                            child: Card(
                              //color: HexColor('#FEA8B7'),
                              child: Container(
                                padding: EdgeInsets.only(left: 10, right: 10),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    (snapshot.data[index].front ==
                                                null ||
                                            snapshot.data[index].front == ''
                                        ? SizedBox(height: 20)
                                        : Expanded(
                                          child: Image.network(
                                              snapshot.data[index].front),
                                        )),
                                    SizedBox(height: 10),
                                    Text(
                                      snapshot.data[index].title
                                          .toUpperCase(),
                                      style: GoogleFonts.lato(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 34),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        );
                      }),
                ),
              );
            }),
      );
    }
  }
}
