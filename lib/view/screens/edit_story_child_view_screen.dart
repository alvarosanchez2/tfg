import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:tfg_app/view/screens/select_image_screen.dart';
import 'package:tfg_app/view/widgets/loading_words.dart';
import 'package:tfg_app/view_model/create_story_view_model.dart';
import 'package:tfg_app/view_model/read_story_view_model.dart';

import 'add_image_to_page_screen.dart';
import 'assign_story_to_user.dart';

class EditChildViewPage extends StatefulWidget {
  final String storyID;
  EditChildViewPage({Key key, this.storyID}) : super(key: key);

  @override
  _EditChildViewPageState createState() {
    return _EditChildViewPageState(storyID: storyID);
  }
}

class _EditChildViewPageState extends State<EditChildViewPage> {
  String storyID;
  _EditChildViewPageState({this.storyID});

  MediaQueryData queryData;

  var currentPageInfo;
  var imageInfo;
  var storyInfo;
  int pageID = 1;

  ReadStoryViewModel _storyViewModel = ReadStoryViewModel();
  CreateStoryViewModel _createStoryViewModel = CreateStoryViewModel();

  TextEditingController _controller = TextEditingController(text: '');

  @override
  void initState() {
    super.initState();

    _storyViewModel = new ReadStoryViewModel();
    _createStoryViewModel = new CreateStoryViewModel();

    _storyViewModel.getStory(storyID).then((result) {
      setState(() {
        storyInfo = result;
      });
    });

    print(storyInfo);

    _storyViewModel.getPageInfo(pageID.toString(), storyID).then((result) {
      setState(() {
        currentPageInfo = result;
      });
    });

    _createStoryViewModel
        .getPageImageInfo(storyID, pageID.toString())
        .then((result) {
      setState(() {
        imageInfo = result;
      });
    });

    _controller = TextEditingController(text: '');
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);

    var screenSize = queryData.size;

    if (screenSize.height < 925) {
      Future<void> _showEditDialog(String word, String picto, int index) async {
        String finalWord = word;
        String finalPicto = picto;
        _controller = TextEditingController(text: finalWord);

        return showDialog<void>(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Text('Edición de palabra',
                      style: TextStyle(fontWeight: FontWeight.bold))),
              content: SizedBox(
                height: 100,
                width: 280,
                child: Row(
                  children: [
                    Container(
                      width: 100,
                      child: TextFormField(
                        controller: _controller,
                        onChanged: (value) {
                          setState(() => finalWord = value);
                          print(finalWord);
                        },
                      ),
                    ),
                    SizedBox(width: 20),
                    (picto != null && picto != ''
                        ? GestureDetector(
                            onTap: () async {
                              if (storyID != null) {
                                await _storyViewModel
                                    .getStory(storyID)
                                    .then((value) {
                                  setState(() {
                                    storyInfo = value;
                                  });
                                });
                              }

                              var pictoSelected = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => SelectImagePage(
                                            pictoEdit: picto,
                                          )));
                              setState(() {
                                finalPicto = pictoSelected;
                                picto = pictoSelected;
                              });
                              Navigator.pop(context);
                              _showEditDialog(finalWord, finalPicto, index);
                              _controller =
                                  TextEditingController(text: finalWord);
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image.network(
                                  finalPicto,
                                ),
                                SizedBox(width: 10),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    GestureDetector(
                                        onTap: () async {
                                          if (storyID != null) {
                                            await _storyViewModel
                                                .getStory(storyID)
                                                .then((value) {
                                              setState(() {
                                                storyInfo = value;
                                              });
                                            });
                                          }

                                          var pictoSelected =
                                              await Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          SelectImagePage(
                                                            pictoEdit: picto,
                                                          )));
                                          setState(() {
                                            finalPicto = pictoSelected;
                                            picto = pictoSelected;
                                          });
                                          Navigator.pop(context);
                                          _showEditDialog(
                                              finalWord, finalPicto, index);
                                          _controller = TextEditingController(
                                              text: finalWord);
                                        },
                                        child: Icon(Icons.add,
                                            color: Colors.grey, size: 30)),
                                    SizedBox(height: 5),
                                    GestureDetector(
                                      onTap: () async {
                                        await _createStoryViewModel
                                            .deletePictoFromWord(
                                                storyID,
                                                pageID.toString(),
                                                (index + 1).toString());

                                        await _storyViewModel
                                            .getPageInfo(
                                                pageID.toString(), storyID)
                                            .then((result) {
                                          setState(() {
                                            currentPageInfo = result;
                                          });
                                        });

                                        setState(() {
                                          finalPicto = null;
                                          picto = null;
                                        });

                                        Navigator.pop(context);
                                        _showEditDialog(
                                            finalWord, finalPicto, index);
                                        _controller = TextEditingController(
                                            text: finalWord);
                                      },
                                      child: Icon(Icons.delete,
                                          color: Colors.grey, size: 30),
                                    )
                                  ],
                                )
                              ],
                            ),
                          )
                        : GestureDetector(
                            onTap: () async {
                              if (storyID != null) {
                                await _storyViewModel
                                    .getStory(storyID)
                                    .then((value) {
                                  setState(() {
                                    storyInfo = value;
                                  });
                                });
                              }

                              var pictoSelected = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => SelectImagePage(
                                            storyInfo: null,
                                          )));
                              setState(() {
                                finalPicto = pictoSelected;
                                picto = pictoSelected;
                              });
                              Navigator.pop(context);
                              _showEditDialog(word, picto, index);
                            },
                            child: Row(
                              children: [
                                IconButton(
                                  icon: Icon(Icons.camera_alt_rounded),
                                  iconSize: 100,
                                  color: Colors.grey,
                                ),
                                SizedBox(width: 5),
                                GestureDetector(
                                    onTap: () async {
                                      if (storyID != null) {
                                        await _storyViewModel
                                            .getStory(storyID)
                                            .then((value) {
                                          setState(() {
                                            storyInfo = value;
                                          });
                                        });
                                      }

                                      var pictoSelected = await Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  SelectImagePage(
                                                    pictoEdit: picto,
                                                  )));
                                      setState(() {
                                        finalPicto = pictoSelected;
                                        picto = pictoSelected;
                                      });
                                      Navigator.pop(context);
                                      _showEditDialog(
                                          finalWord, finalPicto, index);
                                      _controller = TextEditingController(
                                          text: finalWord);
                                    },
                                    child: Icon(Icons.add,
                                        color: Colors.grey, size: 30)),
                              ],
                            )))
                  ],
                ),
              ),
              actions: [
                TextButton(
                  child: Text('Cancelar',
                      style: TextStyle(color: HexColor('#0D518C'))),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                TextButton(
                  child: Text('Aceptar',
                      style: TextStyle(color: HexColor('#0D518C'))),
                  onPressed: () async {
                    print(finalWord);
                    await _createStoryViewModel.updateWord(
                        (index + 1).toString(),
                        storyID,
                        pageID.toString(),
                        finalWord);
                    await _storyViewModel
                        .getPageInfo(pageID.toString(), storyID)
                        .then((result) {
                      setState(() {
                        currentPageInfo = result;
                      });
                    });

                    await _createStoryViewModel.addPictoToWord(storyID,
                        pageID.toString(), (index + 1).toString(), picto);

                    await _storyViewModel
                        .getPageInfo(pageID.toString(), storyID)
                        .then((result) {
                      setState(() {
                        currentPageInfo = result;
                      });
                    });

                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      }

      return Scaffold(
        body: Container(
          padding: EdgeInsets.only(top: 60),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                      child: Icon(Icons.navigate_before_rounded,
                          size: 50, color: HexColor('#071A40')),
                      onTap: () => Navigator.pop(context)),
                  RaisedButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                AssignStoryPage(storyID: storyID),
                          ));
                    },
                    child: Text(
                      'Asignación de usuarios',
                      style: TextStyle(color: Colors.white),
                    ),
                    color: HexColor('#0F8DBF'),
                  ),
                  SizedBox(width: 50)
                ],
              ),
              (currentPageInfo == null || imageInfo == null
                  ? Expanded(child: LoadingWords())
                  : Expanded(
                      child: Column(
                        children: [
                          (currentPageInfo == null
                              ? Text('')
                              : Expanded(
                                  child: GridView.builder(
                                      gridDelegate:
                                          const SliverGridDelegateWithFixedCrossAxisCount(
                                              crossAxisCount: 2,
                                              mainAxisSpacing: 1,
                                              crossAxisSpacing: 1),
                                      //scrollDirection: Axis.horizontal,
                                      padding: EdgeInsets.all(0),
                                      itemCount: currentPageInfo.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        final wordsInfo =
                                            currentPageInfo[index];
                                        return Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            (wordsInfo.picto != null
                                                ? GestureDetector(
                                                    onTap: () =>
                                                        _showEditDialog(
                                                            wordsInfo.word,
                                                            wordsInfo.picto,
                                                            index),
                                                    child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: [
                                                        Container(
                                                          height: 100,
                                                          child: Image.network(
                                                            wordsInfo.picto,
                                                          ),
                                                        ),
                                                        Container(
                                                          height: 30,
                                                          child: (wordsInfo.word
                                                                      .toString()
                                                                      .length <=
                                                                  5
                                                              ? Text(
                                                                  wordsInfo.word
                                                                          .toString()
                                                                          .toUpperCase() +
                                                                      '  ',
                                                                  style: GoogleFonts.lato(
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                      fontSize:
                                                                          25))
                                                              : Text(
                                                                  wordsInfo.word
                                                                          .toString()
                                                                          .toUpperCase() +
                                                                      '  ',
                                                                  style: GoogleFonts.lato(
                                                                      fontWeight: FontWeight.bold,
                                                                      fontSize: 18))),
                                                        )
                                                      ],
                                                    ),
                                                  )
                                                : (wordsInfo.word
                                                            .toString()
                                                            .length <=
                                                        5
                                                    ? SizedBox(
                                                        width: 100,
                                                        child: Center(
                                                          child:
                                                              GestureDetector(
                                                            onTap: () =>
                                                                _showEditDialog(
                                                                    wordsInfo
                                                                        .word,
                                                                    wordsInfo
                                                                        .picto,
                                                                    index),
                                                            child: Text(
                                                                wordsInfo.word
                                                                        .toString()
                                                                        .toUpperCase() +
                                                                    '  ',
                                                                style: GoogleFonts.lato(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    fontSize:
                                                                        25)),
                                                          ),
                                                        ),
                                                      )
                                                    : SizedBox(
                                                        width: 120,
                                                        child: Center(
                                                          child:
                                                              GestureDetector(
                                                            onTap: () =>
                                                                _showEditDialog(
                                                                    wordsInfo
                                                                        .word,
                                                                    wordsInfo
                                                                        .picto,
                                                                    index),
                                                            child: Text(
                                                                wordsInfo.word
                                                                        .toString()
                                                                        .toUpperCase() +
                                                                    '  ',
                                                                style: GoogleFonts.lato(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    fontSize:
                                                                        18)),
                                                          ),
                                                        ),
                                                      ))),
                                            SizedBox(width: 20)
                                          ],
                                        );
                                      }),
                                )),
                        ],
                      ),
                    )),
              Divider(
                color: Colors.grey,
                height: 12,
                thickness: 1,
              ),
              Container(
                padding: EdgeInsets.all(2),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    (pageID != 1
                        ? GestureDetector(
                            child: Icon(Icons.navigate_before_rounded,
                                size: 70, color: HexColor('#071A40')),
                            onTap: () async {
                              pageID = pageID - 1;
                              await _storyViewModel
                                  .getPageInfo((pageID).toString(), storyID)
                                  .then((result) {
                                setState(() {
                                  currentPageInfo = result;
                                });
                              });

                              await _createStoryViewModel
                                  .getPageImageInfo(storyID, pageID.toString())
                                  .then((result) {
                                setState(() {
                                  imageInfo = result;
                                });
                              });
                            })
                        : SizedBox(width: 70)),
                    (imageInfo != null && imageInfo.image != null
                        ? GestureDetector(
                            onTap: () async {
                              var imageInfoEdited = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => AddImageToPage(
                                        storyID: storyID,
                                        pageID: pageID,
                                        imageInfo: imageInfo),
                                  ));

                              if (imageInfoEdited != null) {
                                setState(() {
                                  imageInfo = imageInfoEdited;
                                });
                              }
                            },
                            child: Column(
                              children: [
                                Image.network(imageInfo.image, height: 70),
                                SizedBox(height: 5),
                                Text('Página ' + pageID.toString(),
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold)),
                              ],
                            ),
                          )
                        : Column(
                            children: [
                              GestureDetector(
                                  onTap: () async {
                                    var imageInfoEdited = await Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => AddImageToPage(
                                              storyID: storyID,
                                              pageID: pageID,
                                              imageInfo: imageInfo),
                                        ));

                                    if (imageInfoEdited != null) {
                                      setState(() {
                                        imageInfo = imageInfoEdited;
                                      });
                                    }

                                    _createStoryViewModel
                                        .getPageImageInfo(
                                            storyID, pageID.toString())
                                        .then((result) {
                                      setState(() {
                                        imageInfo = result;
                                      });
                                    });
                                  },
                                  child: Icon(Icons.camera_alt_rounded,
                                      size: 75, color: HexColor('#72CEF2'))),
                              Text('Página ' + pageID.toString(),
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold)),
                            ],
                          )),
                    (pageID <
                            int.parse(
                                (storyInfo != null ? storyInfo.numPages : "0"))
                        ? GestureDetector(
                            child: Icon(
                              Icons.navigate_next_rounded,
                              size: 70,
                              color: HexColor('#071A40'),
                            ),
                            onTap: () async {
                              pageID = pageID + 1;
                              await _storyViewModel
                                  .getPageInfo((pageID).toString(), storyID)
                                  .then((result) {
                                setState(() {
                                  currentPageInfo = result;
                                });
                              });

                              await _createStoryViewModel
                                  .getPageImageInfo(storyID, pageID.toString())
                                  .then((result) {
                                setState(() {
                                  imageInfo = result;
                                });
                              });
                            })
                        : SizedBox(width: 70))
                  ],
                ),
              ),
              SizedBox(height: 10),
            ],
          ),
        ),
      );
    } else {
      Future<void> _showEditDialog(String word, String picto, int index) async {
        String finalWord = word;
        String finalPicto = picto;
        _controller = TextEditingController(text: finalWord);

        return showDialog<void>(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Text('Edición de palabra',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 26))),
              content: SizedBox(
                height: 300,
                width: 500,
                child: Row(
                  children: [
                    Container(
                      width: 200,
                      child: TextFormField(
                        style: TextStyle(fontSize: 24),
                        controller: _controller,
                        onChanged: (value) {
                          setState(() => finalWord = value);
                          print(finalWord);
                        },
                      ),
                    ),
                    SizedBox(width: 20),
                    (picto != null && picto != ''
                        ? GestureDetector(
                            onTap: () async {
                              if (storyID != null) {
                                await _storyViewModel
                                    .getStory(storyID)
                                    .then((value) {
                                  setState(() {
                                    storyInfo = value;
                                  });
                                });
                              }

                              var pictoSelected = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => SelectImagePage(
                                            pictoEdit: picto,
                                          )));
                              setState(() {
                                finalPicto = pictoSelected;
                                picto = pictoSelected;
                              });
                              Navigator.pop(context);
                              _showEditDialog(finalWord, finalPicto, index);
                              _controller =
                                  TextEditingController(text: finalWord);
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SizedBox(
                                  height: 300,
                                  width: 200,
                                  child: Image.network(
                                    finalPicto,
                                  ),
                                ),
                                SizedBox(width: 20),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    GestureDetector(
                                        onTap: () async {
                                          if (storyID != null) {
                                            await _storyViewModel
                                                .getStory(storyID)
                                                .then((value) {
                                              setState(() {
                                                storyInfo = value;
                                              });
                                            });
                                          }

                                          var pictoSelected =
                                              await Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          SelectImagePage(
                                                            pictoEdit: picto,
                                                          )));
                                          setState(() {
                                            finalPicto = pictoSelected;
                                            picto = pictoSelected;
                                          });
                                          Navigator.pop(context);
                                          _showEditDialog(
                                              finalWord, finalPicto, index);
                                          _controller = TextEditingController(
                                              text: finalWord);
                                        },
                                        child: Icon(Icons.add,
                                            color: Colors.grey, size: 40)),
                                    SizedBox(height: 5),
                                    GestureDetector(
                                      onTap: () async {
                                        await _createStoryViewModel
                                            .deletePictoFromWord(
                                                storyID,
                                                pageID.toString(),
                                                (index + 1).toString());

                                        await _storyViewModel
                                            .getPageInfo(
                                                pageID.toString(), storyID)
                                            .then((result) {
                                          setState(() {
                                            currentPageInfo = result;
                                          });
                                        });

                                        setState(() {
                                          finalPicto = null;
                                          picto = null;
                                        });

                                        Navigator.pop(context);
                                        _showEditDialog(
                                            finalWord, finalPicto, index);
                                        _controller = TextEditingController(
                                            text: finalWord);
                                      },
                                      child: Icon(Icons.delete,
                                          color: Colors.grey, size: 40),
                                    )
                                  ],
                                )
                              ],
                            ),
                          )
                        : GestureDetector(
                            onTap: () async {
                              if (storyID != null) {
                                await _storyViewModel
                                    .getStory(storyID)
                                    .then((value) {
                                  setState(() {
                                    storyInfo = value;
                                  });
                                });
                              }

                              var pictoSelected = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => SelectImagePage(
                                            storyInfo: null,
                                          )));
                              setState(() {
                                finalPicto = pictoSelected;
                                picto = pictoSelected;
                              });
                              Navigator.pop(context);
                              _showEditDialog(word, picto, index);
                            },
                            child: Row(
                              children: [
                                IconButton(
                                  icon: Icon(Icons.camera_alt_rounded),
                                  iconSize: 200,
                                  color: Colors.grey,
                                ),
                                SizedBox(width: 10),
                                GestureDetector(
                                    onTap: () async {
                                      if (storyID != null) {
                                        await _storyViewModel
                                            .getStory(storyID)
                                            .then((value) {
                                          setState(() {
                                            storyInfo = value;
                                          });
                                        });
                                      }

                                      var pictoSelected = await Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  SelectImagePage(
                                                    pictoEdit: picto,
                                                  )));
                                      setState(() {
                                        finalPicto = pictoSelected;
                                        picto = pictoSelected;
                                      });
                                      Navigator.pop(context);
                                      _showEditDialog(
                                          finalWord, finalPicto, index);
                                      _controller = TextEditingController(
                                          text: finalWord);
                                    },
                                    child: Icon(Icons.add,
                                        color: Colors.grey, size: 40)),
                              ],
                            )))
                  ],
                ),
              ),
              actions: [
                TextButton(
                  child: Text('Cancelar',
                      style:
                          TextStyle(color: HexColor('#0D518C'), fontSize: 24)),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                TextButton(
                  child: Text('Aceptar',
                      style:
                          TextStyle(color: HexColor('#0D518C'), fontSize: 24)),
                  onPressed: () async {
                    print(finalWord);
                    await _createStoryViewModel.updateWord(
                        (index + 1).toString(),
                        storyID,
                        pageID.toString(),
                        finalWord);
                    await _storyViewModel
                        .getPageInfo(pageID.toString(), storyID)
                        .then((result) {
                      setState(() {
                        currentPageInfo = result;
                      });
                    });

                    await _createStoryViewModel.addPictoToWord(storyID,
                        pageID.toString(), (index + 1).toString(), picto);

                    await _storyViewModel
                        .getPageInfo(pageID.toString(), storyID)
                        .then((result) {
                      setState(() {
                        currentPageInfo = result;
                      });
                    });

                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      }

      return Scaffold(
        body: Container(
          padding: EdgeInsets.only(top: 60),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                      child: Icon(Icons.navigate_before_rounded,
                          size: 70, color: HexColor('#071A40')),
                      onTap: () => Navigator.pop(context)),
                  ButtonTheme(
                    height: 60,
                    minWidth: 300,
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) =>
                                  AssignStoryPage(storyID: storyID),
                            ));
                      },
                      child: Text(
                        'Asignación de usuarios',
                        style: TextStyle(color: Colors.white, fontSize: 22),
                      ),
                      color: HexColor('#0F8DBF'),
                    ),
                  ),
                  SizedBox(width: 50)
                ],
              ),
              (currentPageInfo == null || imageInfo == null
                  ? Expanded(child: LoadingWords())
                  : Expanded(
                      child: Column(
                        children: [
                          (currentPageInfo == null
                              ? Text('')
                              : Expanded(
                                  child: GridView.builder(
                                      gridDelegate:
                                          const SliverGridDelegateWithFixedCrossAxisCount(
                                              crossAxisCount: 2,
                                              mainAxisSpacing: 1,
                                              crossAxisSpacing: 1),
                                      //scrollDirection: Axis.horizontal,
                                      padding: EdgeInsets.all(0),
                                      itemCount: currentPageInfo.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        final wordsInfo =
                                            currentPageInfo[index];
                                        return Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            (wordsInfo.picto != null
                                                ? GestureDetector(
                                                    onTap: () =>
                                                        _showEditDialog(
                                                            wordsInfo.word,
                                                            wordsInfo.picto,
                                                            index),
                                                    child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: [
                                                        Container(
                                                          height: 180,
                                                          child: Image.network(
                                                            wordsInfo.picto,
                                                          ),
                                                        ),
                                                        Container(
                                                          height: 40,
                                                          child: (wordsInfo.word
                                                                      .toString()
                                                                      .length <=
                                                                  5
                                                              ? Text(
                                                                  wordsInfo.word
                                                                          .toString()
                                                                          .toUpperCase() +
                                                                      '  ',
                                                                  style: GoogleFonts.lato(
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                      fontSize:
                                                                          35))
                                                              : Text(
                                                                  wordsInfo.word
                                                                          .toString()
                                                                          .toUpperCase() +
                                                                      '  ',
                                                                  style: GoogleFonts.lato(
                                                                      fontWeight: FontWeight.bold,
                                                                      fontSize: 30))),
                                                        )
                                                      ],
                                                    ),
                                                  )
                                                : (wordsInfo.word
                                                            .toString()
                                                            .length <=
                                                        5
                                                    ? SizedBox(
                                                        width: 140,
                                                        child: Center(
                                                          child:
                                                              GestureDetector(
                                                            onTap: () =>
                                                                _showEditDialog(
                                                                    wordsInfo
                                                                        .word,
                                                                    wordsInfo
                                                                        .picto,
                                                                    index),
                                                            child: Text(
                                                                wordsInfo.word
                                                                        .toString()
                                                                        .toUpperCase() +
                                                                    '  ',
                                                                style: GoogleFonts.lato(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    fontSize:
                                                                        40)),
                                                          ),
                                                        ),
                                                      )
                                                    : SizedBox(
                                                        width: 200,
                                                        child: Center(
                                                          child:
                                                              GestureDetector(
                                                            onTap: () =>
                                                                _showEditDialog(
                                                                    wordsInfo
                                                                        .word,
                                                                    wordsInfo
                                                                        .picto,
                                                                    index),
                                                            child: Text(
                                                                wordsInfo.word
                                                                        .toString()
                                                                        .toUpperCase() +
                                                                    '  ',
                                                                style: GoogleFonts.lato(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    fontSize:
                                                                        30)),
                                                          ),
                                                        ),
                                                      ))),
                                            SizedBox(width: 20)
                                          ],
                                        );
                                      }),
                                )),
                        ],
                      ),
                    )),
              Divider(
                color: Colors.grey,
                height: 12,
                thickness: 1,
              ),
              Container(
                padding: EdgeInsets.all(2),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    (pageID != 1
                        ? GestureDetector(
                            child: Icon(Icons.navigate_before_rounded,
                                size: 80, color: HexColor('#071A40')),
                            onTap: () async {
                              pageID = pageID - 1;
                              await _storyViewModel
                                  .getPageInfo((pageID).toString(), storyID)
                                  .then((result) {
                                setState(() {
                                  currentPageInfo = result;
                                });
                              });

                              await _createStoryViewModel
                                  .getPageImageInfo(storyID, pageID.toString())
                                  .then((result) {
                                setState(() {
                                  imageInfo = result;
                                });
                              });
                            })
                        : SizedBox(width: 70)),
                    (imageInfo != null && imageInfo.image != null
                        ? GestureDetector(
                            onTap: () async {
                              var imageInfoEdited = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => AddImageToPage(
                                        storyID: storyID,
                                        pageID: pageID,
                                        imageInfo: imageInfo),
                                  ));

                              if (imageInfoEdited != null) {
                                setState(() {
                                  imageInfo = imageInfoEdited;
                                });
                              }
                            },
                            child: Column(
                              children: [
                                Image.network(imageInfo.image, height: 100),
                                SizedBox(height: 5),
                                Text('Página ' + pageID.toString(),
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold)),
                              ],
                            ),
                          )
                        : Column(
                            children: [
                              GestureDetector(
                                  onTap: () async {
                                    var imageInfoEdited = await Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => AddImageToPage(
                                              storyID: storyID,
                                              pageID: pageID,
                                              imageInfo: imageInfo),
                                        ));

                                    if (imageInfoEdited != null) {
                                      setState(() {
                                        imageInfo = imageInfoEdited;
                                      });
                                    }

                                    _createStoryViewModel
                                        .getPageImageInfo(
                                            storyID, pageID.toString())
                                        .then((result) {
                                      setState(() {
                                        imageInfo = result;
                                      });
                                    });
                                  },
                                  child: Icon(Icons.camera_alt_rounded,
                                      size: 105, color: HexColor('#72CEF2'))),
                              Text('Página ' + pageID.toString(),
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold)),
                            ],
                          )),
                    (pageID <
                            int.parse(
                                (storyInfo != null ? storyInfo.numPages : "0"))
                        ? GestureDetector(
                            child: Icon(
                              Icons.navigate_next_rounded,
                              size: 80,
                              color: HexColor('#071A40'),
                            ),
                            onTap: () async {
                              pageID = pageID + 1;
                              await _storyViewModel
                                  .getPageInfo((pageID).toString(), storyID)
                                  .then((result) {
                                setState(() {
                                  currentPageInfo = result;
                                });
                              });

                              await _createStoryViewModel
                                  .getPageImageInfo(storyID, pageID.toString())
                                  .then((result) {
                                setState(() {
                                  imageInfo = result;
                                });
                              });
                            })
                        : SizedBox(width: 70))
                  ],
                ),
              ),
              SizedBox(height: 20),
            ],
          ),
        ),
      );
    }
  }
}
