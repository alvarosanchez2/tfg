import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tfg_app/model/services/auth_service.dart';
import 'package:flutter/services.dart';
import 'package:tfg_app/model/story.dart';
import 'package:tfg_app/view/widgets/story_list.dart';
import 'package:tfg_app/view_model/read_story_view_model.dart';
import 'package:tfg_app/view_model/sign_up_view_model.dart';

class SelectGenrePage extends StatefulWidget {
  final String userID;
  SelectGenrePage({Key key, this.userID}) : super(key: key);

  @override
  _SelectGenrePageState createState() {
    return _SelectGenrePageState(userID: userID);
  }
}

class _SelectGenrePageState extends State<SelectGenrePage> {
  String userID;
  _SelectGenrePageState({this.userID});

  ReadStoryViewModel _storyViewModel = ReadStoryViewModel();

  @override
  void initState() {
    super.initState();
    _storyViewModel = new ReadStoryViewModel();
  }

  @override
  void dispose() {
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamProvider<List<Story>>.value(
          value: _storyViewModel.getStories(userID), child: StoriesList(userID: userID)),
    );
  }
}
