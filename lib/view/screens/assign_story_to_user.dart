import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';
import 'package:tfg_app/view_model/create_story_view_model.dart';
import 'package:tfg_app/view_model/read_story_view_model.dart';

class AssignStoryPage extends StatefulWidget {
  final String storyID;
  AssignStoryPage({Key key, this.storyID}) : super(key: key);

  @override
  _AssignStoryPageState createState() {
    return _AssignStoryPageState(storyID: storyID);
  }
}

class _AssignStoryPageState extends State<AssignStoryPage> {
  String storyID;
  _AssignStoryPageState({this.storyID});

  ReadStoryViewModel _readStoryViewModel = ReadStoryViewModel();
  CreateStoryViewModel _createStoryViewModel = CreateStoryViewModel();
  var storyInfo;
  final List<DropdownMenuItem> items = [];
  List<dynamic> usersList = [];
  List<dynamic> usersListAssigned = [];
  String selectedValue = '';
  MediaQueryData queryData;

  @override
  void initState() {
    super.initState();
    _readStoryViewModel = new ReadStoryViewModel();
    _createStoryViewModel = new CreateStoryViewModel();

    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]);

    _readStoryViewModel.getStory(storyID).then((value) {
      setState(() {
        storyInfo = value;
      });
    });

    _createStoryViewModel.getUsers().then((List<dynamic> result) {
      setState(() {
        result.forEach((user) {
          items.add(DropdownMenuItem(
            child: Text(user['name'] + " " + user['fullname'], style: TextStyle(fontSize: 20)),
            value: user['id'] + " " + user['name'] + " " + user['fullname'],
          ));
        });
        usersList = result;
      });
    });

    _createStoryViewModel
        .getUsersAssigned(storyID)
        .then((List<dynamic> result) {
      setState(() {
        usersListAssigned = result;
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    queryData = MediaQuery.of(context);

    var screenSize = queryData.size;

    if (screenSize.height < 925) {
      Map<String, Widget> widget;
      widget = {
        "Lista de niños": SearchableDropdown.single(
          items: items,
          value: (selectedValue == null || selectedValue == ''
              ? ''
              : selectedValue),
          hint: "Elige uno",
          searchHint: "Elige uno",
          onChanged: (value) {
            setState(() {
              selectedValue = value;
            });
          },
          isExpanded: true,
        ),
      };

      return Scaffold(
        appBar: AppBar(
            title: const Text("Asignación de cuentos a usuarios"),
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () => Navigator.of(context).pop(),
            ),
            centerTitle: true,
            backgroundColor: HexColor('#0D518C')),
        body: Column(
          children: [
            Container(
              padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      Text('Título: ',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 22)),
                      Text(storyInfo != null ? storyInfo.title : '',
                          style: TextStyle(fontSize: 20)),
                    ],
                  ),
                  Row(
                    children: [
                      Text('Género: ',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 22)),
                      Text(storyInfo != null ? storyInfo.genre : '',
                          style: TextStyle(fontSize: 20)),
                    ],
                  ),
                  Row(
                    children: [
                      Text('Autor: ',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 22)),
                      Text(storyInfo != null ? storyInfo.author : '',
                          style: TextStyle(fontSize: 20)),
                    ],
                  ),
                  Row(
                    children: [
                      Text('Número de páginas: ',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 22)),
                      Text(storyInfo != null ? storyInfo.numPages : '',
                          style: TextStyle(fontSize: 20)),
                    ],
                  ),
                ],
              ),
            ),
            Column(
              children: widget
                  .map((k, v) {
                return (MapEntry(
                    k,
                    Center(
                        child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                              side: BorderSide(
                                color: Colors.grey,
                                width: 1.0,
                              ),
                            ),
                            margin: EdgeInsets.all(20),
                            child: Padding(
                              padding: const EdgeInsets.all(20.0),
                              child: Column(
                                children: <Widget>[
                                  Text("$k:"),
                                  v,
                                ],
                              ),
                            )))));
              })
                  .values
                  .toList(),
            ),
            ButtonTheme(
              minWidth: 300,
              child: RaisedButton(
                onPressed: (selectedValue == null || selectedValue == '')
                    ? null
                    : () async {
                  print(selectedValue);
                  var userInfo = selectedValue.split(' ');
                  String userID = userInfo.first;

                  var result = await _createStoryViewModel
                      .assignStoryToUser(storyID, userID);

                  // Volvemos a obtener la lista de tutores para recargar la lista que se muestra por pantalla
                  _createStoryViewModel
                      .getUsersAssigned(storyID)
                      .then((List<dynamic> result) {
                    setState(() {
                      usersListAssigned = result;
                    });
                  });

                  setState(() {
                    selectedValue = '';
                  });
                },
                child: Text(
                  'Asignar',
                  style: TextStyle(color: Colors.white),
                ),
                color: HexColor('#72CEF2'),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ),
              ),
            ),
            SizedBox(height: 10),
            (usersListAssigned.length == 0)
                ? Expanded(child: Text('No se han asignado educadores todavía'))
                : Expanded(
              child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: usersListAssigned.length,
                  itemBuilder: (BuildContext context, int index) {
                    final user = usersListAssigned[index];
                    return Column(
                      children: [
                        ListTile(
                          title: (Text(user['name'] +
                              " " +
                              user['fullname'])),
                          leading: Icon(Icons.person),
                        ),
                        Divider(
                          color: Colors.grey,
                          height: 12,
                        )
                      ],
                    );
                  }),
            ),
            SizedBox(
              height: 35,
              child: RaisedButton(
                onPressed: () =>
                    Navigator.popUntil(
                        context, ModalRoute.withName('/manage-book')),
                child: Text('Finalizar', style: TextStyle(color: Colors.white)),
                color: HexColor('#0F8DBF'),),
            ),
            SizedBox(height: 30),
          ],
        ),
      );
    } else {
      Map<String, Widget> widget;
      widget = {
        "Lista de niños": SearchableDropdown.single(
          style: TextStyle(fontSize: 24, color: Colors.black),
          iconSize: 40,
          items: items,
          value: (selectedValue == null || selectedValue == ''
              ? ''
              : selectedValue),
          hint: "Elige uno",
          searchHint: "Elige uno",
          onChanged: (value) {
            setState(() {
              selectedValue = value;
            });
          },
          isExpanded: true,
        ),
      };

      return Scaffold(
        appBar: AppBar(
            title: const Text("Asignación de cuentos a usuarios", style: TextStyle(fontSize: 28)),
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.white, size: 35),
              onPressed: () => Navigator.of(context).pop(),
            ),
            centerTitle: true,
            backgroundColor: HexColor('#0D518C')),
        body: Column(
          children: [
            Container(
              padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      Text('Título: ',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 32)),
                      Text(storyInfo != null ? storyInfo.title : '',
                          style: TextStyle(fontSize: 30)),
                    ],
                  ),
                  Row(
                    children: [
                      Text('Género: ',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 32)),
                      Text(storyInfo != null ? storyInfo.genre : '',
                          style: TextStyle(fontSize: 30)),
                    ],
                  ),
                  Row(
                    children: [
                      Text('Autor: ',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 32)),
                      Text(storyInfo != null ? storyInfo.author : '',
                          style: TextStyle(fontSize: 30)),
                    ],
                  ),
                  Row(
                    children: [
                      Text('Número de páginas: ',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 32)),
                      Text(storyInfo != null ? storyInfo.numPages : '',
                          style: TextStyle(fontSize: 30)),
                    ],
                  ),
                ],
              ),
            ),
            Column(
              children: widget
                  .map((k, v) {
                return (MapEntry(
                    k,
                    Center(
                        child: Container(
                          height: 200,
                          child: Card(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                                side: BorderSide(
                                  color: Colors.grey,
                                  width: 1.0,
                                ),
                              ),
                              margin: EdgeInsets.all(20),
                              child: Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: Column(
                                  children: <Widget>[
                                    Text("$k:", style: TextStyle(fontSize: 24)),
                                    SizedBox(height: 20),
                                    v,
                                  ],
                                ),
                              )),
                        ))));
              })
                  .values
                  .toList(),
            ),
            ButtonTheme(
              height: 50,
              minWidth: 400,
              child: RaisedButton(
                onPressed: (selectedValue == null || selectedValue == '')
                    ? null
                    : () async {
                  print(selectedValue);
                  var userInfo = selectedValue.split(' ');
                  String userID = userInfo.first;

                  var result = await _createStoryViewModel
                      .assignStoryToUser(storyID, userID);

                  // Volvemos a obtener la lista de tutores para recargar la lista que se muestra por pantalla
                  _createStoryViewModel
                      .getUsersAssigned(storyID)
                      .then((List<dynamic> result) {
                    setState(() {
                      usersListAssigned = result;
                    });
                  });

                  setState(() {
                    selectedValue = '';
                  });
                },
                child: Text(
                  'Asignar',
                  style: TextStyle(color: Colors.white, fontSize: 24),
                ),
                color: HexColor('#72CEF2'),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(22.0),
                ),
              ),
            ),
            SizedBox(height: 10),
            (usersListAssigned.length == 0)
                ? Expanded(child: Text('No se han asignado educadores todavía', style: TextStyle(fontSize: 24)))
                : Expanded(
              child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: usersListAssigned.length,
                  itemBuilder: (BuildContext context, int index) {
                    final user = usersListAssigned[index];
                    return Column(
                      children: [
                        ListTile(
                          title: (Text(user['name'] +
                              " " +
                              user['fullname'], style: TextStyle(fontSize: 24))),
                          leading: Icon(Icons.person, size: 40),
                        ),
                        Divider(
                          color: Colors.grey,
                          height: 18,
                        )
                      ],
                    );
                  }),
            ),
            ButtonTheme(
              height: 50,
              minWidth: 200,
              child: RaisedButton(
                onPressed: () =>
                    Navigator.popUntil(
                        context, ModalRoute.withName('/manage-book')),
                child: Text('Finalizar', style: TextStyle(color: Colors.white, fontSize: 24)),
                color: HexColor('#0F8DBF'),),
            ),
            SizedBox(height: 50),
          ],
        ),
      );
    }
  }
}
