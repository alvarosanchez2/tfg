import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:tfg_app/model/story_page.dart';
import 'package:tfg_app/view/screens/select_image_screen.dart';
import 'package:tfg_app/view_model/create_story_view_model.dart';

class AddImageToPage extends StatefulWidget {
  final String storyID;
  final StoryPage imageInfo;
  final int pageID;
  AddImageToPage({Key key, this.storyID, this.pageID, this.imageInfo})
      : super(key: key);

  @override
  _AddImageToPageState createState() {
    return _AddImageToPageState(
        storyID: storyID, pageID: pageID, imageInfo: imageInfo);
  }
}

class _AddImageToPageState extends State<AddImageToPage> {
  String storyID;
  int pageID;
  StoryPage imageInfo;
  _AddImageToPageState({this.storyID, this.pageID, this.imageInfo});

  MediaQueryData queryData;

  CreateStoryViewModel _createStoryViewModel = CreateStoryViewModel();
  String imageText = '';
  String error = '';
  String pageImage = '';
  TextEditingController _controller;

  @override
  void initState() {
    super.initState();
    _createStoryViewModel = new CreateStoryViewModel();

    _controller = TextEditingController(text: '');

    print(imageInfo);

    _createStoryViewModel
        .getPageImageInfo(storyID, pageID.toString())
        .then((result) {
      setState(() {
        imageInfo = result;
      });
    });

    if (imageInfo != null) {
      _controller = TextEditingController(text: imageInfo.imageText);
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);
    var screenSize = queryData.size;

    if (screenSize.height < 925) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Añadir imagen a la página'),
          backgroundColor: HexColor('#0D518C'),
          centerTitle: true,
        ),
        body: Column(
          children: [
            (pageImage != '' && pageImage != null
                ? GestureDetector(
                    onTap: () async {
                      var imageSelected = await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => SelectImagePage(
                                    pageInfo: imageInfo,
                                  )));
                      setState(() {
                        pageImage = imageSelected;
                      });
                    },
                    child: Image.network(
                      pageImage,
                      height: 150,
                      width: 150,
                    ),
                  )
                : imageInfo != null && imageInfo.image != null
                    ? GestureDetector(
                        onTap: () async {
                          var imageSelected = await Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SelectImagePage(
                                        pageInfo: imageInfo,
                                      )));
                          setState(() {
                            pageImage = imageSelected;
                          });
                        },
                        child: Image.network(
                          imageInfo.image,
                          height: 150,
                          width: 150,
                        ),
                      )
                    : GestureDetector(
                        onTap: () async {
                          var imageSelected = await Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SelectImagePage(
                                        pageInfo: imageInfo,
                                      )));
                          setState(() {
                            pageImage = imageSelected;
                          });
                        },
                        child: Icon(Icons.camera_alt,
                            color: Colors.grey, size: 160))),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(width: 20),
                Expanded(
                  child: TextFormField(
                    controller: _controller,
                    decoration: InputDecoration(
                        hintText: 'Texto a escuchar',
                        prefixIcon: Icon(Icons.surround_sound_outlined)),
                    validator: (val) => val.isEmpty
                        ? 'Introduzca un texto para el pictograma'
                        : null,
                    onChanged: (value) {
                      setState(() {
                        imageText = value;
                      });
                    },
                  ),
                ),
                SizedBox(width: 20)
              ],
            ),
            SizedBox(height: 10),
            Text(error, style: TextStyle(color: Colors.red)),
            SizedBox(height: 5),
            RaisedButton(
                onPressed: () async {
                  if ((pageImage != null &&
                      pageImage != '' &&
                      imageText != '')) {
                    print(pageImage);
                    await _createStoryViewModel.addImageToPage(
                        storyID, pageID.toString(), pageImage);

                    await _createStoryViewModel
                        .getPageImageInfo(storyID, pageID.toString())
                        .then((result) {
                      setState(() {
                        imageInfo = result;
                      });
                    });

                    print('Guardar texto');

                    print('Guardando texto');
                    await _createStoryViewModel.addImageTextToPage(
                        storyID, pageID.toString(), imageText);

                    _controller = TextEditingController(text: '');

                    Navigator.pop(context, imageInfo);
                  } else if (pageImage != '' && imageInfo.imageText != null) {
                    print(pageImage);
                    await _createStoryViewModel.addImageToPage(
                        storyID, pageID.toString(), pageImage);

                    await _createStoryViewModel
                        .getPageImageInfo(storyID, pageID.toString())
                        .then((result) {
                      setState(() {
                        imageInfo = result;
                      });
                    });
                    Navigator.pop(context, imageInfo);
                  } else if (imageText != '' && imageInfo.image != null) {
                    await _createStoryViewModel.addImageTextToPage(
                        storyID, pageID.toString(), imageText);

                    _controller = TextEditingController(text: '');

                    Navigator.pop(context, imageInfo);
                  } else if (imageInfo.image != null &&
                      imageInfo.imageText != null) {
                    Navigator.pop(context, imageInfo);
                  } else if (pageImage == '') {
                    setState(() {
                      error = 'Añada una imagen a la página';
                    });
                  } else if (imageText == '') {
                    setState(() {
                      error = 'Seleccione un texto para la imagen';
                    });
                  } else if (imageInfo != null) {
                    Navigator.pop(context, imageInfo);
                  }
                },
                child: Text(
                  'Guardar',
                  style: TextStyle(color: Colors.white),
                ),
                color: HexColor('#0F8DBF'))
          ],
        ),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          title: Text('Añadir imagen a la página', style: TextStyle(fontSize: 26)),
          backgroundColor: HexColor('#0D518C'),
          centerTitle: true,
        ),
        body: Container(
          width: double.infinity,
          padding: EdgeInsets.symmetric(vertical: 50, horizontal: 50),
          child: Column(
            children: [
              (pageImage != '' && pageImage != null
                  ? GestureDetector(
                      onTap: () async {
                        var imageSelected = await Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SelectImagePage(
                                      pageInfo: imageInfo,
                                    )));
                        setState(() {
                          pageImage = imageSelected;
                        });
                      },
                      child: Image.network(
                        pageImage,
                        height: 300,
                        width: 300,
                      ),
                    )
                  : imageInfo != null && imageInfo.image != null
                      ? GestureDetector(
                          onTap: () async {
                            var imageSelected = await Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => SelectImagePage(
                                          pageInfo: imageInfo,
                                        )));
                            setState(() {
                              pageImage = imageSelected;
                            });
                          },
                          child: Image.network(
                            imageInfo.image,
                            height: 300,
                            width: 300,
                          ),
                        )
                      : GestureDetector(
                          onTap: () async {
                            var imageSelected = await Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => SelectImagePage(
                                          pageInfo: imageInfo,
                                        )));
                            setState(() {
                              pageImage = imageSelected;
                            });
                          },
                          child: Icon(Icons.camera_alt,
                              color: Colors.grey, size: 160))),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(width: 20),
                  Expanded(
                    child: TextFormField(
                      style: TextStyle(fontSize: 24),
                      controller: _controller,
                      decoration: InputDecoration(
                          hintText: 'Texto a escuchar',
                          prefixIcon: Icon(Icons.surround_sound_outlined, size: 40,)),
                      validator: (val) => val.isEmpty
                          ? 'Introduzca un texto para el pictograma'
                          : null,
                      onChanged: (value) {
                        setState(() {
                          imageText = value;
                        });
                      },
                    ),
                  ),
                  SizedBox(width: 20)
                ],
              ),
              SizedBox(height: 20),
              Text(error, style: TextStyle(color: Colors.red)),
              SizedBox(height: 10),
              ButtonTheme(
                height: 50,
                minWidth: 200,
                child: RaisedButton(
                    onPressed: () async {
                      if ((pageImage != null &&
                          pageImage != '' &&
                          imageText != '')) {
                        print(pageImage);
                        await _createStoryViewModel.addImageToPage(
                            storyID, pageID.toString(), pageImage);

                        await _createStoryViewModel
                            .getPageImageInfo(storyID, pageID.toString())
                            .then((result) {
                          setState(() {
                            imageInfo = result;
                          });
                        });

                        print('Guardar texto');

                        print('Guardando texto');
                        await _createStoryViewModel.addImageTextToPage(
                            storyID, pageID.toString(), imageText);

                        _controller = TextEditingController(text: '');

                        Navigator.pop(context);
                      } else if (pageImage != '' && imageInfo.imageText != null) {
                        print(pageImage);
                        await _createStoryViewModel.addImageToPage(
                            storyID, pageID.toString(), pageImage);

                        await _createStoryViewModel
                            .getPageImageInfo(storyID, pageID.toString())
                            .then((result) {
                          setState(() {
                            imageInfo = result;
                          });
                        });

                        Navigator.pop(context, imageInfo);
                      } else if (imageText != '' && imageInfo.image != null) {
                        await _createStoryViewModel.addImageTextToPage(
                            storyID, pageID.toString(), imageText);

                        _controller = TextEditingController(text: '');

                        Navigator.pop(context);
                      } else if (imageInfo.image != null &&
                          imageInfo.imageText != null) {
                        Navigator.pop(context);
                      } else if (pageImage == '') {
                        setState(() {
                          error = 'Añada una imagen a la página';
                        });
                      } else if (imageText == '') {
                        setState(() {
                          error = 'Seleccione un texto para la imagen';
                        });
                      } else if (imageInfo != null) {
                        Navigator.pop(context);
                      }
                    },
                    child: Text(
                      'Guardar',
                      style: TextStyle(color: Colors.white, fontSize: 22),
                    ),
                    color: HexColor('#0F8DBF')),
              )
            ],
          ),
        ),
      );
    }
  }
}
